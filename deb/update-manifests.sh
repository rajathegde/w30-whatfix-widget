#!/bin/bash

echo "update-manifests called with - $1"

# Check if the script needs to be run
if [ -z "$1" ]; then
	echo "No Version passed. Exitting"
	exit 0;
fi

#Set the value for version
VERSION=$1

#Update version number - Chrome
sed -i "s#\"version\".*#\"version\":\"$VERSION\",#" ../war/extension-meta-info/editor/manifest.json

#Update version number - Chrome
sed -i "s#\"version\".*#\"version\":\"$VERSION\",#" ../war/extension-meta-info-firefox/editor/manifest.json 
