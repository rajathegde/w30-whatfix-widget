#Spec file for building whatfix UI
Summary: Whatfix UI
Name: whatfix
Version: ##version
Release: 1
License: whatfix
Group: Development/application
SOURCE0 : whatfix.tar
URL: https://whatfix.com/
BuildArch : noarch


%description
%{summary}

%prep
%setup -q

%build
# Empty section.

%install
rm -rf %{buildroot}
mkdir -p  %{buildroot}

# in builddir
cp -a * %{buildroot}

%clean
rm -rf %{buildroot}

%pre
echo "-------------"
echo "Removing old Whatfix UI"
echo "-------------"

%post

# Create the group if it doesn't exist.
if ! getent group whatfix > /dev/null 2>&1 ; then
    groupadd --system whatfix
fi

# Create the user if it doesn't exist.
if ! id whatfix > /dev/null 2>&1 ; then
    adduser --system --no-create-home -g whatfix --shell /bin/false whatfix
fi

# Give ownership to UI Files
chown -R whatfix:whatfix /var/www/whatfix/


%files
%defattr(-,root,root,-)
%config(noreplace) /var/www/whatfix
# /etc/httpd/conf.d/whatfix.com