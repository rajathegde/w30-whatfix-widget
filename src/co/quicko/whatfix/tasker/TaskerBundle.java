package co.quicko.whatfix.tasker;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.Import;

public interface TaskerBundle extends ClientBundle {
    @Source("tasker.css")
    @Import(value = { TaskerCss.class })
    @CssResource.NotStrict
    TaskerCss css();

    @Source("style.css")
    CssResource icons();
}
