package co.quicko.whatfix.tasker;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;

import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.overlay.Launcher.Settings;

public class MobileTasker extends TheTasker {

    public MobileTasker(Settings settings) {
        super(settings);
        double height = Window.getClientHeight();
        if ((int) height <= 260) {
            // 260-110
            scroller.setHeight(150 + "px");
        } else {
            scroller.setHeight(height - 110 + "px");
        }
    }

    @Override
    protected String pending(int remaining, int total) {
        remainingFlowsLabel.addStyleName(Tasker.CSS.trackerLabelMobile());
        return super.pending(remaining, total) + "/" + total;
    }

    @Override
    protected FlowPanel progressPanel() {
        FlowPanel progressPanel = new FlowPanel();
        if ("show".equalsIgnoreCase(Themer.value(Themer.TASK_LIST.PROGESS))) {
            progressPanel.add(remainingFlowsLabel);
        }
        return progressPanel;
    }

}
