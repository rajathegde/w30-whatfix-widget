package co.quicko.whatfix.tasker;

import co.quicko.whatfix.common.Common;

import com.google.gwt.core.shared.GWT;

public class Tasker {
    public static final TaskerBundle BUNDLE = GWT.create(TaskerBundle.class);
    public static final TaskerCss CSS = BUNDLE.css();
    public static final TaskerConstants CONSTANTS = GWT
            .create(TaskerConstants.class);

    static {
        Common.CSS.ensureInjected();
        CSS.ensureInjected();
    }

}
