package co.quicko.whatfix.tasker;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.PopupEntryPoint;
import co.quicko.whatfix.common.Resizer;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EnterpriseSettings;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.extension.util.ExtensionHelper;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.LaunchTasker.TaskerSettings;
import co.quicko.whatfix.overlay.Launcher.Settings;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.service.Where;

public class TaskerEntry extends PopupEntryPoint {

    private static final String prefix = "tasker";

    @Override
    public void initialize() {
        Tasker.BUNDLE.icons().ensureInjected();
        Themer.initialize();
    }

    @Override
    public Widget getWidget(String content) {
        EnterpriseSettings enterpriseSettings = DataUtil.create(content);
        GaUtil.getInteraction(enterpriseSettings);
        Enterpriser.initialize(enterpriseSettings);
        Themer.initialize(enterpriseSettings.jsTheme());
        ExtensionHelper.initialize(Enterpriser.community());
        ExtensionHelper.setPreviewMode(enterpriseSettings.isPreviewMode());
        TheTasker tasker = enterpriseSettings.isMobile()
                ? new MobileTasker(enterpriseSettings.settings())
                : new TheTasker(enterpriseSettings.settings());
        Resizer.resizeWrt(tasker, getWidth(enterpriseSettings),
                getHeight(enterpriseSettings));
        initializeGa(enterpriseSettings.settings());
        setLanguage();
        return tasker;
    }

    private int getHeight(EnterpriseSettings enterpriseSettings) {
        if (!enterpriseSettings.isMobile()) {
            return Framers.MIN_TASKER_HEIGHT;
        }
        return Window.getClientHeight();
    }

    private int getWidth(EnterpriseSettings enterpriseSettings) {
        if (!enterpriseSettings.isMobile()) {
            return Framers.MIN_TASKER_WIDTH;
        }
        return Window.getClientWidth();
    }

    private static void initializeGa(Settings settings) {
        String segmentName = (settings.segment_name() != null)
                ? settings.segment_name() : settings.label();
        GaUtil.setAnalytics(TrackerEventOrigin.TASK_LIST, settings.segment_id(),
                segmentName);
        Common.tracker().initialize(Security.unq_id());
        Common.tracker().use(Enterpriser.enterprise().ent_id(), Security.user_id(),
                Security.displayName(), Security.user_name(),
                TrackerEventOrigin.TASK_LIST.getSrcName(),
                Enterpriser.enterprise().ga_id());
        GaUtil.analyticsCache.role_tag(((TaskerSettings) settings).role_tags());
        GaUtil.src_id = TrackerEventOrigin.TASK_LIST.getSrcName();
        CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                StringUtils.stringifyObject(Where.create("type",
                        TrackerEventOrigin.TASK_LIST.toString(), "event_type",
                        "init", "segment_name",
                        GaUtil.analyticsCache.segment_name(), "segment_id",
                        GaUtil.analyticsCache.segment_id(), "role_tag",
                        GaUtil.analyticsCache.role_tag())));
    }

    @Override
    public String prefix() {
        return prefix;
    }
    
    @Override
    public void onMessage(String type, String content) {
        super.onMessage(type, content);
        Scheduler.get().scheduleDeferred(new ScheduledCommand() {

            @Override
            public void execute() {
                CrossMessager.sendMessage(mainWindow(),
                        prefix + "_frame_loaded", "");
            }
        });
    }
}
