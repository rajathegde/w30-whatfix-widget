package co.quicko.whatfix.tasker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import com.google.gwt.aria.client.Roles;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AccessibilityRoles;
import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Awesome;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.ShortcutHandler;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.ShortcutHandler.Shortcut;
import co.quicko.whatfix.data.AbstractContent;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.Content.Text;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.Group;
import co.quicko.whatfix.data.TaskerInfo;
import co.quicko.whatfix.data.TaskerInfo.TaskerData;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.data.BrandingWidgets;
import co.quicko.whatfix.i18n.client.Propertizable;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.LaunchTasker.TaskerSettings;
import co.quicko.whatfix.overlay.Launcher.Settings;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.Callbacks;
import co.quicko.whatfix.widgetbase.ContentFlowPanel;
import co.quicko.whatfix.widgetbase.TextPanel;
import co.quicko.whatfix.widgetbase.WidgetBase;
import co.quicko.whatfix.widgetbase.WidgetGroupPanel;

public class TheTasker extends WidgetBase<Flow> {
    private static final int DELAY_FOR_FOCUS = 100;
    protected Label remainingFlowsLabel;
    protected Label innerTracker;
    private TaskerInfo taskerInfo;
    private static final int WIDTH_MULTIPLIER = Tasker.CSS.outerTracker_width();
    private Boolean shortcutAccess = true;
    protected FlowPanel outerTracker;
    protected boolean showProgress;
    protected boolean showTaskSequence;
    private boolean hasActiveIncompleteTask = false;
    private boolean hasActiveIncompleteTaskInGroup = false;
    private Label title;

    private Shortcut shortCut;
    private TaskerSettings taskerSettings;
    private List<TaskListGroupPanel> groups;
    public TheTasker(Settings settings) {
        this(settings, true);
    }

    public TheTasker(Settings settings, boolean shortcutAccess) {
        super(settings, "tasker");
        this.taskerSettings = (TaskerSettings) settings;
        this.shortcutAccess = shortcutAccess;
        this.showProgress = "show"
                .equalsIgnoreCase(Themer.value(Themer.TASK_LIST.PROGESS));

        this.showTaskSequence = "enabled"
                .equalsIgnoreCase(Themer.value(Themer.TASK_LIST.SEQUENCING));
        flows.addStyleName(Tasker.CSS.taskerContentListPanel());
        
        taskerInfo = new TaskerInfo();
        Themer.applyTheme(this, Themer.STYLE.BORDER_RADIUS,
                Themer.TASK_LIST.BORDER_RADIUS, Themer.STYLE.COLOR,
                Themer.TASK_LIST.BODY_TEXT_COLOR, Themer.STYLE.LINE_HEIGHT,
                Themer.TASK_LIST.LINE_HEIGHT);
        FlowPanel header = new FlowPanel();
        setHeaderThemes(settings, header);

        String label = settings.label();
        if (label == null) {
            label = "";
            settings.label(label);
        } else {
            label = label.trim();
        }
        title = Common.label(label);
        Roles.getPresentationRole().set(getElement());
        String titleAriaLabel = "Expanded, " + label;
        title.getElement().setAttribute(AriaProperty.LABEL, titleAriaLabel);
        title.getElement().setAttribute(AriaProperty.LEVEL, "1");
        title.setStyleName(Tasker.CSS.headerTitle());
        Common.setRoleAttribute(AccessibilityRoles.HEADING,
                title.getElement());
        header.add(title);
        header.add(close);
        Roles.getPresentationRole().set(header.getElement());
        if (showTaskSequence) {
            title.getElement().setAttribute(AriaProperty.LABEL,
                    label + "," + Tasker.CONSTANTS.seqTasksEnabled());
        }

        outerTracker = new FlowPanel();
        innerTracker = Common.label("", Tasker.CSS.innerTracker());
        remainingFlowsLabel = Common.label("", Tasker.CSS.trackerLabel());

        FlowPanel progressPanel = progressPanel();
        progressPanel.setStyleName(Tasker.CSS.progressPanel());
        header.add(progressPanel);
        FlowPanel taskerContainer = new FlowPanel();

        taskerContainer.add(header);
        taskerContainer.add(container);

        brand.addStyleName(Tasker.CSS.brand());
        Widget brandContainer = null;
        if (Enterpriser.isTrialAccount(Enterpriser.ent_id()) && Enterpriser
                .isWatermarkRemovalDisabled(Enterpriser.ent_id())) {
            FlowPanel trialLogo = new FlowPanel();
            trialLogo.getElement().setInnerHTML(SVGElements.TRIAL_LOGO);
            trialLogo.setStyleName(Tasker.CSS.trialLogo());
            brandContainer = (Common.makeThreePart(trialLogo, null, brand));
        } else {
            brandContainer = (Common.makeThreePart(null, null, brand));
        }

        brandContainer.addStyleName(Tasker.CSS.noTransform());
        Roles.getPresentationRole().set(brandContainer.getElement());
        taskerContainer.add(brandContainer);
        add(taskerContainer);

        updateBranding();
        showContent(
                delegatingCb(Callbacks.<JsArray<Flow>> emptyCb(), false, true),
                false);

        close.getElement().setInnerHTML(
                getWidgetCrossIcon(Themer.value(Themer.TASK_LIST.CROSS_COLOR)));
        Themer.applyTheme(title, Themer.STYLE.COLOR,
                Themer.TASK_LIST.HEADER_TEXT_COLOR, remainingFlowsLabel,
                Themer.STYLE.COLOR, Themer.TASK_LIST.HEADER_TEXT_COLOR,
                innerTracker, Themer.STYLE.BACKGROUD_COLOR,
                Themer.TASK_LIST.HEADER_TEXT_COLOR, outerTracker,
                Themer.STYLE.BACKGROUD_COLOR,
                Themer.TASK_LIST.HEADER_TEXT_COLOR, close, Themer.STYLE.COLOR,
                Themer.TASK_LIST.CROSS_COLOR, taskerContainer, Themer.STYLE.FONT,
                Themer.FONT);
        if (shortcutAccess) {
            addShortcuts();
        }
        if (shouldGrabFocus()) {
            Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {
                @Override
                public boolean execute() {
                    title.getElement().focus();
                    return false;
                }

            }, DELAY_FOR_FOCUS);

        }
        setTabIndices(title);
        addTaskerEndElement();
        setNudgeCloseListener();
    }

    /**
     * Set the styling for tasklist header
     */
    private void setHeaderThemes(Settings settings, FlowPanel header) {
        header.setStyleName(Tasker.CSS.header());
        Themer.applyTheme(header, Themer.STYLE.BORDER_TOP_LEFT_RADIUS,
                Themer.TASK_LIST.BORDER_RADIUS,
                Themer.STYLE.BORDER_TOP_RIGHT_RADIUS,
                Themer.TASK_LIST.BORDER_RADIUS, Themer.STYLE.BACKGROUD_COLOR);
        String color = Themer.value(Themer.TASK_LIST.HEADER_COLOR,
                settings.color());
        Style headerStyle = header.getElement().getStyle();
        if (color != null) {
            if (headerStyle != null) {
                headerStyle.setProperty(OverlayUtil.BACKGROUND, color);
            } else {
                OverlayUtil.setStyle(header, OverlayUtil.BACKGROUND, color);
            }
        }
    }
    
    /**
     * Set focus on the tasklist title, on closing the nudge tooltip
     */
    private void setNudgeCloseListener() {
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                title.getElement().focus();
                CrossMessager.removeListener(this, "tasker_tooltip_close");
            }
        }, "tasker_tooltip_close");
    }

    @Override
    protected void setBranding() {
        brand.setVisible(
                Enterpriser.widgetBrandingEnabled(BrandingWidgets.TASKLIST));
    }

    private void setTabIndices(Label title) {
        // First title
        title.getElement().setTabIndex(0);
        // After that tab should go to label
        remainingFlowsLabel.getElement().setTabIndex(0);
        // Close button
        close.setTabIndex(0);
        if (showProgress) {
            Roles.getProgressbarRole().set(outerTracker.getElement());
            outerTracker.getElement().setTabIndex(0);
        }
    }

    
    private void addTaskerEndElement() {
        Element taskerEnd = DOM.createSpan();
        taskerEnd.setTabIndex(0);
        taskerEnd.addClassName(Tasker.CSS.taskerEnd());
        taskerEnd.setInnerText(Tasker.CONSTANTS.taskerEndMessage());
        DOM.appendChild(this.getElement(), taskerEnd);

        setTabHandler(taskerEnd, title.getElement());
        
        title.addDomHandler(new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_TAB
                        && event.isShiftKeyDown()) {
                    taskerEnd.focus();
                    event.preventDefault();
                }

            }
        }, KeyDownEvent.getType());
    }

    public native void setTabHandler(Element src, Element next)/*-{
                src.addEventListener('keydown',function(e){
                    if(e.keyCode == 9  && !e.shiftKey){
                       next.focus();
                       e.preventDefault();
                    }
                });                                                    
    }-*/;
    /**
     * To determine , if tasker widget should grab focus. If the tasker is
     * rendered inside dashboard for preview , it shouldn't grab. Else it
     * should. So default it's assumed to be run by Embed.
     * 
     * @return
     */
    protected boolean shouldGrabFocus() {
        return true;
    }
    protected FlowPanel progressPanel() {
        FlowPanel progressBar = new FlowPanel();
        progressBar.setStyleName(Tasker.CSS.progressBar());

        progressBar.add(outerTracker);

        FlowPanel progressPanel = new FlowPanel();

        if (showProgress) {
            outerTracker.setStyleName(Tasker.CSS.outerTracker());
            progressBar.add(innerTracker);
            progressPanel.add(remainingFlowsLabel);
        }

        progressPanel.add(progressBar);

        if (showProgress) {
            progressPanel.add(remainingFlowsLabel);
        }
        return progressPanel;
    }

    protected void addShortcuts() {
        shortCut = Themer.taskListShortcut();
        if (shortCut != null) {
            ShortcutHandler.register(shortCut, this);
        }
    }

    @Override
    public void onShortcut(Shortcut shortcut) {
        if (!shortcutAccess) {
            return;
        }
        if (shortCut != null) {
            ShortcutHandler.unregister(shortCut, this);
        }
        super.onShortcut(shortcut);
    }

    @Override
    protected BlurHandler getAnchorBlurHandler(final Widget wrapperPanel) {
        return new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                wrapperPanel.removeStyleName(Common.CSS.widgetFlowRowBackground());
                /*
                 * Below is code to fire MouseOutEvent on the wrapperPanel to
                 * apply style which is similar to style applied on mouse out of
                 * it. Passing default values to the method arguments because
                 * the sole purpose of the it for styling.
                 */
                DomEvent.fireNativeEvent(
                        Document.get().createMouseOutEvent(0, 0, 0, 0, 0,
                                false, false, false, false, 0, null),
                        wrapperPanel);
            }
        };
    }

    @Override
    protected FocusHandler getAnchorFocushandler(final Widget wrapperPanel) {
        return new FocusHandler() {
            @Override
            public void onFocus(FocusEvent event) {
                wrapperPanel.addStyleName(Common.CSS.widgetFlowRowBackground());
                /*
                 * Below is code to fire MouseOverEvent on the wrapperPanel to
                 * apply style which is similar to style applied on mouse over
                 * it.Passing default values to the method arguments because the
                 * sole purpose of the it for styling.
                 */
                DomEvent.fireNativeEvent(
                        Document.get().createMouseOverEvent(0, 0, 0, 0, 0,
                                false, false, false, false, 0, null),
                        wrapperPanel);
            }
        };
    }

    @Override
    public void showContent(final AsyncCallback<JsArray<Flow>> cb,
            boolean showCrawledContent) {
        CrossMessager.addListener(new CrossListener() {

            @Override
            public void onMessage(String type, String content) {
                TaskerData data = DataUtil.create(content);
                taskerInfo = new TaskerInfo(data, taskerSettings);
                updateRemainingLabel();
                cb.onSuccess(taskerInfo.flows());
                CrossMessager.removeListener(this, "tasks");
            }
        }, "tasks");
        CrossMessager.sendMessageToParent("send_tasks", "");
    }


    @Override
    protected boolean hideOnEscape() {
        return true;
    }

    @Override
    protected FlowPanel getGroupPanel(final Group group,
            final boolean isContentFull,
            AbstractContentPosition contentPosition) {
        final ContentFlowPanel contentPanel = new ContentFlowPanel(group,
                isContentFull);
        FlowPanel iconPanel = new FlowPanel();
        iconPanel.add(getWidgetIcon(contentPanel));
        TaskListGroupPanel groupPanel = new TaskListGroupPanel(group, iconPanel,
                scroller, contentPosition) {
            @Override
            protected void handleOpen() {
                super.handleOpen();
                // On each group open reset the incomplete flag
                resetGroupIncompleteFlag();
                addGroupContent(group, this);
            }

            @Override
            protected int getRemainingCount() {
                if (!showProgress) {
                    return -1;
                }
                if (group.isEmpty()) {
                    return 0;
                }
                int totalContent = group.content_ids().length();
                int completedContent = 0;
                JsArrayString contents = group.content_ids();
                for (int index = 0; index < totalContent; index++) {
                    if (taskerInfo.isCompleted(contents.get(index))) {
                        completedContent += 1;
                    }
                }
                int remaining = totalContent - completedContent;
                return remaining;
            }
        };

        if (groups == null) {
            groups = new ArrayList<TaskListGroupPanel>();
        }

        groups.add(groupPanel);
        groupPanel.addFocusHandler(getAnchorFocushandler(contentPanel));
        groupPanel.addBlurHandler(getAnchorBlurHandler(contentPanel));
        contentPanel.add(groupPanel);
        // TODO need to add id for the group and group contents
        // setId(content, groupPanel);
        contentPanel.addStyleName(Tasker.CSS.taskerGroupPanel());

        return contentPanel;
    }
    @Override
    protected FlowPanel getNonTextContentPanel(final Content content,
            final boolean isFlowFull, final Group group,
            AbstractContentPosition contentPosition,
            WidgetGroupPanel parentGroupPanel) {
        final ContentFlowPanel contentPanel = new ContentFlowPanel(content,
                group, isFlowFull);
        final SimplePanel iconPanel = new SimplePanel();
        contentPanel.addStyleName(Tasker.CSS.taskerGroupPanel());

        iconPanel.add(getFlowContainerIcon(contentPanel));
        contentPanel.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                iconPanel.clear();
                iconPanel.add(getWidgetIcon(contentPanel));
            }
        }, MouseOverEvent.getType());
        contentPanel.addDomHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                iconPanel.clear();
                iconPanel.add(getFlowContainerIcon(contentPanel));
            }
        }, MouseOutEvent.getType());
        contentPanel.add(iconPanel);
        SimplePanel flowPanelWrapper = new SimplePanel();
        contentPanel.add(flowPanelWrapper);
        Anchor run = Common.anchor(content.title(), false, "#", true,
                Common.CSS.widgetAnchorElement());
        Themer.applyTheme(run, Themer.STYLE.COLOR,
                Themer.TASK_LIST.BODY_TEXT_COLOR);
        contentPanel.setAnchor(run);
        run.addClickHandler(getHandler(contentPanel));

        StringBuilder builder = new StringBuilder(
                taskAriaLabelPrefix(content.flow_id()));
        builder.append(",");
        builder.append(content.type()).append(",");
        builder.append(content.title());

        Roles.getPresentationRole().set(contentPanel.getElement());
        run.getElement().setAttribute(AriaProperty.LABEL, builder.toString());
        
        if (shortcutAccess) {
            run.addFocusHandler(getAnchorFocushandler(contentPanel));
            run.addBlurHandler(getAnchorBlurHandler(contentPanel));
        }
        flowPanelWrapper.add(run);
        return contentPanel;
    }

    private String taskAriaLabelPrefix(String contentId) {
        boolean isCompleted = taskerInfo.isCompleted(contentId);
        return isCompleted ? Tasker.CONSTANTS.completed()
                : Tasker.CONSTANTS.notCompleted();

    }
    @Override
    protected FlowPanel getExpandableContentPanel(Content content, Group group,
            AbstractContentPosition contentPosition,
            WidgetGroupPanel parentGroupPanel) {
        if (content.getType() != ContentType.text) {
            return null;
        }
        final ContentFlowPanel textContentPanel = new ContentFlowPanel(content,
                group, contentPosition);
        TaskListTextPanel textPanel = new TaskListTextPanel(textContentPanel,
                taskAriaLabelPrefix(content.flow_id()));
        textPanel.addFocusHandler(getAnchorFocushandler(textContentPanel));
        textPanel.addBlurHandler(getAnchorBlurHandler(textContentPanel));
        textContentPanel.add(textPanel);
        return textContentPanel;
    }


    private void setGlass(FlowPanel panel, boolean isComplete) {
        if (hasActiveIncompleteTask) {
            panel.addStyleName(Tasker.CSS.disablePanel());
        } else if (!isComplete) {
            hasActiveIncompleteTask = !hasActiveIncompleteTask;
        }
    }
    
    private Anchor getFlowContainerIcon(ContentFlowPanel contentFlowPanel) {
        if (showProgress) {
            return getCompletionIcon(contentFlowPanel);
        } else {
            return getWidgetIcon(contentFlowPanel);
        }
    }
    
    protected Anchor getWidgetIcon(ContentFlowPanel contentFlowPanel) {
        Anchor anchor = super.getWidgetIcon(contentFlowPanel);
        disableAriaForElement(anchor);

        return anchor;
    }

    public Anchor getCompletionIcon(ContentFlowPanel contentFlowPanel) {
        if (!taskerInfo.isCompleted(contentFlowPanel.getContent().flow_id())) {
            return getWidgetIcon(contentFlowPanel);
        } else {
            Anchor identifierIcon = Common.anchor(null,
                    Tasker.CSS.tickComplete(), Common.CSS.circle());
            identifierIcon.getElement().setInnerHTML(SVGElements.CHECK_CIRCLE);
            disableAriaForElement(identifierIcon);
            return identifierIcon;
        }
    }

    protected void disableAriaForElement(Widget widget) {
        Roles.getLinkRole()
                .setAriaDisabledState(widget.getElement(), true);
        Roles.getLinkRole().setAriaHiddenState(widget.getElement(),
                true);
    }
    
    @Override
    protected String logoTitle() {
        return Tasker.CONSTANTS.poweredTitle();
    }

    @Override
    protected String poweredTitle() {
        return Tasker.CONSTANTS.poweredTitle();
    }

    @Override
    protected Label poweredLabel() {
        Label poweredLabel = Common.label(Tasker.CONSTANTS.powered(),
                Tasker.CSS.powered());
        poweredLabel.getElement().setTabIndex(0);
        return poweredLabel;
    }

    @Override
    protected String powered() {
        return Tasker.CONSTANTS.powered();
    }

    @Override
    protected String nothingFound() {
        return Tasker.CONSTANTS.nothingFound();
    }

    @Override
    protected String closeTitle() {
        return Tasker.CONSTANTS.taskerCloseTitle();
    }

    @Override
    protected Propertizable constants() {
        return Tasker.CONSTANTS;
    }

    @Override
    protected String styleWidget() {
        return Tasker.CSS.tasker();
    }

    @Override
    protected String styleScroller() {
        return Tasker.CSS.scroller();
    }

    @Override
    protected String styleContainer() {
        return Tasker.CSS.container();
    }

    @Override
    protected String styleClose() {
        return Tasker.CSS.close();
    }

    @Override
    protected String styleElement() {
        return Common.CSS.widgetAnchorElement();
    }

    @Override
    protected String styleNothing() {
        return Tasker.CSS.nothing();
    }

    @Override
    protected String brandLogoStyle() {
        return Tasker.CSS.brandLogoTasker();
    }

    private void updateTracker() {
        double percent = getFractionComplete();
        percent = (percent * WIDTH_MULTIPLIER) / 100;
        OverlayUtil.setStyle(innerTracker,
                new String[] { "width", "background-color" },
                new String[] { percent + "%",
                        Themer.value(Themer.TASK_LIST.HEADER_TEXT_COLOR) });
    }

    protected void updateRemainingLabel() {
        updateTracker();
        String pending = "";
        String suffix = "";
        int total = taskerInfo.total();
        if (total != -1) {
            int remaining = total - taskerInfo.completed();
            pending = pending(remaining, total);
            suffix = suffix(remaining);
            setProgressAriaAttributes(total, taskerInfo.completed());
        }

        remainingFlowsLabel.setText(pending + " " + suffix);
    }

    private void setProgressAriaAttributes(int total, int completed) {
        if (showProgress) {
            outerTracker.getElement().setAttribute(AriaProperty.VALUE_MIN, "0");
            outerTracker.getElement().setAttribute(AriaProperty.VALUE_MAX,
                    String.valueOf(total));
            outerTracker.getElement().setAttribute(AriaProperty.VALUE_NOW,
                    String.valueOf(completed));
        }
    }
    
    private void updatePanels() {
        int totalLength = allContents.getWidgetCount();

        for (int panelIndex = 0; panelIndex <= totalLength
                - 1; panelIndex++) {
            ContentFlowPanel panel = (ContentFlowPanel) allContents
                    .getWidget(panelIndex);
            panel.removeStyleName(Tasker.CSS.disablePanel());
            String flowId = panel.getFlowId();

            if (taskerSettings.groups().hasKey(flowId)
                    && panel.getWidget(0) != null) {
                TaskListGroupPanel tlGroupPanel = (TaskListGroupPanel) panel
                        .getWidget(0);
                if (tlGroupPanel.getRemainingCount() != 0) {
                    handleGroupUpdates(flowId, tlGroupPanel);
                    break;
                }
            } else {
                if (!taskerInfo.isCompleted(flowId)) {
                    break;
                }
            }
        }
    }

    /**
     * Iterate through content in group to sequentially show up each content in
     * it during updates
     */
    private void handleGroupUpdates(String flowId,
            TaskListGroupPanel tlGroupPanel) {
        FlowPanel groupContentPanel = tlGroupPanel.getContentPanel();
        // Groups will not have content initially, they will be fetched on
        // opening the group only, hence adding this check
        if (groupContentPanel.getWidgetCount() == 0) {
            return;
        }
        Group group = (Group) taskerSettings.groups().value(flowId);
        List<String> contents = Arrays.asList(group.contentAsArray());
        ListIterator<String> itr = contents.listIterator();
        while (itr.hasNext()) {
            int contentIndex = itr.nextIndex();
            String contentId = itr.next();
            groupContentPanel.getWidget(contentIndex)
                    .removeStyleName(Tasker.CSS.disablePanel());
            if (!taskerInfo.isCompleted(contentId)) {
                break;
            }
        }
    }

    protected String pending(int remaining, int total) {
        if (remaining <= 0) {
            return "0";
        } else if (remaining == 1) {
            return "1";
        }
        return String.valueOf(remaining);
    }

    private String suffix(int remaining) {
        if (remaining == 1) {
            return Tasker.CONSTANTS.taskListSinglePending();
        }
        return Tasker.CONSTANTS.taskListMultiplePending();
    }

    public double getFractionComplete() {
        int total = taskerInfo.total();
        int completed = taskerInfo.completed();
        if (total == 0 || completed == 0) {
            return 0;
        }

        double fractionComplete = ((completed * 100) / total);
        if (fractionComplete > 100) {
            fractionComplete = 100;
        }
        return fractionComplete;
    }

    @Override
    protected String widgetSrc() {
        return TrackerEventOrigin.TASK_LIST.getSrcName();
    }

    @Override
    protected void setId(Content content, Widget widget) {
        // TODO need to add id after discussion with product.
    }
    
    @Override
    protected FlowPanel sequence(FlowPanel panel, AbstractContent content) {
        if (showTaskSequence) {
            boolean isComplete = false;
            if (content.isGroup() && ((TaskListGroupPanel) panel.getWidget(0))
                    .getRemainingCount() == 0) {
                isComplete = true;
            } else {
                isComplete = (taskerInfo.isCompleted(
                        ((Content) content).flow_id()) ? true : false);
            }
            setGlass(panel, isComplete);
        }
        return panel;
    }

    /**
     * Sequentialize content in each group
     */
    @Override
    protected FlowPanel sequenceInGroup(FlowPanel panel, Content content) {
        if (showTaskSequence) {
            boolean isComplete = taskerInfo.isCompleted(content.flow_id());

            if (hasActiveIncompleteTaskInGroup) {
                panel.addStyleName(Tasker.CSS.disablePanel());
            } else if (!isComplete) {
                hasActiveIncompleteTaskInGroup = !hasActiveIncompleteTaskInGroup;
            } else {
                // Do nothing
            }
        }
        return panel;
    }

    /**
     * This is called when group is opened in tasklist to reset the flag to
     * false
     */
    private void resetGroupIncompleteFlag() {
        this.hasActiveIncompleteTaskInGroup = false;
    }

    private class TaskListTextPanel extends TextPanel {

        private final String PREFIX = "tasker";

        public TaskListTextPanel(ContentFlowPanel contentFlowPanel,
                String status) {
        	super(contentFlowPanel, scroller, false);
            addDomHandler(new MouseOverHandler() {
                @Override
                public void onMouseOver(MouseOverEvent event) {
                    header.setIcon(textIcon());
                }
            }, MouseOverEvent.getType());
            addDomHandler(new MouseOutHandler() {
                @Override
                public void onMouseOut(MouseOutEvent event) {
                    header.setIcon(getHeaderIcon());
                }
            }, MouseOutEvent.getType());
            header.addStyleName(Tasker.CSS.taskerTextHeaderPanel());
            header.getWidget(0, 1).getElement()
                    .setAttribute(AriaProperty.HIDDEN, Boolean.toString(true));
            header.getParent().getElement().setAttribute(AriaProperty.LABEL,
                    Common.CONSTANTS.collapsed() + "," + status + ","
                            + contentFlowPanel.getContent().getType() + ", "
                            + contentFlowPanel.getContent().title());
            Roles.getPresentationRole().set(header.getElement());
            Themer.applyTheme(header.getWidget(0, 1),
                    Themer.STYLE.COLOR, Themer.TASK_LIST.BODY_TEXT_COLOR);
        }
        
        @Override
        protected Widget getHeaderIcon() {
            if (showProgress
                    && taskerInfo.isCompleted(((Content) content).flow_id())) {
                return completionIcon();
            }
            return super.getHeaderIcon();
        }
        
        private Widget completionIcon() {
            Anchor identifierIcon = Common.anchor(null, Tasker.CSS.tickComplete(), Common.CSS.circle());
            identifierIcon.getElement().setInnerHTML(SVGElements.CHECK_CIRCLE);
            return identifierIcon;
        }
        
        @Override
        protected void trackOpen() {
            super.trackOpen();
            taskerInfo.setCompleted(((Content) content).flow_id());
            if (groups != null) {
                for (TaskListGroupPanel groupPanel : groups) {
                    groupPanel.updateRemaining();
                }
            }
            updateRemainingLabel();
            if (showTaskSequence) {
                updatePanels();
            }
        }

        @Override
        protected String prefix(String type) {
            return PREFIX + type;
        }
        
        @Override
        protected String widgetSrc() {
        	return TrackerEventOrigin.TASK_LIST.getSrcName();
        }
        
    }
}
