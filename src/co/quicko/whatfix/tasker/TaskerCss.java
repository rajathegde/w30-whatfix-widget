package co.quicko.whatfix.tasker;

import com.google.gwt.resources.client.CssResource;

public interface TaskerCss extends CssResource {
    public String tasker();

    public String header();

    public String close();

    public String headerTitle();

    public String progressBar();

    public String outerTracker();

    public String innerTracker();

    public String trackerLabel();

    public String scroller();

    public String container();

    public String element();

    public String tickComplete();

    public String powered();

    public String brand();

    public String nothing();

    public int outerTracker_width();
    
    public String rowBackground();

    public String noTransform();
    
    public String brandLogoTasker();

    public String trackerLabelMobile();

    public String progressPanel();

    public String elementFull();
    
    public String taskerContentListPanel();
    
    public String taskerGroupPanel();
    
    public String taskerGroupCompleteIcon();
    
    public String taskerHeaderText();
    
    public String taskerTextHeaderPanel();
    
    public String disablePanel();
    
    public String taskerEnd();

    public String widgetGroupContentPanel();

    public String trialLogo();
}
