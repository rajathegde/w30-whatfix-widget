package co.quicko.whatfix.tasker;

import co.quicko.whatfix.i18n.client.Propertizable;

public interface TaskerConstants extends Propertizable {
    @DefaultStringValue("whatfix.com")
    public String poweredTitle();

    @DefaultStringValue("powered by")
    public String powered();

    @DefaultStringValue("nothing found")
    public String nothingFound();

    @DefaultStringValue("Close")
    public String taskerCloseTitle();

    @DefaultStringValue("Completed")
    public String completed();

    @DefaultStringValue("Not completed")
    public String notCompleted();

    @DefaultStringValue("Sequence tasks are enabled")
    public String seqTasksEnabled();

    @DefaultStringValue("Tasker Content ends.")
    public String taskerEndMessage();

    @DefaultStringValue("Task")
    public String task();

    @DefaultStringValue("Tasks")
    public String tasks();
    
    @DefaultStringValue("Task pending")
    public String taskListSinglePending();

    @DefaultStringValue("Tasks pending")
    public String taskListMultiplePending();
    
}
