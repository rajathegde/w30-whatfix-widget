package co.quicko.whatfix.tasker;

import com.google.gwt.aria.client.Roles;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.Awesome;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.data.AbstractContent;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.widgetbase.WidgetBase.AbstractContentPosition;
import co.quicko.whatfix.widgetbase.WidgetGroupPanel;

public class TaskListGroupPanel extends WidgetGroupPanel {

    public TaskListGroupPanel(AbstractContent content, FlowPanel headerIcon,
            ScrollPanel scroller, AbstractContentPosition contentPosition) {
        super(content, headerIcon, contentPosition, scroller);
        Roles.getPresentationRole().set(getElement());
    }
    
    protected int getRemainingCount() {
        return 0;
    }
    
    @Override
    protected Widget getHeader() {
        header = new TaskListGroupHeader(getContentTitle());
        setHeaderIcon();
        return header;
    }
    
    @Override
    protected Widget getHeaderIcon() {
        if (getRemainingCount() != 0) {
            return super.getHeaderIcon();
        }
        FlowPanel iconPanel = (FlowPanel) super.getHeaderIcon();
        iconPanel.addStyleName(Tasker.CSS.taskerGroupCompleteIcon());
        SimplePanel completeIcon = new SimplePanel();
        completeIcon.setStyleName(Awesome.OK);
        iconPanel.add(completeIcon);
        return iconPanel;
    }
    
    public void updateRemaining() {
        setHeader();
        addHeaderStyle(Common.CSS.widgetGroupHeader());
    }

    @Override
    protected com.google.gwt.user.client.ui.Widget getContent() {
        groupContentPanel = new FlowPanel();
        groupContentPanel.addStyleName(
                Tasker.CSS.widgetGroupContentPanel());
        groupContentPanel.getElement().setAttribute("data-wfx-id", "group-content");
        return groupContentPanel;
    }


    private class TaskListGroupHeader extends Header {

        public TaskListGroupHeader(String header) {
            super(header);
            Roles.getPresentationRole().set(getElement());
        }

        @Override
        protected Widget getHeaderTextWidget(String header) {
            int remaining = getRemainingCount();
            if (remaining <= 0) {
                return new HTML(header);
            }
            String postFix = remaining > 1 ? Tasker.CONSTANTS.tasks()
                    : Tasker.CONSTANTS.task();
            String remainingTasks = "<span> (" + remaining + " " + postFix
                    + ") </span>";
            HTML headerText = new HTML(header + remainingTasks);
            headerText.setStyleName(Tasker.CSS.taskerHeaderText());
            headerText.addStyleName(Common.CSS.widgetAnchorElement());
            Themer.applyTheme(headerText, Themer.STYLE.COLOR,
                    Themer.TASK_LIST.BODY_TEXT_COLOR);
            return headerText;
        }

    }
}
