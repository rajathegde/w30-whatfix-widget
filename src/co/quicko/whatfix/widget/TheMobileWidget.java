package co.quicko.whatfix.widget;

import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import com.google.gwt.user.client.Element;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.overlay.Launcher.SelfHelpWidgetSettings;

public class TheMobileWidget extends SelfHelpWidget {
    public TheMobileWidget(SelfHelpWidgetSettings settings) {
        super(settings);
    }

    @Override
    protected void focusOnWidget() {
    }

    @Override
    protected void setBorderRadius(Element element, String position) {
        element.addClassName(Common.CSS.radiusAll());
    }

    @Override
    protected void correctBorders(String position, String relative_to) {
    }

    @Override
    protected String styleSearchPanel() {
        if (Enterpriser.hasFeature(AdditionalFeatures.sh_google_drive)) {
            return Widget.CSS.searchPanelWithTab();
        } else {
            return Widget.CSS.mobileSearchPanel();
        }
    }

    @Override
    protected void addShortcuts() {
    }
}
