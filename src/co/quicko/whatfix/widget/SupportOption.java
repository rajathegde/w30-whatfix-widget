package co.quicko.whatfix.widget;

class SupportOption {
    private final String icon;
    private final String urlPrefix;
    private final String title;

    public SupportOption(String title, String icon) {
        this(title, icon, "");
    }

    public SupportOption(String title, String icon, String urlPrefix) {
        this.icon = icon;
        this.urlPrefix = urlPrefix;
        this.title = title;
    }

    public String getIcon() {
        return this.icon;
    }

    public String getUrlPrefix() {
        return this.urlPrefix;
    }

    public String getTitle() {
        return this.title;
    }
}
