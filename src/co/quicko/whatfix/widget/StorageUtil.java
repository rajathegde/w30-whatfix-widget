package co.quicko.whatfix.widget;

import co.quicko.whatfix.common.StringUtils;
import com.google.gwt.storage.client.Storage;

import java.util.HashMap;
import java.util.Map;

public class StorageUtil {
    private static Storage localStorage = Storage.getLocalStorageIfSupported();
    private static Map<String, String> inMemoryStorage = new HashMap<>();

    public static String getItem(String key) {
        if (localStorage != null) {
            return localStorage.getItem(key);
        } else {
            return inMemoryStorage.get(key);
        }
    }

    public static boolean containsItem(String key) {
        if (localStorage != null) {
            return StringUtils.isNotBlank(localStorage.getItem(key));
        } else {
            return inMemoryStorage.containsKey(key);
        }
    }

    public static void setItem(String key, String value) {
        if (localStorage != null) {
            localStorage.setItem(key, value);
        } else {
            inMemoryStorage.put(key, value);
        }
    }
}
