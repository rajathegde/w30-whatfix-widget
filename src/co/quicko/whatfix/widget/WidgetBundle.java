package co.quicko.whatfix.widget;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.Import;
import com.google.gwt.resources.client.ImageResource;

public interface WidgetBundle extends ClientBundle {
    @Source("widget.css")
    @Import(value = { WidgetCss.class })
    WidgetCss css();

    @Source("style.css")
    CssResource icons();
    
    @Source("getStarted.png")
    ImageResource getStarted();
}