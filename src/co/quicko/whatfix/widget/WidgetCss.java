package co.quicko.whatfix.widget;

import com.google.gwt.resources.client.CssResource;

public interface WidgetCss extends CssResource {
    String widget();

    String header();

    String headerTitle();

    String close();

    String searchPanel();

    String mobileSearchPanel();

    String searchGrid();

    String searchGridProgress();

    String searchIconCell();

    String searchIconCellWithTab();

    String search();

    String searchLabel();

    String searchLabelNoHeader();

    String searchClear();

    String closeBesideSearch();

    String closePanel();

    String onlyCloseIcon();

    String scroller();

    String container();

    String progressor();

    String brandPanel();

    String noBrandPanel();

    String powered();

    String nothing();

    String shareEmail();

    String shareAnonymously();

    String clickableText();

    String shareFeedbackGrid();

    String nothingFound();

    String nothingFoundLabel();

    String toggleButton();

    String noBorderTop();

    String noBorderLeft();

    String noBorderRight();

    String noBorderBottom();

    String widgetIcon();

    String iconContainer();

    String nothingWithImage();

    String editSegmentAnchor();

    String marginBottom20();

    String newSegmentAnchor();

    String flowEmptyLabel();

    String videoLauncher();

    String nothingFoundWithVideo();

    String textIcon();

    String hideSelfHelpDummySpan();

    String brand();

    String tabPanel();

    String searchGridWithTab();

    String searchPanelWithTab();

    String containerwithTab();

    String heading();

    String boldText();

    String feedbackTextArea();

    String sendFeedBack();

    String emailTextBox();

    String trialLogoForSelfHelp();

    String marginTrial();

    String containerForTrial();

    String footerPanel();

    String scrollableNothingFound();

    String thankYouPanel();
}
