package co.quicko.whatfix.widget;

import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.StringUtils;

import co.quicko.whatfix.data.BeanFactory;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.Token;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.service.GoogleAuthManager;
import co.quicko.whatfix.service.WfxInfo;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;


public class GoogleDriveHelper {

    private GoogleDriveHelper() {
    }


    private static final String ACCESS_TOKEN = "_wfx_GDrive_access_token";
    private static final String REFRESH_TOKEN = "_wfx_GDrive_refresh_token";
    private static final String ACCESS_TOKEN_EXPIRY_TIME = "_wfx_GDrive_access_token_will_expire";

    private static final GoogleAuthManager googleAuthManager = GWT.create(GoogleAuthManager.class);
    private static final BeanFactory beanFactory = GWT.create(BeanFactory.class);

    private static final String AUTHORIZE_URL = "accounts.google.com/o/oauth2/auth";
    private static final String SCOPED = "https://www.googleapis.com/auth/drive.metadata.readonly";
    private static final String REDIRECT_URL = "https://"
            + WfxInfo.getDomain(WfxInfo.Domains.API)
            + "/googleDrive.html";


    private static final String CLIENT_ID = "401999501299-9qg0ssaapsn7roqd3slfmrshe4ndvr4i.apps.googleusercontent.com";

    public static String query;
    public static AsyncCallback<JsArray<Content>> cb;

    public static boolean presentAccessToken() {
        return StorageUtil.containsItem(ACCESS_TOKEN);
    }

    public static String getAccessToken() {
        return StorageUtil.getItem(ACCESS_TOKEN);
    }

    public static String getRefreshToken() {
        return StorageUtil.getItem(REFRESH_TOKEN);
    }

    public static boolean presentRefreshToken() {
        return StorageUtil.containsItem(REFRESH_TOKEN);
    }

    public static boolean isLiveAccessToken() {
        if (!StorageUtil.containsItem(ACCESS_TOKEN_EXPIRY_TIME)) return false;
        return System.currentTimeMillis() < Long.parseLong(StorageUtil.getItem(ACCESS_TOKEN_EXPIRY_TIME));
    }

    public static boolean isActiveAccessToken() {
        return presentAccessToken() && isLiveAccessToken();
    }

    public static void storeBothToken(String code) {
        googleAuthManager.getBothToken(code, REDIRECT_URL, new AsyncCallback<JavaScriptObject>() {
            @Override
            public void onFailure(Throwable throwable) {
                Console.log(throwable.getMessage());
            }

            @Override
            public void onSuccess(JavaScriptObject jsonToken) {
                Token token = AutoBeanCodex.decode(beanFactory,
                        Token.class, new JSONObject(jsonToken).toString()).as();
                StorageUtil.setItem(ACCESS_TOKEN, token.getAccess_token());
                StorageUtil.setItem(REFRESH_TOKEN, token.getRefresh_token());
                StorageUtil.setItem(ACCESS_TOKEN_EXPIRY_TIME, Long.toString(secondToMillis(token.expiresIn())
                        + System.currentTimeMillis()));
                googleAuthManager.getResults(query, token.getAccess_token(), cb, StringUtils.isBlank(query));
            }
        });
    }

    public static void refreshAccessToken() {
        googleAuthManager.refreshAccessToken(getRefreshToken(), new AsyncCallback<JavaScriptObject>() {
            @Override
            public void onFailure(Throwable throwable) {
                Console.log(throwable.getMessage());
            }

            @Override
            public void onSuccess(JavaScriptObject jsonToken) {
                Token token = AutoBeanCodex.decode(beanFactory,
                        Token.class, new JSONObject(jsonToken).toString()).as();
                StorageUtil.setItem(ACCESS_TOKEN, token.getAccess_token());
                StorageUtil.setItem(ACCESS_TOKEN_EXPIRY_TIME, Long.toString(secondToMillis(token.expiresIn())
                        + System.currentTimeMillis()));
                googleAuthManager.getResults(query, token.getAccess_token(), cb, StringUtils.isBlank(query));
            }
        });
    }

    public static void refreshBothTokens() {
        final int half = 2;

        final double width = OverlayUtil.screenWidth() * (3.0 / 5);
        final double height = OverlayUtil.screenHeight() * (3.0 / 5);

        String url = new UrlBuilder().setHost(AUTHORIZE_URL)
                .setProtocol("https")
                .setParameter("client_id", CLIENT_ID)
                .setParameter("redirect_uri", REDIRECT_URL)
                .setParameter("scope", SCOPED)
                .setParameter("response_type", "code")
                .setParameter("access_type", "offline")
                .setParameter("approval_prompt", "force").buildString();

        String left = Double.toString((OverlayUtil.screenWidth() / half) - (width / half));
        String top = Double.toString((OverlayUtil.screenHeight() / half) - (height / half));

        Window.open(url, "_blank", "enabled" +
                ",width=" + width +
                ",height=" + height +
                ",top=" + top +
                ",left=" + left);

    }

    public static boolean areTokenUpToDate() {
        return GoogleDriveHelper.presentAccessToken() && GoogleDriveHelper.isLiveAccessToken();
    }

    public static void getResults(String searchQuery, final AsyncCallback<JsArray<Content>> callback) {
        googleAuthManager.getResults(searchQuery, getAccessToken(), callback,
                StringUtils.isBlank(searchQuery));
    }

    private static long secondToMillis(int second) {
        final int multiplier = 1000;
        return second * (long) multiplier;
    }
}
