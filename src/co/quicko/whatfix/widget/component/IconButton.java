package co.quicko.whatfix.widget.component;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;

public class IconButton extends Composite {
    interface TabHeaderUiBinder extends UiBinder<FocusPanel, IconButton> {
    }

    private static TabHeaderUiBinder ourUiBinder = GWT.create(TabHeaderUiBinder.class);

    interface InlineStyles extends CssResource {

        String iconVertical();

        String container();

        String innerContainer();

        String button();

        String icon();

        String active();
    }

    @UiField
    InlineStyles style;

    @UiField
    HTMLPanel container;

    @UiField
    FocusPanel focusablePanel;

    @UiField
    Label icon;

    @UiField
    Button text;


    public IconButton(String text, String svg, boolean isVertical) {
        initWidget(ourUiBinder.createAndBindUi(this));

        if (isVertical) {
            this.container.addStyleName(style.iconVertical());
        }
        this.text.setText(text);
        this.icon.getElement().setInnerHTML(svg);
    }

    public void addClickListener(ClickHandler handler) {
        focusablePanel.addClickHandler(handler);
    }

    public void click() {
        text.click();
    }

    public void focus() {
        focusablePanel.addStyleName(style.active());
    }

    public void blur() {
        focusablePanel.removeStyleName(style.active());

    }
}
