package co.quicko.whatfix.widget.component;

import co.quicko.whatfix.common.SVGElements;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;

public class LinkGoogleDrive extends Composite {
    interface LinkGoogleDriveUiBinder extends UiBinder<HTMLPanel, LinkGoogleDrive> {
    }

    private static LinkGoogleDriveUiBinder ourUiBinder = GWT.create(LinkGoogleDriveUiBinder.class);


    @UiField()
    Label confidentialIcon;

    @UiField()
    Label googleDriveLogo;

    @UiField()
    Label linkIcon;

    private final ClickHandler handler;

    public LinkGoogleDrive(ClickHandler onClickHandler) {
        initWidget(ourUiBinder.createAndBindUi(this));

        confidentialIcon.getElement().setInnerHTML(SVGElements.CONFIDENTIAL_SHIELD);
        googleDriveLogo.getElement().setInnerHTML(SVGElements.G_DRIVE);
        linkIcon.getElement().setInnerHTML(SVGElements.ATTACHMENT);

        this.handler = onClickHandler;
    }


    @UiHandler({"linkIcon", "linkGoogleDrive"})
    void handleClick(ClickEvent e) {
        if (handler != null) {
            handler.onClick(e);
        }
    }
}
