package co.quicko.whatfix.widget;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gwt.aria.client.Roles;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import co.quicko.whatfix.common.AccessibilityRoles;
import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.AriaPropertyValues;
import co.quicko.whatfix.common.Awesome;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.Common.IWidget;
import co.quicko.whatfix.common.Common.ImageProgressor;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.ShortcutHandler;
import co.quicko.whatfix.common.ShortcutHandler.Shortcut;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.ToggleCheckbox;
import co.quicko.whatfix.common.WcagUtils;
import co.quicko.whatfix.common.contextualinfo.ContentSource;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo.OuterKey;
import co.quicko.whatfix.common.contextualinfo.PositionInfo;
import co.quicko.whatfix.common.contextualinfo.ScrollInfo;
import co.quicko.whatfix.data.BrandingWidgets;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Group;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.i18n.client.Propertizable;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.Launcher.SelfHelpWidgetSettings;
import co.quicko.whatfix.overlay.Launcher.Settings;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.overlay.data.AppUser.PersonalisationAttributes;
import co.quicko.whatfix.overlay.data.EndUser;
import co.quicko.whatfix.overlay.data.WfxData;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.SearchFeedbackService;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.widget.component.LinkGoogleDrive;
import co.quicko.whatfix.widgetbase.ContentFlowPanel;
import co.quicko.whatfix.widgetbase.TextPanel;
import co.quicko.whatfix.widgetbase.WidgetBase;
import co.quicko.whatfix.widgetbase.WidgetGroupPanel;
import co.quicko.whatfix.workflowengine.app.AppFactory;

public class SelfHelpWidget extends WidgetBase<Content> implements IWidget<Content> {

    private static final String WFX_SEARCH_QUERY_KEY = "{wfx_search_query}";

    public static final String EVENT_TYPE = "event_type";
    public static final String SRC_ID = "src_id";
    public static final String ACTIVE_TAB = "active_tab";

    protected SelfHelpWidgetSearch search;
    private boolean hasHeader = false;
    private boolean hasSearch = false;
    private boolean hasClose = true;
    private boolean hasBrand = true;

    private FlowPanel header;
    protected FlowPanel searchOuterPanel;
    private FlowPanel searchPanel;
    private FlowPanel brandContainer;
    private Panel brandPanel;
    private FlowPanel closePanel;
    private SelfHelpWidget shWidget;
    private String searchQuery;
    protected boolean isDashboardView;
    private List<String> queriesForSession;


    private boolean feedbackProvided = false;
    private boolean emaildProvived = false;

    // Needed for WCAG, a span in aria-live region to read out the change
    // whenever search yields successful result
    private static final String SEARCH_SPAN_ID = "sh-search-count-span";
    private static final String ID = "id";
    private Element flowListCount;

    // 400 - (4px[container-padding] + 2px[border])
    private int availableHeight = 394;
    private int trialContainerHeight = 30;

    private Shortcut shortCut;
    private Map<String, SupportOption> supportOptions;
    protected SelfHelpWidgetSettings settings;

    TabPanel tabPanel;
    TabPanel footer;

    public SelfHelpWidget(SelfHelpWidgetSettings settings) {
        super(settings, "widget");
        this.settings = settings;
        shWidget = this;
        CrossMessager.addListener((CrossMessager.CrossListener) (String type, String content) -> GoogleDriveHelper.storeBothToken(content)
                , "code");
        setBorderRadius(getElement(), settings.position());
        correctBorders(settings.position(), settings.target());
        header = new FlowPanel();
        header.getElement().setAttribute(DATA_WFX_ID, "self-help-header");
        header.addStyleName(Widget.CSS.header());
        Roles.getPresentationRole().set(getElement());
        getElement().setAttribute(DATA_WFX_ID,"wfx-self-help-widget");
        setDashboardPreview();
        queriesForSession = new ArrayList<>();

        String title = settings.title();
        if (title == null) {
            title = Widget.CONSTANTS.widgetTitle(false);
            settings.title(title);
        }
        if (title != null) {
            title = title.trim();
            if (title.length() > 0) {
                if ("show".equalsIgnoreCase(
                        Themer.value(Themer.SELFHELP.WID_HEADER_SHOW))) {
                    hasHeader = true;
                    Label titleLabel = Common.label(title);
                    titleLabel.setStyleName(Widget.CSS.headerTitle());
                    titleLabel.getElement().setTabIndex(0);
                    Common.setRoleAttribute(AccessibilityRoles.HEADING,
                            titleLabel.getElement());
                    titleLabel.getElement().setAttribute(AriaProperty.LEVEL,
                            "1");
                    titleLabel.getElement().setAttribute(AriaProperty.LABEL,
                            Common.CONSTANTS.expanded() + "," + title);
                    titleLabel.getElement().setAttribute(DATA_WFX_ID, "self-help-title");
                    header.add(titleLabel);
                    header.add(close);
                    close.getElement().setAttribute(AriaProperty.LABEL,
                            Widget.CONSTANTS.widgetCloseTitle() + ", " + title);
                    add(header);
                }
            }
        }

        if ("hide".equalsIgnoreCase(
                Themer.value(Themer.SELFHELP.WID_HEADER_CLOSE_SHOW))) {
            hasClose = false;
        }

        if ("hide".equalsIgnoreCase(
                Themer.value(Themer.SELFHELP.WID_FOOTER_BRAND))) {
            hasBrand = false;
        }
        searchOuterPanel = new FlowPanel();
        searchPanel = new FlowPanel();
        searchPanel.getElement().setAttribute(DATA_WFX_ID,"self-help-search-panel");

        if (Enterpriser.hasFeature(AdditionalFeatures.sh_google_drive)) {
            tabPanel = new TabPanel(false, widgetSrc(), TabPanel.Tabs.TOP_HELP, TabPanel.Tabs.GOOGLE_DRIVE);
        } else {
            tabPanel = new TabPanel(false, widgetSrc(), TabPanel.Tabs.TOP_HELP);
        }

        tabPanel.addClickHandler(TabPanel.Tabs.TOP_HELP, (event) -> {
            allContents.clear();
            contentsInWidget = 0;
            flows.clear();
            FlowPanel nothingFoundPanel = new FlowPanel();
            nothingFoundPanel.addStyleName(Widget.CSS.nothingFound());
            Label nothingFoundLabel = Common.label("", styleNothing());
            Awesome.spin(nothingFoundLabel);
            setResultsCount(contentsInWidget);
            nothingFoundPanel.add(nothingFoundLabel);
            flows.add(nothingFoundPanel);

            showSearchSuggest(search.query(), search);
        });

        tabPanel.addClickHandler(TabPanel.Tabs.GOOGLE_DRIVE, (event) -> {
            onGoogleDriveTabActive(search.query(), this.search);
        });
        add(searchOuterPanel);
        addResultCountSpan();

        closePanel = new FlowPanel();
        add(closePanel);
        WcagUtils.setTabIndexIfWcagEnabled(close.getElement(), 0);
        close.getElement().setInnerHTML(getWidgetCrossIcon(
                Themer.value(Themer.SELFHELP.WID_CLOSE_BG_COLOR)));
        add(container);

        if (Enterpriser.hasFeature(AdditionalFeatures.chat_bot_tab)) {
            createAndAddFooter();
        }
        brandContainer = new FlowPanel();

        final Panel noBrandPanel = new FlowPanel();
        noBrandPanel.addStyleName(Widget.CSS.noBrandPanel());
        FlowPanel trialLogo = new FlowPanel();
        trialLogo.getElement().setInnerHTML(SVGElements.TRIAL_LOGO);
        trialLogo.setStyleName(Widget.CSS.marginTrial());

        brandContainer.add(noBrandPanel);
        brandContainer.getElement().setAttribute(AriaProperty.LABEL,
                Widget.CONSTANTS.selfHelpEndMessage());

        if (hasBrand) {
            if (Enterpriser.isTrialAccount(Enterpriser.ent_id()) && Enterpriser
                    .isWatermarkRemovalDisabled(Enterpriser.ent_id())) {
                com.google.gwt.user.client.ui.Widget trialContainer = (Common
                        .makeThreePart(trialLogo, null, brandContainer));
                trialContainer.addStyleName(Widget.CSS.trialLogoForSelfHelp());
                Themer.applyTheme(trialContainer, Themer.STYLE.BACKGROUD_COLOR,
                        Themer.SELFHELP.WIDGET_COLOR);
                add(trialContainer);
            } else {
                add(brandContainer);
            }
        }

        List<com.google.gwt.user.client.ui.Widget> additionalComponents = additionalComponentsToAdd();
        for (com.google.gwt.user.client.ui.Widget widget : additionalComponents) {
            add(widget);
        }

        brand.addStyleName(Widget.CSS.brand());
        brandPanel = getBrandPanel();
        Roles.getPresentationRole().set(brandPanel.getElement());

        // for support options
        supportOptions = new LinkedHashMap<String, SupportOption>();
        supportOptions.put(Themer.SELFHELP.SUPPORT_EMAIL,
                new SupportOption("Email", SVGElements.SUPPORT_EMAIL, "mailto:"));
        supportOptions.put(Themer.SELFHELP.SUPPORT_CHAT,
                new SupportOption("Chat", SVGElements.SUPPORT_CHAT));
        supportOptions.put(Themer.SELFHELP.SUPPORT_TICKET,
                new SupportOption("Raise a Ticket", SVGElements.SUPPORT_TICKET));

        if (isPersonalisationEnabled()) {
            getPersonalisationConfigData(Promise.<Boolean>define()
                    .then(r -> showContentAndRelayout())
                    .error(e -> showContentAndRelayout()));
        } else {
            showContentAndRelayout();
        }


        Themer.applyTheme(shWidget, Themer.STYLE.BORDER_COLOR,
                Themer.SELFHELP.WIDGET_COLOR, close, Themer.STYLE.COLOR,
                Themer.SELFHELP.WID_CLOSE_BG_COLOR);
        Themer.applyTheme(brandContainer, Themer.STYLE.BACKGROUD_COLOR,
                Themer.SELFHELP.WIDGET_COLOR, header,
                Themer.STYLE.BACKGROUD_COLOR, Themer.SELFHELP.WIDGET_COLOR,
                Themer.STYLE.COLOR, Themer.SELFHELP.WID_HEADER_TEXT_COLOR);
        Themer.applyTheme(poweredLabel, Themer.STYLE.COLOR,
                Themer.SELFHELP.WID_HEADER_TEXT_COLOR, logo, Themer.STYLE.COLOR,
                Themer.SELFHELP.WID_HEADER_TEXT_COLOR);
        addShortcuts();
    }

    private void createAndAddFooter() {
        footer = new TabPanel(true, widgetSrc(), TabPanel.Tabs.HOME, TabPanel.Tabs.CHAT);
        add(footer.getPanel());

        footer.getPanel().addStyleName(Widget.CSS.footerPanel());

        Frame chatBotPanel = Framers.Framer
                .build("100%", "100px");
        chatBotPanel.setUrl(settings.chat_bot().valueAsString("url"));

        footer.addClickHandler(TabPanel.Tabs.CHAT, (event) -> {
            if (!chatBotPanel.isAttached()) {
                int height = searchPanel.getElement().getClientHeight()
                        + tabPanel.getPanel().getElement().getClientHeight()
                        + container.getElement().getClientHeight();
                chatBotPanel.setHeight(height + "px");
                container.removeFromParent();
                searchOuterPanel.removeFromParent();
                insert(chatBotPanel, getWidgetIndex(footer.getPanel()));
            } else {
                container.removeFromParent();
                searchOuterPanel.removeFromParent();
                chatBotPanel.addStyleName(Overlay.CSS.displayBlock());
                chatBotPanel.setVisible(true);
            }
        });

        footer.addClickHandler(TabPanel.Tabs.HOME, (event) -> {
            // removing from parent discards the iframe and destroys the state
            // that is why just hiding it here.
            chatBotPanel.setVisible(false);
            chatBotPanel.removeStyleName(Overlay.CSS.displayBlock());
            insert(searchOuterPanel, getWidgetIndex(footer.getPanel()));
            insert(container, getWidgetIndex(footer.getPanel()));
        });
    }

    private void showContentAndRelayout() {
        showContent(Promise.<JsArray<Content>>define()
        .then(res -> {
            updateBranding();
            relayout();
        }).error(err -> relayout()), true);
    }

    private void setDashboardPreview() {
        setDashboardView();
    }

    private void addResultCountSpan() {
        flowListCount = DOM.createSpan();
        flowListCount.setAttribute(ID, SEARCH_SPAN_ID);
        flowListCount.setTabIndex(-2);
        flowListCount.addClassName(Widget.CSS.hideSelfHelpDummySpan());
        FlowPanel countDiv = new FlowPanel();
        countDiv.getElement().appendChild(flowListCount);
        countDiv.getElement().setAttribute(AriaProperty.LIVE,
                AriaPropertyValues.POLITE);
        searchPanel.add(countDiv);
    }

    @Override
    protected void postFlowsSetup() {
        setResultsCount(contentsInWidget);
        Element selfHelpEnd = DOM.createSpan();
        selfHelpEnd.addClassName(Widget.CSS.hideSelfHelpDummySpan());
        selfHelpEnd.setAttribute(AriaProperty.HIDDEN, String.valueOf(true));

        /*
        These are temp fixes for Covid-19 and will be removed once the pandemic is over.
        These will only come into effect when a field called covid19 is present in
        _wfx_widget.
        When deleting this, remove the 3 methods below this as well and relevant code
        from Launcher and Enterpriser where we read the settings
         */
        if (isCovidSegmentApplicable() && tabPanel.isActive(TabPanel.Tabs.TOP_HELP)) {
            Group content = (Group) Group.createObject();
            content.type("group");
            content.title(settings.fixedContent()
                    .valueAsString("title"));
            content.flow_id(settings.fixedContent()
                    .valueAsString("title"));
            content.ent_id(settings.ent_id());

            allContents.insert(getCovidGroupPanel(content, false, new AbstractContentPosition(0, 0)), 0);
        }
        DOM.appendChild(allContents.getElement(), selfHelpEnd);
    }

    @Override
    protected boolean isNewLabelApplicable(String contentId,
            Settings settings) {
        if (tabPanel.isActive(TabPanel.Tabs.GOOGLE_DRIVE)) {
            return false;
        }
        return super.isNewLabelApplicable(contentId, settings);
    }

    private boolean isCovidSegmentApplicable() {
        return settings.fixedContent() != null && StringUtils.isBlank(searchQuery) &&
                StringUtils.isNotBlank(settings.fixedContent().valueAsString("tag_id")) &&
                StringUtils.isNotBlank(settings.fixedContent().valueAsString("title"));
    }

    private FlowPanel getCovidGroupPanel(final Group group,
                                      final boolean isContentFull,
                                      AbstractContentPosition contentPosition) {
        final ContentFlowPanel contentPanel = new ContentFlowPanel(group,
                isContentFull);
        FlowPanel iconPanel = new FlowPanel();
        iconPanel.add(getWidgetIcon(contentPanel));
        WidgetGroupPanel groupPanel = new WidgetGroupPanel(group, iconPanel,
                contentPosition, scroller) {
            @Override
            protected void handleOpen() {
                super.handleOpen();
                this.handlePostOpen();
            }
        };

        addCovidGroupContent(group, groupPanel);
        groupPanel.addFocusHandler(getAnchorFocushandler(contentPanel));
        groupPanel.addBlurHandler(getAnchorBlurHandler(contentPanel));
        groupPanel.getElement().setAttribute(AriaProperty.LIVE,
                AriaPropertyValues.OFF);
        Roles.getPresentationRole().set(getElement());
        contentPanel.add(groupPanel);
        return contentPanel;
    }

    private void addCovidGroupContent(final Group group,
                                   final WidgetGroupPanel groupPanel) {

        final FlowPanel groupContentPanel = groupPanel.getContentPanel();
        String[] type = new String[] {"tag_ids"};
        String[] values = new String[] {settings.fixedContent().valueAsString("tag_id")};

        manager.groupContents(type, values, group.content_ids(),
                getGroupContentHandler(group, groupPanel, groupContentPanel));

    }

    protected List<com.google.gwt.user.client.ui.Widget> additionalComponentsToAdd() {
        return new ArrayList<com.google.gwt.user.client.ui.Widget>();
    }

    protected void addShortcuts() {
        shortCut = Themer.widgetShortcut();
        if (shortCut != null) {
            ShortcutHandler.register(shortCut, this);
        }
    }

    @Override
    public void onShortcut(Shortcut shortcut) {
        if (shortCut != null) {
            ShortcutHandler.unregister(shortCut, this);
        }
        super.onShortcut(shortcut);
    }

    @Override
    protected void updateBranding() {
        super.updateBranding();
        if (brand.isVisible()) {
            brandContainer.clear();
            brandContainer.add(brandPanel);
        }
    }

    @Override
    protected void setBranding() {
        brand.setVisible(
                Enterpriser.widgetBrandingEnabled(BrandingWidgets.SELFHELP));
    }

    private void relayout() {
        manager.canSearchFull(new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                onSuccess(false);
            }

            @Override
            public void onSuccess(Boolean result) {
                hasSearch = result;
                if (hasSearch) {
                    search = new SelfHelpWidgetSearch(shWidget, hasHeader,
                            true);
                    search.placeholder(Widget.CONSTANTS.searchMore());
                    com.google.gwt.user.client.ui.Widget label = Common.label(
                            Widget.CONSTANTS.searchMore(),
                            Widget.CSS.searchLabel());
                    label.getElement().setAttribute(AriaProperty.HIDDEN,
                            String.valueOf(true));
                    search.disableAutocomplete();
                    if (WcagUtils.isWcagEnabled()) {
                        search.getCellFormatter().getElement(0, 1)
                                .appendChild(label.getElement());
                    }
                    searchPanel.add(search);
                    searchPanel.addStyleName(styleSearchPanel());
                    Roles.getPresentationRole().set(search.getElement());
                    searchOuterPanel.add(searchPanel);
                    if (Enterpriser.hasFeature(AdditionalFeatures.sh_google_drive)) {
                        searchOuterPanel.add(tabPanel.getPanel());
                    }

                    if (!hasHeader && hasClose) {
                        label.addStyleName(Widget.CSS.searchLabelNoHeader());
                        close.removeStyleName(Widget.CSS.close());
                        close.addStyleName(Widget.CSS.closeBesideSearch());
                        close.getElement().setAttribute(AriaProperty.LABEL,
                                Widget.CONSTANTS.widgetCloseTitle());
                        searchOuterPanel.add(close);
                    }
                }

                if (!hasHeader && !hasSearch) {
                    closePanel.addStyleName(Widget.CSS.closePanel());
                    Anchor onlyClose = Common.anchor("",
                            Widget.CONSTANTS.widgetCloseTitle(), true, true,
                            Widget.CSS.onlyCloseIcon(), Awesome.CLOSE);
                    onlyClose.addClickHandler(new ClickHandler() {
                        @Override
                        public void onClick(ClickEvent event) {
                            closeWidget("cross");
                        }
                    });

                    closePanel.add(onlyClose);
                }
                resize();
                setWCAG();
            }
        });
    }

    /*
     * This function is for personalisation API call
     *
     */
    private final void getPersonalisationConfigData(AsyncCallback<Boolean> callback) {
        long startTime = System.currentTimeMillis();
        EndUser.getUserPersonalisationData(
        		Enterpriser.enterprise().personalisationConfig().valueAsString("userId"),
        		settings.segment_id(),
        		Enterpriser.enterprise().personalisationConfig()
        								.valueAsObjExt("config").valueAsString(
        										PersonalisationAttributes.USER_ID_SRC.getKey()),
        		new AsyncCallback<EndUser>() {
		            @Override
		            public void onSuccess(EndUser endUserData) {
                        sendPersonalisationAnalytics(startTime,"success");
		                WfxData.updateEndUserData(endUserData);
		                if(!endUserData.isEmpty() && endUserData.custom().hasKey("content_order")) {
		                	String[] contentOrder = endUserData.custom().valueAsStringArray("content_order");
		                	reOrderUserFlows(contentOrder);
		                }
		                callback.onSuccess(true);
		            }

		            @Override
		            public void onFailure(Throwable caught) {
                        sendPersonalisationAnalytics(startTime,"failure");
		            	callback.onSuccess(true);
		            }
        		});
    }

    private void sendPersonalisationAnalytics(long startTime, String result) {
        CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                StringUtils.stringifyObject(Where.create("type",
                        TrackerEventOrigin.SELF_HELP.toString(),
                        EVENT_TYPE, "personalisation_end",
                        "segment_name", GaUtil.analyticsCache.segment_name(),
                        "segment_id", GaUtil.analyticsCache.segment_id(),
                        "role_tag", settings.role_tags(),
                        "result", result,
                        "api_time", String.valueOf(System.currentTimeMillis() - startTime),
                        SRC_ID, TrackerEventOrigin.SELF_HELP.getSrcName())));
    }


    private SelfHelpWidgetSettings reOrderUserFlows(String[] contentOrder) {


    	if(contentOrder != null) {
    		JsArrayString currentOrder = this.settings.order();
    		Set<String> customFlowIdSet = new HashSet<>();
    		JsArrayString newOrder = JavaScriptObject.createArray().cast();
    		
        	int contentLimit = Enterpriser.enterprise()
        									.personalisationConfig()
        									.valueAsObjExt("config")
        									.valueAsInt(PersonalisationAttributes.CONTENT_LIMIT.getKey());
        	
    		for(int ci = 0; ci < contentOrder.length && ci < contentLimit; ci++) {
    			customFlowIdSet.add(contentOrder[ci]);
    			newOrder.push((String) contentOrder[ci]);
    		}

    		if(currentOrder != null) {
    			for( int orderIdx = 0; orderIdx < currentOrder.length(); orderIdx++) {
    				String flowId = currentOrder.get(orderIdx);
    				if(!customFlowIdSet.contains(flowId)) {
    					newOrder.push((String)flowId);
    				}
    			}
    		}

    		if(newOrder.length() > 0) {
        		this.order = newOrder;
        		this.settings.order(newOrder);
        	}
    	}


    	return settings;
    }

    protected void setDashboardView() {
        this.isDashboardView = false;
    }


    private void resize() {
        Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {
            @Override
            public boolean execute() {
                double occupiedHeight = calculateOccupiedHeight();
                double height = Math.ceil(availableHeight - occupiedHeight);
                OverlayUtil.setStyle(container.getElement(), "height",
                        String.valueOf(height) + "px", false);
                focusOnWidget();
                CrossMessager.addListener((CrossMessager.CrossListener) (type, content) -> {
                    if ("chat_bot".equalsIgnoreCase(content) && footer != null) {
                        footer.activate(TabPanel.Tabs.CHAT, true);
                    }
                }, "routing");
                CrossMessager.sendMessageToParent("widget_loaded", "");

                scroller.addScrollHandler(new ScrollHandler() {

                    @Override
                    public void onScroll(ScrollEvent event) {
                        int offsetHeight = scroller.getElement().getOffsetHeight();
                        int scrollHeight = scroller.getElement().getScrollHeight();
                        int remainingScrollHeight = Math.abs(scrollHeight - offsetHeight);

                        if ((remainingScrollHeight
                                - scroller.getElement().getScrollTop()) == 0) {
                            if (null != search) {
                                trackScroll(search.query(), settings.is_segment_auto_filtered());
                            } else {
                                trackScroll("", settings.is_segment_auto_filtered());
                            }
                        }
                    }
                });
                return false;
            }
        }, 100);
    }

    private double calculateOccupiedHeight() {
        return (header.getOffsetHeight() + searchPanel.getOffsetHeight()
                + ((Enterpriser.isTrialAccount(Enterpriser.ent_id())
                        && Enterpriser.isWatermarkRemovalDisabled(
                                Enterpriser.ent_id())) ? trialContainerHeight
                                        : brandContainer.getOffsetHeight())
                + closePanel.getOffsetHeight()
                + (Enterpriser.hasFeature(AdditionalFeatures.sh_google_drive)
                        ? tabPanel.getPanel().getOffsetHeight()
                        : 0));
    }

    private void trackScroll(String searchQuery, boolean isAutoSegmented) {
        ScrollInfo scrollInfo = DataUtil.create();
        scrollInfo.search_query(searchQuery);
        scrollInfo.is_segment_auto_filtered(isAutoSegmented);
        AnalyticsInfo.addContextualInfo(ContextualInfo.OuterKey.SCROLL,
                scrollInfo);

        Where where = Where.create(Common.SEGMENT_NAME,
                GaUtil.analyticsCache.segment_name(), Common.SEGMENT_ID,
                GaUtil.analyticsCache.segment_id(), Common.SRC_ID, widgetSrc(),
                Common.CONTEXTUAL_INFO, GaUtil.analyticsCache.contextual_info());
        CrossMessager.sendMessageToParent(prefix("_scroll"),
                StringUtils.stringifyObject(where));
    }

    protected void focusOnWidget() {
        if (WcagUtils.isWcagEnabled() && "show".equalsIgnoreCase(
                Themer.value(Themer.SELFHELP.WID_HEADER_SHOW))) {
            setFocusOnHeaderTitle();
        } else if (search != null) {
            search.setFocus();
        }
    }

    private void setFocusOnHeaderTitle() {
        Scheduler.get().scheduleFixedDelay(() -> {
            header.getElement().getFirstChildElement().focus();
            return false;
        }, 100);
    }

    @Override
    protected void addGAContextualInfo(
            Map<ContextualInfo.OuterKey, ? extends JavaScriptObject> outerKeyMap) {
        if (outerKeyMap.get(OuterKey.POSITION) != null) {
            AnalyticsInfo.addContextualInfo(ContextualInfo.OuterKey.POSITION,
                    outerKeyMap.get(OuterKey.POSITION));
        }
        if (outerKeyMap.get(OuterKey.CRAWL) != null) {
            AnalyticsInfo.addContextualInfo(ContextualInfo.OuterKey.CRAWL,
                    outerKeyMap.get(OuterKey.CRAWL));

        }
    }

    @Override
    protected JavaScriptObject makePositionPayload(
            ContentFlowPanel contentFlowPanel) {
        PositionInfo posInfo = DataUtil.create();
        if (contentFlowPanel.getPositionInfo().getGroupPosition() != null)
            posInfo.group_position(contentFlowPanel.getPositionInfo()
                    .getGroupPosition().intValue());
        posInfo.clicked_content_position(
                contentFlowPanel.getPositionInfo().getContentPosition());
        String currentSearchQuery = "";
        if (null != search) {
            currentSearchQuery = search.query();
        }
        if (StringUtils.isNotBlank(currentSearchQuery)) {
            posInfo.search_query(currentSearchQuery);
        }
        posInfo.contents_in_widget(contentsInWidget);
        return posInfo;
    }

    @Override
    protected JavaScriptObject makeSourcePayload(String source) {
        ContentSource srcInfo = (ContentSource) DataUtil.create().cast();
        srcInfo.content_source(source);

        if (tabPanel.isActive(TabPanel.Tabs.GOOGLE_DRIVE)) {
            srcInfo.content_source("1000");
        }
        return srcInfo;
    }

    @Override
    protected TextPanel getTextPanel(ContentFlowPanel contentFlowPanel,
			ScrollPanel scroller, boolean  showNewLabel) {
        TextPanel textPanel = new SelfHelpTextPanel(contentFlowPanel, scroller, showNewLabel);
        return textPanel;
    }

    @Override
    public void  showContent(AsyncCallback<JsArray<Content>> cb,
            boolean showCrawledContent) {
        if (!no_initial_flows
                && StringUtils.isNotBlank(settings.search_query())) {
            String query = settings.search_query();
            addQuriesForSession(query);
            AsyncCallback<JsArray<Content>> searchCb = searchCb(query, cb);
            manager.searchDefaultSuggest(query, getSearchEntIds(), 10,
                    filter_by_tag, type, value,
                    order, new AsyncCallback<JsArray<Content>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            searchCb.onFailure(caught);

                        }

                        @Override
                        public void onSuccess(JsArray<Content> result) {
                            searchCb.onSuccess(result);
                            String searchTerm = settings.search_query();
                            search.query(searchTerm);
                            search.getClear().setVisible(
                                    StringUtils.isNotBlank(searchTerm));
                            settings.search_query("");
                        }
                    });
        } else {
            super.showContent(cb, showCrawledContent);
        }
    }

    void onGoogleDriveTabActive(String query, AsyncCallback<JsArray<Content>> cb) {
        AsyncCallback<JsArray<Content>> searchCb = searchCb(query, cb);

        allContents.clear();
        contentsInWidget = 0;
        flows.clear();

        FlowPanel nothingFoundPanel = new FlowPanel();
        nothingFoundPanel.addStyleName(Widget.CSS.nothingFound());

        Label nothingFoundLabel = Common.label("", styleNothing());
        Awesome.spin(nothingFoundLabel);
        setResultsCount(contentsInWidget);
        nothingFoundPanel.add(nothingFoundLabel);
        flows.add(nothingFoundPanel);

        GoogleDriveHelper.query = query;
        GoogleDriveHelper.cb = searchCb;
        if (GoogleDriveHelper.areTokenUpToDate()) {
            GoogleDriveHelper.getResults(query, searchCb);
        } else if (GoogleDriveHelper.presentRefreshToken()) {
            GoogleDriveHelper.refreshAccessToken();
        } else {
            search.stopProgress();
            LinkGoogleDrive drive = new LinkGoogleDrive(event -> {
                GoogleDriveHelper.refreshBothTokens();
            });
            allContents.clear();
            contentsInWidget = 0;
            flows.clear();

            flows.add(drive);
            GoogleDriveHelper.refreshBothTokens();
        }
    }

    @Override
    protected void showNothingFound() {
        FlowPanel nothingFoundPanel = new FlowPanel();
        nothingFoundPanel.addStyleName(Widget.CSS.nothingFound());
        Label nothingFoundLabel = Common.label(nothingFound(), styleNothing());
        setResultsCount(contentsInWidget);
        FlowPanel iconContainer = new FlowPanel();
        iconContainer.setStyleName(Widget.CSS.iconContainer());

        for (Entry<String, SupportOption> entry : supportOptions.entrySet()) {
            if (checkValidOption(entry.getKey())) {
                SupportOption option = entry.getValue();
                final String optionName = entry.getKey();
                final String widgetSearchQuery = searchQuery;

                String url = option.getUrlPrefix()
                        + Themer.value(entry.getKey());
                Anchor anchor = getAnchor(replaceSearchQuery(url), false,
                        Widget.CSS.widgetIcon());
                anchor.getElement().setInnerHTML(option.getIcon());
                anchor.setTitle(option.getTitle());
                anchor.addClickHandler(new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                                StringUtils.stringifyObject(Where.create(
                                        EVENT_TYPE, "chat_support", "type",
                                        optionName, "query", widgetSearchQuery,
                                        SRC_ID, widgetSrc())));
                        if (AppFactory.isNonBrowserApp()) {
                            CrossMessager.sendMessageToParent(
                                    "link_desktop_content",
                                    StringUtils.stringifyObject(Where.create(
                                            EVENT_TYPE, "chat_support",
                                            "type", optionName, "url",
                                            anchor.getHref(), "query",
                                            widgetSearchQuery, SRC_ID,
                                            widgetSrc())));
                            event.preventDefault();
                        }
                    }
                });
                anchor.getElement().setAttribute(AriaProperty.LABEL,
                        option.getTitle());
                iconContainer.add(anchor);
            }
        }

        showNothingFoundForGoogleDrive(iconContainer);
        showPanel(iconContainer, nothingFoundPanel, nothingFoundLabel);

        flows.add(nothingFoundPanel);
    }

    private void showPanel(FlowPanel iconContainer, FlowPanel nothingFoundPanel,
                           Label nothingFoundLabel) {
        searchFeedbackPanel(nothingFoundPanel, nothingFoundLabel, iconContainer);
        if (iconContainer != null && !tabPanel.isActive(TabPanel.Tabs.GOOGLE_DRIVE)) {
            nothingFoundPanel.add(nothingFoundLabel);
            nothingFoundPanel.add(iconContainer);
        } else {
            nothingFoundPanel
                    .add(Common.image(Common.BUNDLE.selfHelpNothingFound()));
            nothingFoundLabel.addStyleName(Widget.CSS.nothingWithImage());
            if (tabPanel.isActive(TabPanel.Tabs.GOOGLE_DRIVE)) {
                nothingFoundLabel.setText("Sorry, no results found on your Google Drive");
            }
            nothingFoundPanel.add(nothingFoundLabel);
        }
    }

    private void searchFeedbackPanel(FlowPanel nothingFoundPanel, Label nothingFoundLabel, FlowPanel iconContainer) {
        boolean isSearchFeedbackEnabled = Enterpriser
                .hasFeature(AdditionalFeatures.ss_nosearchresult_feedback);
        if (!isSearchFeedbackEnabled || tabPanel.isActive(TabPanel.Tabs.GOOGLE_DRIVE)) {
            return;
        }

        if (this.isDashboardView) {
            nothingFoundPanel.addStyleName(Widget.CSS.scrollableNothingFound());
        }

        feedbackProvided = false;
        emaildProvived = false;

        FlowPanel searchFeedbackPanel = new FlowPanel();

        nothingFoundLabel.addStyleName(Widget.CSS.nothingFoundLabel());
        iconContainer.getElement().getStyle().
                setMarginBottom(15, Style.Unit.PX);

        searchFeedbackPanel.add(
                Common.label(Widget.CONSTANTS.noResults(), Widget.CSS.heading()));
        searchFeedbackPanel.add(
                Common.label(Widget.CONSTANTS.searchFeedbacksuggest(),Widget.CSS.boldText()));

        TextArea feedbackTextArea = Common
                .textArea(Widget.CONSTANTS.searchFeedbackPlaceholder());
        feedbackTextArea.addStyleName(Widget.CSS.feedbackTextArea());

        feedbackTextArea.addBlurHandler(blurEvent -> {
            if (StringUtils.isNotBlank(feedbackTextArea.getText()) && !feedbackProvided) {
                feedbackProvided = true;
                CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                        StringUtils.stringifyObject(Where.create(
                                EVENT_TYPE, GaUtil.PayloadTypes.widget_feedback_provided.name(),
                                Common.SEARCH_TERMS, getQuriesForSessions(),
                                Common.SEGMENT_NAME, GaUtil.analyticsCache.segment_name(),
                                Common.SEGMENT_ID, GaUtil.analyticsCache.segment_id(),
                                Common.SRC_ID, widgetSrc()
                        )));
            }
        });

        TextBox emailTextBox = Common
                .text(Widget.CONSTANTS.searchFeedbackEmailPlaceholder());
        emailTextBox.addStyleName(Widget.CSS.emailTextBox());

        emailTextBox.addBlurHandler(blurEvent -> {
            if (StringUtils.isNotBlank(emailTextBox.getText()) && !emaildProvived) {
                emaildProvived = true;
                CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                        StringUtils.stringifyObject(Where.create(
                                EVENT_TYPE, GaUtil.PayloadTypes.widget_feedback_email_provided.name(),
                                Common.SEARCH_TERMS, getQuriesForSessions(),
                                Common.SEGMENT_NAME, GaUtil.analyticsCache.segment_name(),
                                Common.SEGMENT_ID, GaUtil.analyticsCache.segment_id(),
                                Common.SRC_ID, widgetSrc()
                        )));

            }
        });

        String user = settings.user();
        if (Common.isValidEmail(user)) {
            emailTextBox.setValue(user);
            CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                    StringUtils.stringifyObject(Where.create(
                            EVENT_TYPE, GaUtil.PayloadTypes.widget_feedback_email_captured.name(),
                            Common.SEARCH_TERMS, getQuriesForSessions(),
                            Common.SEGMENT_ID, GaUtil.analyticsCache.segment_id(),
                            Common.SEGMENT_NAME, GaUtil.analyticsCache.segment_name(),
                            Common.SRC_ID, widgetSrc()
                    )));
        }
        emailTextBox.setVisible(false);
        searchFeedbackPanel.add(feedbackTextArea);
        searchFeedbackPanel.add(emailTextBox);
        searchFeedbackPanel.add(showToggleBar(emailTextBox, true));
        Button send = Common.primaryButton(Widget.CONSTANTS.searchFeedbackSend());
        send.addStyleName(Widget.CSS.sendFeedBack());
        send.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                if(StringUtils.isBlank(feedbackTextArea.getValue())){
                    Common.showMessage(Widget.CONSTANTS.emptyFeedbackError(),
                            Common.Alert.ERROR);
                    return;
                }

                SearchFeedbackService service = GWT.create(SearchFeedbackService.class);
                service.submit(ent_id, getQuriesForSessions(),
                        settings.segment_id(), emailTextBox.getValue(),
                        feedbackTextArea.getValue(),
                        new AsyncCallback<JavaScriptObject>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                Common.showMessage(Widget.CONSTANTS.searchFeedbackError(),
                                        Common.Alert.ERROR);
                            }

                            @Override
                            public void onSuccess(JavaScriptObject result) {
                                showThanksForFeedback(nothingFoundPanel);
                                CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                                        StringUtils.stringifyObject(Where.create(
                                                EVENT_TYPE, GaUtil.PayloadTypes.widget_feedback_sent.name(),
                                                Common.SEARCH_TERMS, getQuriesForSessions(),
                                                Common.SEGMENT_NAME, GaUtil.analyticsCache.segment_name(),
                                                Common.SEGMENT_ID, GaUtil.analyticsCache.segment_id(),
                                                Common.SRC_ID, widgetSrc())));
                                clearQueries();
                            }
                        });
            }
        });

        searchFeedbackPanel.add(send);
        nothingFoundPanel.add(searchFeedbackPanel);

        if (!this.isDashboardView) {
            CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                    StringUtils.stringifyObject(Where.create(
                            EVENT_TYPE, GaUtil.PayloadTypes.widget_feedback_seen.name(),
                            Common.SEARCH_TERMS, getQuriesForSessions(),
                            Common.SEGMENT_NAME, GaUtil.analyticsCache.segment_name(),
                            Common.SEGMENT_ID, GaUtil.analyticsCache.segment_id(),
                            Common.SRC_ID, widgetSrc()
                    )));
        }
    }

    public String getQuriesForSessions() {
        return queriesForSession.stream().collect(Collectors.joining(","));
    }

    public void addQuriesForSession(String query) {
        queriesForSession.add(query);
    }

    public void clearQueries() {
        queriesForSession.clear();
    }


    void updateToggleBar(Panel panel, ToggleCheckbox toggleCheckbox,
                         TextBox emailTextBox, boolean toggleValue) {
        panel.clear();
        FlexTable holder = new FlexTable();
        holder.setWidget(0, 0, toggleCheckbox);
        holder.getCellFormatter().setHorizontalAlignment(0, 0,
                HasHorizontalAlignment.ALIGN_LEFT);
        holder.getCellFormatter().setHorizontalAlignment(0, 1,
                HasHorizontalAlignment.ALIGN_LEFT);
        holder.getFlexCellFormatter().setVerticalAlignment(0, 0,
                HasVerticalAlignment.ALIGN_TOP);
        holder.getCellFormatter().setStyleName(0, 0, Widget.CSS.toggleButton());


        if (!toggleValue) {
            Label shareAnonymously = Common.label(Widget.CONSTANTS.shareAnonymously(),
                    Widget.CSS.shareAnonymously());
            holder.setWidget(0, 1, shareAnonymously);
        } else {
            String emailLable;
            String clickableText;
            if (StringUtils.isBlank(emailTextBox.getText())) {
                emailLable = Widget.CONSTANTS.shareMailId();
                clickableText = Widget.CONSTANTS.clickingHere();
            } else {
                emailLable = Widget.CONSTANTS.capturedMailId().replace("{mail_id}",
                        emailTextBox.getValue());
                clickableText = Widget.CONSTANTS.clickHere();
            }
            Label shareEmail = Common.label(emailLable, Widget.CSS.shareEmail());
            Label clickable = Common.label(clickableText,
                    Widget.CSS.clickableText());
            clickable.addClickHandler(clickEvent -> {
                panel.clear();
                emailTextBox.setVisible(true);
            });
            FlowPanel container = new FlowPanel();
            container.add(shareEmail);
            container.add(clickable);
            holder.setWidget(0, 1, container);
        }
        Common.addStyles(holder, Widget.CSS.shareFeedbackGrid());
        panel.add(holder);
    }

    private FlowPanel showToggleBar(TextBox emailTextBox, boolean toggleValue) {
        FlowPanel panel = new FlowPanel();
        ToggleCheckbox toggleCheckbox = new ToggleCheckbox();
        toggleCheckbox.setValue(toggleValue);
        updateToggleBar(panel, toggleCheckbox, emailTextBox, toggleValue);
        toggleCheckbox.addValueChangeHandler(valueChangeEvent -> {
            updateToggleBar(panel, toggleCheckbox, emailTextBox,
                    valueChangeEvent.getValue());
        });
        return panel;
    }


    private void showThanksForFeedback(FlowPanel nothingFoundPanel){
        nothingFoundPanel.clear();
        FlowPanel thankYouPanel = new FlowPanel();
        thankYouPanel.addStyleName(Widget.CSS.thankYouPanel());
        thankYouPanel.add(Common.image(Common.BUNDLE.tickSvg()));
        thankYouPanel.add(Common.label(Widget.CONSTANTS.thankYou(),Widget.CSS.heading()));
        thankYouPanel.add(Common.label(Widget.CONSTANTS.searchFeedbackSuccess()));
        nothingFoundPanel.add(thankYouPanel);
    }


    private void showNothingFoundForGoogleDrive(FlowPanel iconContainer) {
        if (Enterpriser.hasFeature(AdditionalFeatures.sh_google_drive)) {
            Anchor anchor = getAnchor("", false,
                    Widget.CSS.widgetIcon());
            anchor.getElement().setInnerHTML(SVGElements.G_DRIVE);
            anchor.setTitle("Search in Google Drive");
            anchor.setHeight("25px");
            anchor.addClickHandler(e -> {
                tabPanel.activate(TabPanel.Tabs.GOOGLE_DRIVE, true);
                e.preventDefault();
            });
            anchor.getElement().setAttribute(AriaProperty.LABEL,
                    "Search in Google Drive");
            iconContainer.add(anchor);
        }
    }

    protected void setWCAG() {
        Element headerTitle = header.getElement().getFirstChildElement();
        Element searchBox = search.getCellFormatter().getElement(0, 1)
                .getFirstChildElement();
        Element topElement = hasHeader ? headerTitle : searchBox;
        DomWrapper.removeTabListener(topElement);
        Element lastElement = logo.getElement();
        DomWrapper.removeTabListener(lastElement);
        DomWrapper.setShiftTabHandler(topElement, lastElement);
        DomWrapper.setTabHandler(lastElement, topElement);
    }

    // For WCAG- to read out number of results
    private void setResultsCount(int searchResultCount) {
        if (DOM.getElementById(SEARCH_SPAN_ID) != null) {
            flowListCount = DOM.getElementById(SEARCH_SPAN_ID);
            flowListCount.setInnerText(searchResultCount + " "
                    + (searchResultCount <= 1 ? "result" : "results"));
        }
    }

    private String replaceSearchQuery(String url) {
        searchQuery = searchQuery == null ? "" : searchQuery;
        return url.replaceAll(WFX_SEARCH_QUERY_KEY,
                URL.encode(searchQuery));
    }

    protected boolean checkValidOption(String option) {
        return StringUtils.isNotBlank(Themer.value(option));
    }

    protected Anchor getAnchor(String url, boolean self, String... styles) {
        Anchor anchor = Common.anchor("", url, self, styles);
        return anchor;
    }

    private Panel getBrandPanel() {
        Grid grid = Common.makeRight(brand, Widget.CSS.brandPanel());
        grid.setWidth("100%");
        grid.setCellPadding(0);
        grid.setCellSpacing(0);
        return grid;
    }

    @Override
    public void showSearchSuggest(String query,
            AsyncCallback<JsArray<Content>> cb) {
        searchQuery = query;
        String entIds = getSearchEntIds();
        AsyncCallback<JsArray<Content>> searchCb = searchCb(query, cb);
        if (tabPanel.isActive(TabPanel.Tabs.TOP_HELP) && StringUtils.isBlank(searchQuery)) {
            super.showContent(searchCb, true);
        } else {
            if ("live".equals(mode)) {
                manager.searchSuggestFill(query, entIds, 25, filter_by_tag, searchCb, Enterpriser.noindex_tag());
            } else {
                manager.searchSuggest(query, entIds, 25, filter_by_tag, searchCb, Enterpriser.noindex_tag());
            }
        }
    }

    private String getSearchEntIds() {
        return collateEntIds(settings.ent_id(), settings.search_ent_ids());
    }

    /**
     * Creates CSV of enterprise Ids.
     *
     * @param entId
     * @param searchEntIds
     * @return
     */
    private String collateEntIds(String entId, JsArrayString searchEntIds) {

        if (StringUtils.isBlank(entId)) {
            return entId;
        }

        StringBuilder entIdsAnnexed = new StringBuilder(entId);

        if (searchEntIds != null && searchEntIds.length() > 0) {
            entIdsAnnexed.append(Common.COMMA);
            entIdsAnnexed.append(searchEntIds.join());
        }

        return entIdsAnnexed.toString();
    }

    protected AsyncCallback<JsArray<Content>> searchCb(final String query,
            final AsyncCallback<JsArray<Content>> cb) {
        return delegatingCb(new AsyncCallback<JsArray<Content>>() {

            @Override
            public void onFailure(Throwable caught) {
                searchQuery = null;
                cb.onFailure(caught);
            }
            @Override
            public void onSuccess(JsArray<Content> result) {
                searchQuery = null;
                cb.onSuccess(result);
            }
        }, false, false);
    }

    @Override
    protected String logoTitle() {
        return Widget.CONSTANTS.poweredTitle();
    }

    @Override
    protected String poweredTitle() {
        return Widget.CONSTANTS.poweredTitle();
    }

    @Override
    protected Label poweredLabel() {
        return Common.label(Widget.CONSTANTS.powered(), Widget.CSS.powered());
    }

    @Override
    protected String powered() {
        return Widget.CONSTANTS.powered();
    }

    @Override
    protected String nothingFound() {
       return Widget.CONSTANTS.nothingFound(false);
    }

    @Override
    protected String closeTitle() {
        return Widget.CONSTANTS.widgetCloseTitle();
    }

    @Override
    protected Propertizable constants() {
        return Widget.CONSTANTS;
    }

    @Override
    protected String styleWidget() {
        return Widget.CSS.widget();
    }

    @Override
    protected String styleScroller() {
        return Widget.CSS.scroller();
    }

    @Override
    protected String styleContainer() {
        if (Enterpriser.hasFeature(AdditionalFeatures.sh_google_drive)) {
            return Widget.CSS.containerwithTab();
        }
        if (Enterpriser.isTrialAccount(Enterpriser.ent_id()) && Enterpriser
                .isWatermarkRemovalDisabled(Enterpriser.ent_id())) {
            return Widget.CSS.containerForTrial();
        } else {
            return Widget.CSS.container();
        }
    }

    @Override
    protected String styleClose() {
        return Widget.CSS.close();
    }

    @Override
    protected String styleElement() {
        return Common.CSS.widgetAnchorElement();
    }

    @Override
    protected String styleNothing() {
        return Widget.CSS.nothing();
    }

    protected String styleSearchPanel() {
        if (Enterpriser.hasFeature(AdditionalFeatures.sh_google_drive)) {
            return Widget.CSS.searchPanelWithTab();
        } else {
            return Widget.CSS.searchPanel();
        }
    }

    @Override
    protected String brandLogoStyle() {
        return Common.CSS.brandLogoDarkBackground();
    }

    @Override
    protected ImageProgressor getProgressor() {
        return Common.getProgressor(Common.CSS.widgetProgressor(),
                Widget.CSS.progressor());
    }

    protected void correctBorders(String position, String relative_to) {
        if (position == null) {
            getElement().addClassName(Widget.CSS.noBorderBottom());
            getElement().addClassName(Widget.CSS.noBorderTop());
            getElement().addClassName(Widget.CSS.noBorderRight());
            getElement().addClassName(Widget.CSS.noBorderLeft());
            availableHeight = availableHeight + 1;
            return;
        }
        if (relative_to == null) {
            if (position.startsWith("l")) {
                getElement().addClassName(Widget.CSS.noBorderLeft());
            } else if (position.startsWith("r")) {
                getElement().addClassName(Widget.CSS.noBorderRight());
            } else if (position.startsWith("t")) {
                availableHeight = availableHeight + 1;
                getElement().addClassName(Widget.CSS.noBorderTop());
            } else if (position.startsWith("b")) {
                availableHeight = availableHeight + 1;
                getElement().addClassName(Widget.CSS.noBorderBottom());
            }
        }
    }

    public boolean isContainerScrolled() {
        return (scroller.getElement().getScrollTop() > 0);
    }

    @Override
    protected boolean hideOnEscape() {
        return true;
    }

    @Override
    public boolean trackSearch() {
        return true;
    }

    @Override
    protected String widgetSrc() {
        return TrackerEventOrigin.SELF_HELP.getSrcName();
    }

    @Override
    protected void setId(Content content,
            com.google.gwt.user.client.ui.Widget widget) {
        Common.setId(widget,
                "wfx-dashboard-self-help-flow-" + content.flow_id());
    }

    @Override
    protected ClickHandler getLinkHandler(ContentFlowPanel contentFlowPanel) {
        boolean shouldCloseOnClick = !(StringUtils
                .isNotBlank(settings.keep_open_on_launch())
                && settings.keep_open_on_launch()
                        .contains(ContentType.link.name()));

        return new LinkHandler(contentFlowPanel, shouldCloseOnClick);
    }


    public class SelfHelpTextPanel extends TextPanel {

        public SelfHelpTextPanel(ContentFlowPanel contentFlowPanel,
                ScrollPanel scroller, boolean showNewLabel) {
            super(contentFlowPanel, scroller, showNewLabel);
            header.getParent().getElement().setAttribute(AriaProperty.LABEL,
                    Common.CONSTANTS.collapsed() + ","
                            + contentFlowPanel.getContent().getType() + ", "
                            + contentFlowPanel.getContent().title());
        }

        @Override
        public void trackOpen() {
            Content textInfo = content.cast();
            setContextualInfo(contentFlowPanel, textInfo.source());
            super.trackOpen();
        }
    }

    @Override
    protected void updateUnseenContent(String contentId) {
        CrossMessager.sendMessageToParent("updateUnseenContent", contentId);
    }
}
