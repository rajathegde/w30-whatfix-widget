package co.quicko.whatfix.widget;

import static co.quicko.whatfix.service.WfxInfo.getDomain;

import java.util.List;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;

import co.quicko.whatfix.common.Awesome;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.Content.Video;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Enterprise;
import co.quicko.whatfix.data.BrandingWidgets;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.Launcher.SelfHelpWidgetSettings;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.WfxInfo.Domains;

public class PreviewModeSelfHelp extends SelfHelpWidget {

    private Anchor editSegmentAnchor;
    
    public PreviewModeSelfHelp(SelfHelpWidgetSettings settings) {
        super(settings);
    }
    
    @Override
    protected AsyncCallback<JsArray<Content>> searchCb(String query,
            AsyncCallback<JsArray<Content>> cb) {
        final AsyncCallback<JsArray<Content>> superSearchCb = super.searchCb(
                query, cb);
        return new AsyncCallback<JsArray<Content>>() {
            @Override
            public void onFailure(Throwable caught) {
                superSearchCb.onFailure(caught);
            }

            @Override
            public void onSuccess(JsArray<Content> result) {
                if(result == null || result.length() == 0) {
                    searchNothingFound();
                } else {
                    superSearchCb.onSuccess(result);
                }
            }
        };
    }
    
    private void searchNothingFound() {
        clearFlowsPanel();
        super.showNothingFound();
        search.onFailure(null);
    }
    
    @Override
    protected void showNothingFound() {
        FlowPanel nothingFoundPanel = new FlowPanel();
        String customMessage = settings.custom_message();
        if(StringUtils.isNotBlank(customMessage)) {
            nothingFoundPanel.add(Common.image(Widget.BUNDLE.getStarted()));
            nothingFoundPanel
                    .add(Common.label(customMessage, Widget.CSS.flowEmptyLabel()));
        } else {
            final String youtubeUrl = "https://www.youtube.com/embed/rd_rRTRIBwk?rel=0&autoplay=1&list=";
            ClickHandler ClickHandler = new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    CrossMessager.sendMessageToParent("widget_video",
                            StringUtils.stringifyObject(
                                    getLearnCreateFlowVideo()));
                    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
                        @Override
                        public void execute() {
                            closeWidget("video/click");
                        }
                    });
                }

                private Video getLearnCreateFlowVideo() {
                    Video videoDetails = DataUtil.create();
                    videoDetails.url(youtubeUrl);
                    return videoDetails;
                }
            };
            nothingFoundPanel = Common.getNothingFoundPanel(
                    Widget.BUNDLE.getStarted(), Widget.CSS.flowEmptyLabel(),
                    Widget.CSS.videoLauncher(), ClickHandler);
        }
        nothingFoundPanel.setStyleName(Widget.CSS.nothingFoundWithVideo());
        editSegmentAnchor.setVisible(false);
        searchOuterPanel.setVisible(false);
        flows.add(nothingFoundPanel);
    }

    @Override
    protected List<com.google.gwt.user.client.ui.Widget> additionalComponentsToAdd() {
        List<com.google.gwt.user.client.ui.Widget> list = super.additionalComponentsToAdd();
        if (settings.is_editor_realized() && settings.enable_edit_segment()
                && settings.segment_id() != null) {
            editSegmentAnchor = getEditSegmentAnchor();
            list.add(editSegmentAnchor);
        }
        return list;
    }
    
    private Anchor getEditSegmentAnchor() {
        String icon;
        String anchorTitle = null;
        if(settings.is_default_segment()) {
            icon = Awesome.CLOSE;
            anchorTitle = Common.i18nConstants.addNewSegment();
        } else {
            icon = Awesome.EDIT;
            anchorTitle = "Edit '";
            anchorTitle += (settings.segment_name() != null
                    && settings.segment_name().length() > 40)
                            ? settings.segment_name().substring(0, 40) + "..."
                            : settings.segment_name();
            anchorTitle += "'";
        }
        Anchor editSegment = Common.anchor("", getSegmentEditUrl(),
                false, Widget.CSS.editSegmentAnchor(), Common.CSS.anchor(),
                icon);
        if(settings.is_default_segment()) {
            editSegment.addStyleName(Widget.CSS.newSegmentAnchor());
        }
        if(Enterpriser.widgetBrandingEnabled(BrandingWidgets.SELFHELP)) {
            editSegment.addStyleName(Widget.CSS.marginBottom20());
        }
        setTitleAttribute(editSegment, anchorTitle);
        return editSegment;
    }
    
    private String getSegmentEditUrl() {
        String url = "https://" + getDomain(Domains.DASHBOARD) + "/" + Enterpriser.ent()
                + "/#!widgets/selfhelp/segment/";
        String id = settings.segment_id();
        if (id == null) {
            return null;
        }
        url += id + "/";
        String pageTags = settings.page_tags() != null ? settings.page_tags()
                : "-";
        // Currently Role tags are not supported for overriding a segment
        // String roleTags = settings.role_tags() != null ? settings.role_tags()
        //        : "-";
        if (settings.is_default_segment()) {
            url += pageTags + "/-";
        }
        return url;
    }

    private void setTitleAttribute(Anchor w, String title) {
        w.getElement().setAttribute("title", title);
    }
}
