package co.quicko.whatfix.widget;

import co.quicko.whatfix.overlay.Launcher;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.PopupEntryPoint;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.contextualinfo.AttentionInfo;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EnterpriseSettings;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.extension.util.ExtensionHelper;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.Launcher.SelfHelpWidgetSettings;
import co.quicko.whatfix.overlay.Launcher.Settings;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.service.LocaleUtil;
import co.quicko.whatfix.service.Where;

public class WidgetEntry extends PopupEntryPoint {

    private static final String PREFIX = "widget";

    @Override
    public void initialize() {
        Themer.initialize();
    }

    @Override
    public Widget getWidget(String content) {
        EnterpriseSettings enterpriseSettings = DataUtil.create(content);
        GaUtil.getInteraction(enterpriseSettings);
        Enterpriser.initialize(enterpriseSettings);
        Themer.initialize(enterpriseSettings.jsTheme());
        ExtensionHelper.initialize(Enterpriser.community());
        ExtensionHelper.setPreviewMode(enterpriseSettings.isPreviewMode());

        co.quicko.whatfix.widget.Widget.BUNDLE.icons().ensureInjected();
        co.quicko.whatfix.widget.Widget.CONSTANTS
                .use(Enterpriser.properties(LocaleUtil.locale(true)));
        setLanguage();
        return getTheWidget(
                (SelfHelpWidgetSettings) enterpriseSettings.settings(),
                enterpriseSettings.isMobile());
    }

    @Override
    public String prefix() {
        return PREFIX;
    }

    private SelfHelpWidget getTheWidget(final SelfHelpWidgetSettings settings,
            boolean isMobile) {
        SelfHelpWidget widget;

        if (!isMobile) {
            widget = getDesktopWidget(settings);
        } else {
            widget = getMobileWidget(settings);
        }
        initializeGa(settings);
        return widget;
    }

    protected SelfHelpWidget getDesktopWidget(SelfHelpWidgetSettings settings) {
        return new SelfHelpWidget(settings);
    }

    private SelfHelpWidget getMobileWidget(SelfHelpWidgetSettings settings) {
        return new TheMobileWidget(settings);
    }

    private static void initializeGa(Settings settings) {
        String segmentName = (settings.segment_name() != null)
                ? settings.segment_name() : settings.title();
        GaUtil.setAnalytics(TrackerEventOrigin.SELF_HELP, settings.segment_id(),
                segmentName);
        Common.tracker().initialize(Security.unq_id());
        Common.tracker().use(Enterpriser.enterprise().ent_id(),
                Security.user_id(), Security.displayName(),
                Security.user_name(), TrackerEventOrigin.SELF_HELP.getSrcName(),
                Enterpriser.enterprise().ga_id());
        GaUtil.src_id = TrackerEventOrigin.SELF_HELP.getSrcName();
        AttentionInfo attentionInfo = DataUtil.create();
        String attentionAnimation = "show"
                .equalsIgnoreCase(Themer.value(Themer.SELFHELP.ANIMATE))
                        ? Themer.value(Themer.SELFHELP.ANIMATION) : "none";
        attentionInfo.attention_animation(attentionAnimation);
        AnalyticsInfo.addContextualInfo(ContextualInfo.OuterKey.ATTENTION,
                attentionInfo);
        CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                StringUtils.stringifyObject(Where.create("type",
                        TrackerEventOrigin.SELF_HELP.toString(),
                        "event_type", "init",
                        "segment_name", GaUtil.analyticsCache.segment_name(),
                        "segment_id", GaUtil.analyticsCache.segment_id(),
                        "contextual_info", GaUtil.analyticsCache.contextual_info(),
                        "role_tag", ((Launcher.TagEnabledWidgetSettings)settings).role_tags())));
    }
}
