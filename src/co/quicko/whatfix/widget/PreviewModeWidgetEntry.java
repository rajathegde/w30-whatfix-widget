package co.quicko.whatfix.widget;

import co.quicko.whatfix.overlay.Launcher.SelfHelpWidgetSettings;

public class PreviewModeWidgetEntry extends WidgetEntry {
    
    @Override
    protected SelfHelpWidget getDesktopWidget(SelfHelpWidgetSettings settings) {
        return new PreviewModeSelfHelp(settings);
    }
}