package co.quicko.whatfix.widget;

import co.quicko.whatfix.common.Common;

import com.google.gwt.core.client.GWT;

public class Widget {
    public static final WidgetBundle BUNDLE = GWT.create(WidgetBundle.class);
    public static final WidgetCss CSS = BUNDLE.css();
    public static final WidgetConstants CONSTANTS = GWT
            .create(WidgetConstants.class);

    static {
        Common.CSS.ensureInjected();
        CSS.ensureInjected();
    }

}
