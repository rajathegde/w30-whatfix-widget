package co.quicko.whatfix.widget;

import co.quicko.whatfix.common.*;
import co.quicko.whatfix.common.logger.*;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.Where;
import com.google.gwt.aria.client.Roles;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.TextBox;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static co.quicko.whatfix.common.logger.LOGConstants.*;

public class SelfHelpWidgetSearch extends Grid implements AsyncCallback<JsArray<Content>>, DelayedTrigger.Trigger,
        ValueChangeHandler<String>, KeyUpHandler, ClickHandler {

    private static final String SEGMENT_NAME = "segment_name";
    private static final String SEGMENT_ID = "segment_id";
    private static final String TRACK_SEARCH_KEYWORD = "flows";
    private static final String QUERY = "query";

    protected TextBox search;
    protected Anchor searchIcon;
    protected HandlerRegistration searchIconHandler;

    protected SelfHelpWidget widget;
    private DelayedTrigger delayer;
    private String previous;
    private boolean trackerDelayerAlive = false;
    private JsArray<Content> currentResult;
    private String currentQuery;
    private boolean showCrawledContent;

    protected Anchor clear;

    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();

    public SelfHelpWidgetSearch(SelfHelpWidget widget, boolean showClear,
                                boolean showCrawledContent) {
        super(1, 3);
        search = Common.text("");
        search.addStyleName(searchBoxCss());
        searchIcon = Common.anchor(null, Common.CSS.searchIcon());
        searchIcon.getElement().setInnerHTML(SVGElements.SEARCH);
        (searchIcon.getElement()).setAttribute(AriaProperty.HIDDEN,
                String.valueOf(true));
        setCellPadding(0);
        setCellSpacing(0);
        setWidget(0, 0, search);
        setWidget(0, 1, searchIcon);
        searchIcon.setTabIndex(-2);
        CellFormatter searchFormatter = getCellFormatter();
        searchFormatter.setStyleName(0, 1, searchIconCss());
        search.setTabIndex(0);
        Common.handleEnter(search, this);
        searchIconHandler = searchIcon.addClickHandler(this);
        Roles.getPresentationRole().set(getElement());
        initWidgetSearch(widget, showClear, showCrawledContent);
    }

    private void initWidgetSearch(SelfHelpWidget widget, boolean showClear,
                                 boolean showCrawledContent) {
        this.showCrawledContent = showCrawledContent;
        // Not setting to default href value as from chrome version 76
        // clicking on the clear icon is opening a new blank tab
        clear = Common.anchor("", widgetSearchClearTitle(), true, false,
                searchClear());
        clear.getElement().setInnerHTML(SVGElements.CLOSE);
        clear.getElement().setAttribute(AriaProperty.LABEL,
                widgetSearchClearTitle());
        // overriding super settings.
        setWidget(0, 0, searchIcon);
        setWidget(0, 1, search);
        if (showClear) {
            setWidget(0, 2, clear);
        }

        search.setStyleName(searchStyle());
        Common.setId(search, "search");
        search.setTitle(widgetSearchTitle());
        search.addFocusHandler(event -> addStyleName(searchGridProgress()));
        search.addBlurHandler(event -> removeStyleName(searchGridProgress()));

        clear.addKeyUpHandler(event -> {
            event.preventDefault();
            // Checking for enter key
            if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                clearSearchHandler();
            }
        });

        clear.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                event.preventDefault();
                clearSearchHandler();
            }
        });

        clear.setVisible(false);
        clear.setTabIndex(0);
        setStyleName(searchGridStyle());
        getElement().setAttribute("data-wfx-id", "self-help-search-grid");
        CellFormatter searchFormatter = getCellFormatter();
        searchFormatter.setStyleName(0, 0, searchIconCellStyle());

        // set presentation role to all the td elements
        Roles.getPresentationRole().set(searchFormatter.getElement(0, 0));
        Roles.getPresentationRole().set(searchFormatter.getElement(0, 1));
        Roles.getPresentationRole().set(searchFormatter.getElement(0, 2));

        Roles.getPresentationRole().set(this.getElement());

        this.widget = widget;
        this.delayer = new DelayedTrigger(this, 800);
        this.previous = search.getText();
        search.addKeyUpHandler(this);
        search.addValueChangeHandler(this);
    }

    private void clearSearchHandler() {
        search.setValue("", true);
        clear.setVisible(false);
        setFocus();
        CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                StringUtils.stringifyObject(Where.create(SelfHelpWidget.EVENT_TYPE,
                        "search_cross", SelfHelpWidget.SRC_ID, analyticsSource(),
                        SEGMENT_ID, GaUtil.analyticsCache.segment_id(),
                        SEGMENT_NAME, GaUtil.analyticsCache.segment_name(),
                        SelfHelpWidget.ACTIVE_TAB, widget.tabPanel.getActiveTab().toString().toLowerCase()
                )));
    }

    @Override
    public void onClick(ClickEvent event) {
        TimeStampTracker.setTime(TimeStampTracker.Event.SELF_HELP_SEARCH_TRIGGERED_TIME, System.currentTimeMillis());
        showProgress();
        String query = query();
        if (this.widget.tabPanel.isActive(TabPanel.Tabs.GOOGLE_DRIVE)) {
            widget.onGoogleDriveTabActive(query(), this);
        } else {
            if (query.length() == 0) {
                widget.showContent(this, showCrawledContent);
            } else {
                widget.addQuriesForSession(query);
                widget.showSearchSuggest(query, this);
                Where where = Where.create(QUERY,
                        Boolean.toString(widget.isContainerScrolled()),
                        SEGMENT_ID, GaUtil.analyticsCache.segment_id(),
                        SEGMENT_NAME, GaUtil.analyticsCache.segment_name(),
                        SelfHelpWidget.ACTIVE_TAB, widget.tabPanel.getActiveTab().toString().toLowerCase(),
                        SelfHelpWidget.EVENT_TYPE, "search_scroll");
                CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                        StringUtils.stringifyObject(where));
            }
        }
    }

    @Override
    public void onFailure(Throwable caught) {
        stopProgress();
        searchIcon.getElement().removeAllChildren();
        searchIcon.getElement().setInnerHTML(SVGElements.SEARCH);
    }

    @Override
    public void onSuccess(JsArray<Content> result) {
        onFailure(null);
        currentResult = result;
        currentQuery = query();

        Long searchTime = (System.currentTimeMillis() - TimeStampTracker.getTime(TimeStampTracker.Event.SELF_HELP_SEARCH_TRIGGERED_TIME));
        Where logObject = Where.create(LOG_LEVEL, WfxLogLevel.PERFORMANCE.toString(),
                CONTENT, ("Time taken for Search API to respond and search result rendering : "+searchTime+ " ms" ),
                CLASS_NAME, SelfHelpWidgetSearch.class.getSimpleName(),
                MODULE, WfxModules.SELF_HELP.toString(),
                METHOD_NAME, "search::onSuccess");
        CrossMessager.sendMessageToParent(LOGGING,StringUtils.stringifyObject(logObject));
        // tracking is delayed to bunch up the user search terms and
        // avoiding multiple tracking events in a short duration
        if (widget.trackSearch() && !trackerDelayerAlive
                && currentQuery != null && currentQuery.length() > 0) {
            trackerDelayerAlive = true;
            new DelayedTrigger(() -> {
                boolean noResults = (currentResult == null
                        || currentResult.length() == 0);

                Where searchTimesMetrics = Where.create("search_api_time", TimeStampTracker.getTime(TimeStampTracker.Event.SELF_HELP_SEARCH_API_TIME), "search_time", searchTime);
                Where where = Where.create("type",
                        TRACK_SEARCH_KEYWORD
                                + ((noResults) ? "/noresults" : ""),
                        QUERY, currentQuery, SelfHelpWidget.EVENT_TYPE, "search",
                        SEGMENT_ID, GaUtil.analyticsCache.segment_id(),
                        SEGMENT_NAME, GaUtil.analyticsCache.segment_name(),
                        SelfHelpWidget.ACTIVE_TAB, widget.tabPanel.getActiveTab().toString().toLowerCase(),
                        SelfHelpWidget.SRC_ID, analyticsSource(),
                        "metrics", StringUtils.stringifyObject(searchTimesMetrics));
                CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                        StringUtils.stringifyObject(where));

                trackerDelayerAlive = false;
            }, 5000).doTrigger();
        }
    }

    public void showProgress() {
        searchIcon.getElement().removeAllChildren();
        Awesome.spinSmall(searchIcon);
    }

    public void stopProgress() {
        Awesome.unspinSmall(searchIcon);
    }

    @Override
    public void trigger() {
        if (this.widget.tabPanel.isActive(TabPanel.Tabs.GOOGLE_DRIVE)) {
            widget.onGoogleDriveTabActive(query(), this);
        } else {
            String query = query();

            if (query.length() == 0) {
                widget.showContent(this, showCrawledContent);
            } else {
                widget.addQuriesForSession(query);
                widget.showSearchSuggest(query, this);
                Where where = Where.create(QUERY,
                        Boolean.toString(widget.isContainerScrolled()),
                        SelfHelpWidget.EVENT_TYPE, "search_scroll",
                        SelfHelpWidget.SRC_ID, analyticsSource(),
                        SelfHelpWidget.ACTIVE_TAB, widget.tabPanel.getActiveTab().toString().toLowerCase(),
                        SEGMENT_ID, GaUtil.analyticsCache.segment_id(),
                        SEGMENT_NAME, GaUtil.analyticsCache.segment_name());
                CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                        StringUtils.stringifyObject(where));
            }
        }
    }

    @Override
    public void onValueChange(ValueChangeEvent<String> event) {
        fireEdit();
    }

    @Override
    public void onKeyUp(KeyUpEvent event) {
        fireEdit();
    }

    public void fireEdit() {
        TimeStampTracker.setTime(TimeStampTracker.Event.SELF_HELP_SEARCH_TRIGGERED_TIME, System.currentTimeMillis());
        String now = search.getText();
        if (now != null && now.length() > 0) {
            clear.setVisible(true);
        } else {
            clear.setVisible(false);
        }
        if (!previous.equals(now)) {
            showProgress();
            previous = now;
            delayer.doTrigger();
        }
    }

    public void setFocus() {
        Scheduler.get().scheduleFixedDelay(() -> {
            search.setFocus(true);
            return false;
        }, 100);
    }

    public String query() {
        return search.getText();
    }

    public void query(String query) {
        search.setText(query);
    }

    public void placeholder(String placeHolder) {
        placeHolder(search, placeHolder);
    }

    public static void placeHolder(com.google.gwt.user.client.ui.Widget w, String placeHolder) {
        if (placeHolder.length() != 0) {
            w.getElement().setAttribute("placeholder", placeHolder);
        }
    }

    public void setAriaAttribute(String aria_property, String aria_value) {
        search.getElement().setAttribute(aria_property, aria_value);
    }

    public void showBusy() {
        search.setEnabled(false);
        searchIcon.setEnabled(false);
        searchIcon.getElement().removeAllChildren();
        searchIcon.getElement().setInnerHTML(SVGElements.SPINNER);
    }

    public void showNormal() {
        search.setEnabled(true);
        searchIcon.setEnabled(true);
        searchIcon.getElement().removeAllChildren();
        searchIcon.getElement().setInnerHTML(SVGElements.SEARCH);
    }

    public void focus() {
        search.setFocus(true);
    }

    private String searchBoxCss() {
        return Common.CSS.headerSearch();
    }

    private String searchIconCss() {
        return Common.CSS.headerSearchIcon();
    }

    public Anchor getSearchIcon() {
        return searchIcon;
    }

    public Anchor getClear() {
        return clear;
    }

    public void disableAutocomplete() {
        search.getElement().setAttribute("autocomplete", "off");
    }

    public String widgetSearchClearTitle() {
        return Widget.CONSTANTS.widgetSearchClearTitle();
    }

    public String searchClear() {
        return Widget.CSS.searchClear();
    }

    public String searchStyle() {
        return Widget.CSS.search();
    }

    protected String widgetSearchTitle() {
        return Widget.CONSTANTS.widgetSearchTitle();
    }

    protected String searchGridProgress() {
        return Widget.CSS.searchGridProgress();
    }

    protected String searchGridStyle() {
        if (Enterpriser.hasFeature(AdditionalFeatures.sh_google_drive)) {
            return Widget.CSS.searchGridWithTab();
        } else {
            return Widget.CSS.searchGrid();
        }
    }

    protected String searchIconCellStyle() {
        if (Enterpriser.hasFeature(AdditionalFeatures.sh_google_drive)) {
            return Widget.CSS.searchIconCellWithTab();
        } else {
            return Widget.CSS.searchIconCell();
        }
    }

    protected String analyticsSource() {
        return TrackerEventOrigin.SELF_HELP.getSrcName();
    }

    protected boolean isSearchQueryEmpty() {
        return StringUtils.isBlank(query());
    }
}
