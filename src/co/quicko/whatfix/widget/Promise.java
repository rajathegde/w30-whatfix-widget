package co.quicko.whatfix.widget;


import com.google.gwt.user.client.rpc.AsyncCallback;

public class Promise<T> implements AsyncCallback<T> {

    private final Callable<T> success;
    private final Callable<Throwable> failure;

    public interface Callable<P> {
        void call(P res);
    }

    private Promise(Callable<T> success, Callable<Throwable> failure) {
        this.success = success;
        this.failure = failure;

    }

    @Override
    public void onFailure(Throwable caught) {
        this.failure.call(caught);
    }

    @Override
    public void onSuccess(T result) {
        this.success.call(result);
    }

    public static class PromiseBuilder<T> {

        private Callable<T> success;

        public PromiseBuilder<T> then(Callable<T> success) {
            this.success = success;
            return this;
        }

        public Promise<T> error(Callable<Throwable> failure) {
            return new Promise<>(success, failure);
        }

    }


    public static <T> PromiseBuilder<T> define() {
        return new PromiseBuilder<>();
    }

    public static <T> PromiseBuilder<T> wrap(AsyncCallback<T> callback) {
        PromiseBuilder<T> builder = new PromiseBuilder<T>();
        builder.error(callback::onFailure);
        return new PromiseBuilder<>();
    }

}
