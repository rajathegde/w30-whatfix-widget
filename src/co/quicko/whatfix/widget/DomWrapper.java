package co.quicko.whatfix.widget;

import com.google.gwt.dom.client.Element;

/**
 * Utility class to handle interaction with the DOM so that native methods are not present inside Java code
 */
public final class DomWrapper {

    private DomWrapper() {
    }

    public static native void removeTabListener(Element src) /*-{
        src.onkeydown = null;
    }-*/;

    public static native void setTabHandler(Element src, Element next)/*-{
        src.onkeydown = function (e) {
            if (e.keyCode == 9 && !e.shiftKey) {
                next.focus();
                e.preventDefault();
            }
        }
    }-*/;

    public static native void setShiftTabHandler(Element src, Element next)/*-{
        src.onkeydown = function (e) {
            if (e.keyCode == 9 && e.shiftKey) {
                next.focus();
                e.preventDefault();
            }
        }
    }-*/;

}
