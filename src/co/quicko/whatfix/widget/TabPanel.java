package co.quicko.whatfix.widget;

import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.widget.component.IconButton;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;

import java.util.HashMap;
import java.util.Map;

public class TabPanel {

    public enum Tabs {
        /* Do not rename, analytics events are linked to the enum name */
        TOP_HELP("Top Help", SVGElements.TOP_HELP),
        CHAT("Chat", SVGElements.CHAT_BW),
        HOME("Home", SVGElements.HOME),
        GOOGLE_DRIVE("Search in Drive", SVGElements.G_DRIVE);

        private final String icon;
        private final String header;

        Tabs(String headerText, String headerIcon) {
            this.header = headerText;
            this.icon = headerIcon;
        }
    }

    private Map<Tabs, TabHeader> tabButtons = new HashMap<>();
    private Tabs active;

    private final FlowPanel panel;
    private String widgetSrc;

    public TabPanel(boolean isVertical, String widgetSrc, Tabs... tabs) {
        panel = new FlowPanel();
        this.widgetSrc = widgetSrc;
        if (tabs.length > 1) {
            for (Tabs tab : tabs) {
                TabHeader but = new TabHeader(tab, isVertical);
                tabButtons.put(tab, but);
                panel.add(but);
                but.blur();

                but.addClickListener(event -> {
                    this.sendTabChangeEvent(tab.toString());
                    this.activate(but.type);
                });
            }

            panel.addStyleName(Widget.CSS.tabPanel());
            this.activate(tabs[0]);
        } else {
            TabHeader but = new TabHeader(Tabs.TOP_HELP, isVertical);
            tabButtons.put(Tabs.TOP_HELP, but);
            this.activate(Tabs.TOP_HELP);
        }
    }

    public void addClickHandler(Tabs tab, ClickHandler handler) {
        if (tabButtons.containsKey(tab)) {
            tabButtons.get(tab).addClickListener(handler);
        } else {
            // Log error here
        }
    }

    public FlowPanel getPanel() {
        return this.panel;
    }

    public boolean isActive(Tabs tab) {
        return this.active == tab;
    }

    private void activate(Tabs tab) {
        if (tabButtons.containsKey(tab)) {
            if (this.active != null) {
                tabButtons.get(this.active).blur();
            }
            this.active = tab;
            this.tabButtons.get(this.active).focus();
        }
    }

    private void sendTabChangeEvent(String tabName) {
        CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                StringUtils.stringifyObject(Where.create(
                        "event_type", GaUtil.PayloadTypes.tab_change.name(),
                        "tab_name", tabName,
                        "segment_name", GaUtil.analyticsCache.segment_name(),
                        "segment_id", GaUtil.analyticsCache.segment_id(),
                        "src_id", this.widgetSrc)));
    }

    public void activate(Tabs tab, boolean triggerClick) {
        this.activate(tab);
        if (triggerClick) {
            this.tabButtons.get(this.active).click();
        }
    }

    public Tabs getActiveTab() {
        return this.active;
    }

    private static class TabHeader extends IconButton {
        private final Tabs type;

        public TabHeader(Tabs tab, boolean isVertical) {
            super(tab.header, tab.icon, isVertical);
            getElement().getStyle().setCursor(Style.Cursor.POINTER);
            this.type = tab;

        }
    }
}
