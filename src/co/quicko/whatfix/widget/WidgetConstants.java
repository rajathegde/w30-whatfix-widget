package co.quicko.whatfix.widget;

import co.quicko.whatfix.i18n.client.Propertizable;

public interface WidgetConstants extends Propertizable {
    @DefaultStringValue("powered by whatfix.com")
    public String poweredTitle();

    @DefaultStringValue("powered by")
    public String powered();

    @DefaultStringValue("Enter your search criteria here")
    public String searchMore();

    @DefaultStringValue("Sorry, no results found")
    public String nothingFound(boolean returnKey);
    
    @DefaultStringValue("Close")
    public String widgetCloseTitle();
    
    @DefaultStringValue("Clear")
    public String widgetSearchClearTitle();
    
    @DefaultStringValue("Search")
    public String widgetSearchTitle();

    @DefaultStringValue("Self Help")
    public String widgetTitle(boolean returnKey);
    
    @DefaultStringValue("Sorry, no results found. Try refining your search or contact us using options given below:")
    public String selfHelpNothingFoundWithIcon();

    @DefaultStringValue("Self Help Content ends.")
    public String selfHelpEndMessage();

    @DefaultStringValue("No Results?")
    public String noResults();

    @DefaultStringValue("Do you have any suggestion to share about your last query?")
    public String searchFeedbacksuggest();

    @DefaultStringValue("Enter your suggestions here")
    public String searchFeedbackPlaceholder();

    @DefaultStringValue("Enter email id here or Share anonymously")
    public String searchFeedbackEmailPlaceholder();

    @DefaultStringValue("Send")
    public String searchFeedbackSend();

    @DefaultStringValue("Feedback is blank!!")
    public String emptyFeedbackError();

    @DefaultStringValue("The email is invalid")
    public String invalidEmailError();

    @DefaultStringValue("Sorry! Couldn't share your feedback")
    public String searchFeedbackError();

    @DefaultStringValue("Thank you!")
    public String thankYou();

    @DefaultStringValue("Your feedback/suggestion has been successfully shared")
    public String searchFeedbackSuccess();

    @DefaultStringValue("Sharing Email Id. Please add the email by ")
    public String shareMailId();

    @DefaultStringValue("Sharing Email Id: {mail_id}. To edit ")
    public String capturedMailId();

    @DefaultStringValue("clicking here")
    public String clickingHere();

    @DefaultStringValue("click here")
    public String clickHere();

    @DefaultStringValue("Sharing this feedback anonymously")
    public String shareAnonymously();
}
