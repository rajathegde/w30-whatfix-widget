package co.quicko.whatfix.widgetbase;

import com.google.gwt.aria.client.Roles;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.Common;

/**
 * Panel with header and content, wrapper over {@link DisclosurePanel}
 * 
 * Extending {@link FocusPanel} to obtain focus at the widget level.
 * 
 * @author sujeesh
 *
 */
public abstract class ExpandablePanel extends FocusPanel {

    private DisclosurePanel fullPanel;
    
    public ExpandablePanel() {
        fullPanel = new DisclosurePanel();
        Roles.getPresentationRole().set(fullPanel.getElement());
        fullPanel.setAnimationEnabled(true);
        setWidget(fullPanel);
    }

    protected void setupPanel() {
        fullPanel.setHeader(getHeader());
        fullPanel.setContent(getContent());
        StateChangeHandler handler = getHandler();
        fullPanel.addOpenHandler(handler);
        fullPanel.addCloseHandler(handler);
        fullPanel.setStateChangeCb(handler);
        handleEvents();
    }

    protected void setHeader() {
        fullPanel.setHeader(getHeader());
    }

    private StateChangeHandler getHandler() {
        return new StateChangeHandler(new StateCallback() {

            @Override
            public void onOpen() {
                handleOpen();
            }
            
            @Override
            public void onClose() {
                handleClose();
            }
            
            @Override
            public void preOpen() {
            }
            
            @Override
            public void postOpen() {
                handlePostOpen();
            }
        });
    }
    
    protected void addPanelStyle(String style) {
        fullPanel.addStyleName(style);
    }
    
    /**
     * href attribute of {@link DisclosurePanel} header ClickableHeader is
     * causing style issues in firefox, removing it to fix the issue.
     * Adding key handler to open / close panel on Enter key
     */
    private void handleEvents() {
        fullPanel.getHeader().getParent().getElement().removeAttribute("href");
        addKeyDownHandler(new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent event) {
                if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    fullPanel.setOpen(!fullPanel.isOpen());
                }
                event.stopPropagation();
            }
        });
        addStyleName(Common.CSS.expandablePanel());
    }
    
    public interface StateCallback  {
        public void preOpen();
        
        public void postOpen();
        
        public void onOpen();
        
        public void onClose();
    }

    protected abstract Widget getHeader();

    protected abstract Widget getContent();

    protected abstract void handleOpen();

    protected abstract void handleClose();
    
    public abstract void handlePostOpen();
}
