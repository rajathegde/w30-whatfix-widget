package co.quicko.whatfix.widgetbase;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.editor.StepCompletionActions;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.productanalytics.PAEventName;
import co.quicko.whatfix.productanalytics.PATracker;
import co.quicko.whatfix.workflowengine.data.strategy.Operators;

public class EditorTroubleshootExpandablePanel extends ExpandablePanel {
    private String stepDescription;
    private String status;
    private Draft flow;
    private int step;
    private EditorTroubleshootHeader header;
    private EditorTroubleshootContent content;
    private static final String SUPPORTLINK = "http://support.whatfix.com/";
    private static final String REFER = " You can also refer our ";
    private static final String TROUBLESHOOTGUIDE = "troubleshooting guide";

    public EditorTroubleshootExpandablePanel(int step, Draft flow,
            String status) {
        this.stepDescription = flow.step_description_md(step);
        this.flow = flow;
        this.step = step;
        this.status = status;
        setupPanel();
    }

    public void setStatus(String status) {
        this.status = status;
        header.updateStatus(status);
    }

    public void setFailureStatus(String reason) {
        setStatus(Events.STEP_FAILED.getValue());
        reason += REFER;
        content.updateFailureMessage(reason);
        header.statusWidget.getWidget(0)
                .addStyleName(Common.CSS.failureColor());
    }

    public String getStatus() {
        return status;
    }

    @Override
    protected Widget getHeader() {
        if (header == null) {
            header = new EditorTroubleshootHeader();
            addExpandableIconVisibilityRule();
        }
        header.getElement().setAttribute("data-wfx-id", "ET-header");
        return header;
    }

    @Override
    protected Widget getContent() {
        if (content == null) {
            content = new EditorTroubleshootContent();
        }
        content.getElement().setAttribute("data-wfx-id", "ET-content");
        return content;
    }

    @Override
    protected void handleOpen() {
        header.toggleIcon();
        PATracker.captureEventWithDefaultPayload(
                PAEventName.EditorTroubleshootEvent.STEP_DETAILS);
    }

    @Override
    protected void handleClose() {
        header.toggleIcon();

    }

    @Override
    public void handlePostOpen() {
        Common.scrollIntoView(getElement(), true);
    }

    protected void addExpandableIconVisibilityRule() {
        header.addStyleName(Common.CSS.dropDownIconVisibility());
    }

    protected class EditorTroubleshootHeader extends Grid {
        private boolean isOpen;
        private static final int ROW_SIZE = 1;
        // step, status, arrow
        private static final int COL_SIZE = 3;
        private FlowPanel statusWidget;

        public EditorTroubleshootHeader() {
            super(ROW_SIZE, COL_SIZE);
            Widget descriptionWidget = getHeaderTextWidget(step,
                    stepDescription);
            statusWidget = getHeaderTextWidget(status);
            Anchor spinnerIcon = Common.anchor(null);
            String iconStyle = SVGElements.SPINNER_EDITOR_TROUBLESHOOT;
            spinnerIcon.getElement().setInnerHTML(iconStyle);
            spinnerIcon.addStyleName(Common.CSS.spinner());

            if (status.equals(Events.STEP_START.getValue())) {
                statusWidget.getWidget(0)
                        .addStyleName(Common.CSS.stepSearchStatus());
                statusWidget.addStyleName(Common.CSS.flexDisplay());
                statusWidget.add(spinnerIcon);
            }
            setWidget(0, 0, descriptionWidget);
            setWidget(0, 1, statusWidget);
            setWidget(0, 2, addIcon());

            setCellPadding(0);
            setCellSpacing(0);
            getWidget(0, 1).getElement().setTabIndex(-2);
            getWidget(0, 1).getElement().setAttribute(AriaProperty.HIDDEN,
                    String.valueOf(true));
            getElement().setAttribute(AriaProperty.HIDDEN,
                    String.valueOf(true));
            addStyleName(Common.CSS.etExpandablePanelHeader());
        }

        protected FlowPanel getHeaderTextWidget(int stepNumber, String text) {
            // remove tags from html title text
            text = Common.html(text).getText();
            if (null == text || text.length() == 0) {
                // this can be the case only when no text is there, but some
                // image or video has been inserted
                text = "View Multimedia";
            }
            String title = stepNumber == -1 ? text
                    : Integer.toString(step) + ". " + text;
            FlowPanel headerWrapper = new FlowPanel();
            Label stepTitleLabel = Common.label(title,
                    Common.CSS.widgetAnchorElement());
            stepTitleLabel.setTitle(text);
            stepTitleLabel.addStyleName(Common.CSS.stepTitle());
            headerWrapper.add(stepTitleLabel);
            headerWrapper.addStyleName(Common.CSS.removeOutlineOnFocus());
            return headerWrapper;
        }

        protected FlowPanel getHeaderTextWidget(String title) {
            return getHeaderTextWidget(-1, title);
        }

        public void toggleIcon() {
            this.isOpen = !this.isOpen;
            setWidget(0, 2, addIcon());
        }

        private Anchor addIcon() {
            Anchor identifierIcon = Common.anchor(null);
            String iconStyle = this.isOpen
                    ? SVGElements.ANGLE_UP_EDITOR_TROBLESHOOT
                    : SVGElements.ANGLE_DOWN_EDITOR_TROBLESHOOT;
            identifierIcon.getElement().setInnerHTML(iconStyle);
            identifierIcon.getElement().setAttribute(AriaProperty.HIDDEN,
                    String.valueOf(true));
            return identifierIcon;
        }

        private void updateStatus(String status) {
            statusWidget = getHeaderTextWidget(status);
            header.setWidget(0, 1, statusWidget);
        }
    }

    protected class EditorTroubleshootContent extends FlowPanel {
        private HorizontalPanel failureMessagePanel;
        private Label failureMessageLabel;
        private FlowPanel advancedDetails;
        private Grid stepDetails;
        // type, display rule, url, completion rule
        private static final int CONTENT_ROW_SIZE = 4;
        private static final int CONTENT_COL_SIZE = 2;

        public EditorTroubleshootContent() {
            failureMessagePanel = new HorizontalPanel();
            failureMessagePanel.addStyleName(Common.CSS.failureMessagePanel());
            failureMessagePanel.add(getFailureIcon());
            failureMessageLabel = Common.label("",
                    Common.CSS.failureMessageLabel());
            failureMessagePanel.setVisible(false);
            advancedDetails = new FlowPanel();
            stepDetails = new Grid(CONTENT_ROW_SIZE, CONTENT_COL_SIZE);
            stepDetails.addStyleName(Common.CSS.stepContentPanel());
            advancedDetails.add(Common.label("Advanced Step details",
                    Common.CSS.advanceStepDetails()));
            advancedDetails.add(stepDetails);
            add(failureMessagePanel);
            add(advancedDetails);
            handleGrid(stepDetails);
        }

        private Anchor getFailureIcon() {
            Anchor failureIcon = Common.anchor(null);
            String iconStyle = SVGElements.FAILURE_EDITOR_TROUBLESHOOT;
            failureIcon.getElement().setInnerHTML(iconStyle);
            return failureIcon;
        }

        private void handleGrid(Grid grid) {
            addStepType(grid);
            addStepDisplayRule(grid);
            addURL(grid);
            addCompletionRules(grid);
        }

        public void updateFailureMessage(String reason) {
            FlowPanel failureMessage = new FlowPanel();
            failureMessageLabel.setText(reason);
            Anchor failureSupportLink = Common.anchor(TROUBLESHOOTGUIDE,
                    SUPPORTLINK, false, Common.CSS.failureMessageLabel(),
                    Common.CSS.etSupportLink());
            failureSupportLink.addClickHandler(event -> {
                PATracker.captureEventWithDefaultPayload(
                        PAEventName.EditorTroubleshootEvent.ERROR_LINK);
            });
            failureMessage.add(failureMessageLabel);
            failureMessage.add(failureSupportLink);
            failureMessagePanel.add(failureMessage);
            failureMessagePanel.setVisible(true);
        }

        private void addStepType(Grid grid) {
            String type = flow.step_optional(step) ? "Optional" : "Mandatory";
            if (null != flow.step_branch_conditions(step)
                    && flow.step_branch_conditions(step).length() > 0) {
                type += ", Branched";
            }
            grid.setWidget(0, 0, Common.label("Step type"));
            grid.setWidget(0, 1, Common.html(type));
        }

        private void addStepDisplayRule(Grid grid) {
            StringBuilder conditions = new StringBuilder();

            if (null == flow.step_conditions(step)
                    || flow.step_conditions(step).length() == 0) {
                conditions.append("Always show");
            } else {
                Set<String> uniqueConditions = new LinkedHashSet<>();
                for (int i = 0; i < flow.step_conditions(step).length(); i++) {
                    String type = flow.step_conditions(step).get(i).type();
                    StepDisplayRule displayRule = StepDisplayRule.getStepCompletion(type);
                    String text = "";
                    if (StepDisplayRule.SELECTED_ELEMENT.equals(displayRule)) {
                        text = Operators.getByKey(
                                flow.step_conditions(step).get(i).operator())
                                .toString();
                    } else {
                        text = displayRule.getText();
                    }

                    uniqueConditions.add(text);
                }
                Iterator<String> itr = uniqueConditions.iterator();
                while (itr.hasNext()) {
                    conditions.append(itr.next());
                    if (itr.hasNext()) {
                        conditions.append(",<br>");
                    }
                }
            }
            grid.setWidget(1, 0, Common.label("Step display rule"));
            grid.setWidget(1, 1, Common.html(conditions.toString()));
        }

        private void addURL(Grid grid) {
            grid.setWidget(2, 0, Common.label("Step creation page"));
            Label stepUrlLabel = Common.label(flow.step_url(step),
                    Common.CSS.etUrlLabel());
            stepUrlLabel.setTitle(flow.step_url(step));
            grid.setWidget(2, 1, stepUrlLabel);
        }

        private void addCompletionRules(Grid grid) {
            StringBuilder completionRules = new StringBuilder();
            if (null != flow.step_completion_actions(step)) {
                for (int i = 0; i < flow.step_completion_actions(step)
                        .length(); i++) {
                    completionRules.append(StepCompletionActions
                            .get(flow.step_completion_actions(step).get(i))
                            .getDisplayValue());
                    if (i < flow.step_completion_actions(step).length() - 1) {
                        completionRules.append(",<br>");
                    }
                }
            }
            grid.setWidget(3, 0, Common.label("Step completion rule"));
            grid.setWidget(3, 1, Common.html(completionRules.toString()));
        }
    }

    public enum Events {
        STEP_START("Searching"),
        STEP_FOUND("Displayed"),
        STEP_PLAYED("Completed"),
        STEP_FAILED("Failed");
        public String value;

        private Events(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    
    public enum StepDisplayRule {
        URL("url", "URL"),
        HOSTNAME("hostname", "URL Hostname"),
        PATH("path", "URL Path"),
        PARAMETERS("query", "URL Parameters"),
        HASH("hash", "URL Hash"),
        OTHER_ELEMENT("other_element", "Other Element on Page"),
        WINDOW_VARIABLE("variable", "Window Variable"),
        SELECTED_ELEMENT("element_selector", "Selected Element Is"),
        SELECTED_ELEMENT_TEXT("element_text", "Selected Element Text"),
        EMPTY("", "");
        
        private String type;
        private String text;
        
        private StepDisplayRule(String type, String text) {
            this.type = type;
            this.text = text;
        }
        
        public String getText() {
            return text;
        }
        
        public static StepDisplayRule getStepCompletion(String type) {
            for (StepDisplayRule stepCompletion : StepDisplayRule.values()) {
                if (stepCompletion.type.equalsIgnoreCase(type)) {
                    return stepCompletion;
                }
            }
            return EMPTY;
        }
    }
}
