package co.quicko.whatfix.widgetbase;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;

import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.Group;
import co.quicko.whatfix.widgetbase.WidgetBase.AbstractContentPosition;
import com.google.gwt.user.client.ui.Label;

public class ContentFlowPanel extends FlowPanel {
    private String flowId;
    private Group group;
    private Content content;
    private Anchor anchor;
    private boolean isContentFull;
    private AbstractContentPosition contentPosition;
    private Label newLabel;
    private WidgetGroupPanel parentGroupPanel;

    public ContentFlowPanel(Group group, boolean isContentFull) {
        this.flowId = group.flow_id();
        this.group = group;
        this.isContentFull = isContentFull;
    }

    public ContentFlowPanel(Content content, Group group) {
        this.flowId = content.flow_id();
        this.group = group;
        this.content = content;
    }
    
    public ContentFlowPanel(Content content, Group group,
            AbstractContentPosition contentPosition) {
        this(content, group);
        this.contentPosition = contentPosition;
    }

    public ContentFlowPanel(Content content, Group group,
            AbstractContentPosition contentPosition,
            WidgetGroupPanel parentGroupPanel) {
        this(content, group, contentPosition);
        this.parentGroupPanel = parentGroupPanel;
    }

    public ContentFlowPanel(Content content, boolean isContentFull) {
        this.content = content;
        this.isContentFull = isContentFull;
    }

    public ContentFlowPanel(Content content, Group group,
            boolean isContentFull) {
        this(content, group);
        this.isContentFull = isContentFull;
    }

    public ContentFlowPanel(Content content, Group group, boolean isContentFull,
            AbstractContentPosition contentPosition) {
        this(content, group, isContentFull);
        this.contentPosition = contentPosition;
    }

    public ContentFlowPanel(Content content, Group group, boolean isContentFull,
            AbstractContentPosition contentPosition,
            WidgetGroupPanel parentGroupPanel) {
        this(content, group, isContentFull, contentPosition);
        this.parentGroupPanel = parentGroupPanel;
    }

    public ContentFlowPanel(Content content, boolean isContentFull,
            Anchor anchor) {
        this(content, isContentFull);
        this.anchor = anchor;
    }
    
    public ContentFlowPanel(Content content, Group group, boolean isContentFull,
            Anchor anchor, AbstractContentPosition contentPosition) {
        this(content, group, isContentFull, anchor);
        this.contentPosition = contentPosition;
    }

    public ContentFlowPanel(Content content, Group group, boolean isContentFull,
            Anchor anchor) {
        this(content, group, isContentFull);
        this.anchor = anchor;
    }

    public Label getNewLabel() {
        return newLabel;
    }

    public void setNewLabel(Label newLabel) {
        this.newLabel = newLabel;
    }

    public void setAnchor(Anchor anchor) {
        this.anchor = anchor;
    }

    public String getFlowId() {
        return this.flowId;
    }

    public String getGroupId() {
        return (group != null) ? group.flow_id() : "-";
    }

    public String getGroupTitle() {
        return (group != null) ? group.title() : "-";
    }

    public Content getContent() {
        return content;
    }

    public Anchor getAnchor() {
        return anchor;
    }

    public boolean isContentFull() {
        return isContentFull;
    }

    public Group getGroup() {
        return group;
    }

    public AbstractContentPosition getPositionInfo() {
        return contentPosition;
    }

    public WidgetGroupPanel getParentGroupPanel() {
        return parentGroupPanel;
    }

    public void setParentGroupPanel(WidgetGroupPanel parentGroupPanel) {
        this.parentGroupPanel = parentGroupPanel;
    }
}
