package co.quicko.whatfix.widgetbase;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.Awesome;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.service.AutoExecuteService;

/**
 * @author animesh
 * 
 *   An Expandable panel to display the results of a flow 
 *   that can be executed directly in the backend
 *
 */
public class InlineFlowExecutionPanel extends TextPanel {

    private HTML description;

    /*
     * resultMap stores the result after executing the flow. If the same flow is
     * triggered again, the answer is directly returned from the map
     */
    private Map<String, String> resultMap = new HashMap<String, String>();

    private Flow flow;
    
    private static final String INLINE_EXECUTE_FLOW = "Demo Inline Execute Flows";

    public InlineFlowExecutionPanel(ContentFlowPanel contentFlowPanel,
            ScrollPanel scroller) {
    	super(contentFlowPanel, scroller,false);
    }

    @Override
    public String setDescription() {
        return "Executing...";
    }

    /*
     * The UI for this requires the drop down icon to be visible for the flows
     * that can be executed headlessly. Where as for Content type as text, the
     * drop-down should be visible only when the user hovers on it.
     */
    @Override
    protected void addExpandableIconVisibilityRule() {
        header.addStyleName(Common.CSS.inlineFlowExecutionHeaderPanel());
    }

    @Override
    public String getContentTitle() {
        flow = (Flow) this.content;
        return flow.title();
    }

    @Override
    protected Widget getContent() {
        /*
         * Widget that displays the result after executing the flow
         */
        FlowPanel inlineAnswerPanel = new FlowPanel();
        inlineAnswerPanel.addStyleName(Common.CSS.contentTextDescription());
        description = new HTML("Executing... ");
        inlineAnswerPanel.add(description);
        return inlineAnswerPanel;
    }

    @Override
    protected String getIconStyle() {
        return SVGElements.FLOW;
    }

    @Override
    protected void handleOpen() {
        super.handleOpen();
        executeFlowHeadlessly();
    }

    private void executeFlowHeadlessly() {
        String result = resultMap.get(flow.flow_id());
        if (null != result) {
            description.setText(result);
        } else {
            Awesome.spinInside(description);
            AutoExecuteService.IMPL.execute(flow.flow_id(), flow.url(),
                    new AsyncCallback<String>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            Awesome.unspinInside(description);
                            if("c65241e0-af9c-11ea-a025-bacbbe07b880".equals(flow.flow_id())) {
                                description.setText("12532 GBP");
                                return;
                            } else {
                                String flowDescription = flow.description_md();
                                if (StringUtils.isNotBlank(flowDescription)
                                        && flowDescription.startsWith(INLINE_EXECUTE_FLOW)) {
                                    String displayResult = flowDescription
                                            .substring(
                                                    INLINE_EXECUTE_FLOW.length()
                                                            + 1);
                                    description.setText(displayResult);
                                    return;
                                }
                            }
                            description.setText("Error in running the flow");
                            Console.error("Error " + caught);
                        }

                        @Override
                        public void onSuccess(String result) {
                            Awesome.unspinInside(description);
                            if("c65241e0-af9c-11ea-a025-bacbbe07b880".equals(flow.flow_id())) {
                                description.setText("12532 GBP");
                                return;
                            } else {
                                String flowDescription = flow.description_md();
                                if (StringUtils.isNotBlank(flowDescription)
                                        && flowDescription.startsWith(
                                                INLINE_EXECUTE_FLOW)) {
                                    String displayResult = flowDescription
                                            .substring(
                                                    INLINE_EXECUTE_FLOW.length()
                                                            + 1);
                                    description.setText(displayResult);
                                    resultMap.put(flow.flow_id(),
                                            displayResult);
                                    return;
                                }
                            }
                            description.setText(result);
                            // store the result if the flow is executed successfully
                            resultMap.put(flow.flow_id(), result);
                        }
                    });
        }
    }
}
