package co.quicko.whatfix.widgetbase;

import com.google.gwt.aria.client.Roles;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ScrollPanel;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.AbstractContent;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.Group;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.service.LocaleUtil;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.widgetbase.WidgetBase.AbstractContentPosition;
import com.google.gwt.user.client.ui.Widget;

public class WidgetGroupPanel extends TextPanel {
    private boolean isOpen;
    protected FlowPanel groupContentPanel;
    
    public WidgetGroupPanel(AbstractContent content, FlowPanel headerIcon,
            ScrollPanel scroller) {
        super(content, scroller);
        addHeaderStyle(Common.CSS.widgetGroupHeader());
    }

    public WidgetGroupPanel(AbstractContent content, FlowPanel headerIcon,
            AbstractContentPosition contentPosition, ScrollPanel scroller) {
        this(content, headerIcon, scroller);
        this.contentPosition = contentPosition;
        header.getParent().getElement().setAttribute(AriaProperty.LABEL,
                Common.CONSTANTS.collapsed() + "," + content.title());
        Roles.getPresentationRole().set(header.getElement());
    }

    public void addHeaderStyle(String style) {
        header.addStyleName(style);
    }
    
    @Override
    public String getContentTitle() {
        return ((Group) content).titleForLocale(LocaleUtil.locale(true));
    }

    @Override
    protected Widget getHeader() {
        Widget w = super.getHeader();
        w.getElement().setAttribute("data-wfx-id", "group-header");
        return w;
    }

    @Override
    protected com.google.gwt.user.client.ui.Widget getContent() {
        groupContentPanel = new FlowPanel();
        groupContentPanel.addStyleName(
                Common.CSS.widgetGroupContentPanel());
        groupContentPanel.getElement().setAttribute("data-wfx-id", "group-content");
        return groupContentPanel;
    }
    
    @Override
    protected String getIconStyle() {
        return isOpen ? SVGElements.GROUP_OPEN : SVGElements.GROUP;
    }
    
    public FlowPanel getContentPanel() {
        return groupContentPanel;
    }
    
    @Override
    protected void handleOpen() {
        header.toggleIcon();
        header.getParent().getElement().setAttribute(AriaProperty.LABEL,
                Common.CONSTANTS.expanded() + "," + content.title());
        trackOpen();

        isOpen = true;
        setHeaderIcon();
    }

    @Override
    protected void handleClose() {
        super.handleClose();
        isOpen = false;
        setHeaderIcon();
    }

    @Override
    protected void trackOpen() {
        CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                StringUtils.stringifyObject(Where.create("group_id",
                        ((Content) content).flow_id(), "group_title",
                        ((Content) content).title(), "event_type",
                        GaUtil.PayloadTypes.group_open.toString(),
                        "segment_name", GaUtil.analyticsCache.segment_name(),
                        "segment_id", GaUtil.analyticsCache.segment_id(),
                        "src_id", widgetSrc())));
    }

    @Override
    protected void trackClose() {
        CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                StringUtils.stringifyObject(Where.create("group_id",
                        ((Content) content).flow_id(), "group_title",
                        ((Content) content).title(), "event_type",
                        GaUtil.PayloadTypes.group_close.toString(),
                        "segment_name", GaUtil.analyticsCache.segment_name(),
                        "segment_id", GaUtil.analyticsCache.segment_id(),
                        "src_id", widgetSrc())));
    }
    
    @Override
    protected Integer getPosition() {
        return contentPosition.getGroupPosition();
    }

    public void removeNewLabel() {
        header.setWidget(0, 3, header.getWidget(0, 2));
        showNewLabel = !showNewLabel;
    }
}
