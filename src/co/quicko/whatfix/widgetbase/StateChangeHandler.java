package co.quicko.whatfix.widgetbase;

import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.DisclosurePanel.StateChangeCallback;

import co.quicko.whatfix.widgetbase.ExpandablePanel.StateCallback;

public class StateChangeHandler
        implements OpenHandler<DisclosurePanel>, CloseHandler<DisclosurePanel>, StateChangeCallback {
    
    private static DisclosurePanel curDisclosePanel = null;
    private StateCallback callback;

    public StateChangeHandler(StateCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onOpen(OpenEvent<DisclosurePanel> event) {
        callback.onOpen();
    }

    @Override
    public void onClose(CloseEvent<DisclosurePanel> event) {
        callback.onClose();
    }
    
    @Override
    public void preOpen(DisclosurePanel panel){
        boolean closeRequired = handleCloseRequired(panel);
        if (panel.isOpen()) {
            curDisclosePanel = null;
        } else {
            if (closeRequired) {
                curDisclosePanel.setOpen(false);
                curDisclosePanel = null;
            }
            curDisclosePanel = panel;
        }
        callback.preOpen();
    }
    
    private boolean handleCloseRequired(DisclosurePanel fullPanel) {
        return curDisclosePanel != null && !curDisclosePanel.getElement()
                .isOrHasChild(fullPanel.getElement());
    }
    
    @Override
    public void postOpen(DisclosurePanel panel) {
        callback.postOpen();
    }
}
