package co.quicko.whatfix.widgetbase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.aria.client.Roles;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.AriaPropertyValues;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.Common.Progressor;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.PlayInvoker;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.ShortcutHandler;
import co.quicko.whatfix.common.ShortcutHandler.Shortcut;
import co.quicko.whatfix.common.ShortcutHandler.ShortcutListener;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo.OuterKey;
import co.quicko.whatfix.data.AbstractContent;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.Content.Video;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Enterprise;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.Group;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.extension.util.Extension;
import co.quicko.whatfix.extension.util.ExtensionHelper;
import co.quicko.whatfix.extension.util.Runner;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.i18n.client.Propertizable;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.Launcher.Settings;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.data.WfxData;
import co.quicko.whatfix.overlay.data.AppUser.PersonalisationAttributes;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.Callbacks.InvalidatableCb;
import co.quicko.whatfix.service.ContentManager;
import co.quicko.whatfix.service.LocaleUtil;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.widgetbase.TextPanel.Header;
import co.quicko.whatfix.workflowengine.app.AppFactory;

public abstract class WidgetBase<T extends Content> extends VerticalPanel
        implements ShortcutListener {

    public static final String DATA_WFX_ID = "data-wfx-id";


    protected String ent_id;
    protected String filter_by_tag;
    protected HorizontalPanel brand;
    protected FlowPanel flows;
    protected FlowPanel allContents;
    protected Anchor logo;
    protected Label poweredLabel;
    protected Anchor close;
    protected SimplePanel container;

    protected ScrollPanel scroller;
    protected String[] type;
    protected String[] value;
    protected String mode;
    protected JsArrayString order;
    private InvalidatableCb<JsArray<T>> latestWidgetCb;
    private String prefix;
    protected ContentManager manager = GWT.create(ContentManager.class);
    protected int contentsInWidget;

    private Shortcut escape = new Shortcut(KeyCodes.KEY_ESCAPE, false, false,
            false);
    protected boolean no_initial_flows;
    private Settings settings;

    private static final String AUTO_EXECUTE_FLOWS = "Demo Auto Execute Flows";
    private static final String INLINE_EXECUTE_FLOW = "Demo Inline Execute Flows";


    public WidgetBase(Settings settings, String prefix) {
        this.prefix = prefix;
        initializeGroups(settings);
        this.settings = settings;
        allContents = new FlowPanel();
        ent_id = settings.ent_id();
        mode = settings.mode();
        order = settings.order();
        filter_by_tag = settings.filter_by();
        no_initial_flows = settings.no_initial_flows();
        manager.initialize(ent_id);
        String[][] typeValue = settings.mixedTypeValue();
        /*
         *  typeValue is a list of flow_ids or tag_ids to show
         *  type = tags | tag_ids | flow_ids
         *  
         */
        type = typeValue[0];
        value = typeValue[1];

        setStyleName(styleWidget());
        if (AppFactory.isNonBrowserApp()) {
            logo = Common.anchor("", "#", false);
            logo.addClickHandler(event -> {
                event.preventDefault();
                Content desktopMessageContent = DataUtil.create();
                desktopMessageContent.url(Common.referralUrl());
                desktopMessageContent.type(ContentType.link.toString());
                CrossMessager.sendMessageToParent(prefix("_desktop_content"),
                        StringUtils.stringifyObject(desktopMessageContent));
            });
        } else {
            logo = Common.anchor("", Common.referralUrl(), false);
        }
        logo.getElement().setInnerHTML(SVGElements.WHATFIX_LOGO);
        logo.addStyleName(brandLogoStyle());
        logo.setTitle(logoTitle());
        poweredLabel = poweredLabel();
        brand = Common.actions(poweredLabel, logo);
        Roles.getPresentationRole().set(brand.getElement());
        if (ent_id != null) {
            brand.setVisible(false);
        }

        flows = new FlowPanel();
        Widget progress = getProgressor();
        flows.add(progress);

        scroller = new ScrollPanel(flows);
        scroller.setStyleName(styleScroller());

        container = new SimplePanel(scroller);
        container.setStyleName(styleContainer());

        close = Common.anchor("", "#", true, styleClose());
        close.getElement().setInnerHTML(SVGElements.CANCEL_CIRCLE);
        close.getElement().setAttribute(DATA_WFX_ID, this.prefix + "-close");
        close.addClickHandler(event -> closeWidget("cross"));
        if (hideOnEscape()) {
            ShortcutHandler.register(escape, this);
        }
    }

    protected String getWidgetCrossIcon(String themeColor) {
        return StringUtils.isBlank(themeColor) ? SVGElements.CANCEL_CIRCLE
                : SVGElements.getWidgetCrossIcon(themeColor);
    }

    private void initializeGroups(Settings settings) {
        JsArrayString order = settings.order();
        if (order == null || order.length() == 0) {
            return;
        }
        JavaScriptObjectExt segmentGroups = DataUtil.create().cast();
        for (int index = 0; index < order.length(); index++) {
            String contentId = order.get(index);
            if (Enterpriser.enterprise().groups().hasKey(contentId)) {
                Group segmentGroup = Enterpriser.enterprise().group(contentId);
                segmentGroups.put(contentId, segmentGroup);
            }
        }
        settings.groups(segmentGroups);
    }

    protected void clearFlowsPanel() {
        flows.clear();
    }


    public void showContent(final AsyncCallback<JsArray<T>> cb,
            boolean showCrawledContent) {

        if (no_initial_flows) {
            delegatingCb(cb, false, false).onSuccess(null);
        } else {
            Enterprise ent = Enterpriser.enterprise();
            String filter_order_type = null;
            if (ent != null && ent.auto_segment_enabled()
                    && ent.show_all_applicable_content()) {
            	filter_order_type = "OR_FIRST";
            }
            /*
             * type = filter_by which is sent to the API
             * value[0] = contains list of flow_ids or tag_ids to be shown
             * type[0] = flow_ids | tag_ids | tags
             */
            if(isPersonalisationEnabled()
            		&& type != null) {
            	if(type[0].equals("flow_ids")) {
	            	String userFlowIds = "";
	            	for(int idx=0; idx < this.order.length(); idx++) {
	            		userFlowIds += this.order.get(idx);
	            		if(idx < this.order.length()-1){
	            			userFlowIds += ",";
	            		}
	            	}
	            	value[0] = value[0] + "," + userFlowIds;
            	}
            }
            
            manager.topHelp(type, value, order, filter_order_type,
                    new AsyncCallback<JsArray<Content>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            delegatingCb(cb, false, false).onFailure(caught);
                        }

                        @Override
                        public void onSuccess(JsArray<Content> result) {
                            getDelegatingCb(cb).onSuccess((JsArray<T>) result);
                        }
                    });
        }
    }

    protected AsyncCallback<JsArray<T>> getDelegatingCb(
            AsyncCallback<JsArray<T>> cb) {
        return delegatingCb(cb, true, true);
    }

    protected void updateBranding() {
        Enterprise enterprise = Enterpriser.enterprise();
        if (enterprise != null) {
            Propertizable constants = constants();
            constants.use(Enterpriser.properties(LocaleUtil.locale(true)));
            Extension.CONSTANTS
                    .use(Enterpriser.properties(LocaleUtil.locale(true)));
            String brandingLabel = enterprise.brandingConfig().branding_text();
            String brandingLink = enterprise.brandingConfig().branding_link();
            if (StringUtils.isNotBlank(brandingLabel)
                    || StringUtils.isNotBlank(brandingLink)) {
                if (StringUtils.isNotBlank(brandingLabel)) {
                    updateCustomBranding(brandingLabel);
                }
                if (StringUtils.isNotBlank(brandingLink)) {
                    logo.setHref(brandingLink);
                }
            } else {
                updateBrand(poweredTitle(), powered());
            }
            setBranding();
            setAccessibilityAttributes();
        }
    }

    protected void setBranding() {
        brand.setVisible(!Enterpriser.enterprise().no_branding());
    }
        
    protected void updateCustomBranding(String poweredTitle) {
        logo.setTitle(poweredTitle);
        logo.removeStyleName(brandLogoStyle());
        logo.getElement().setInnerHTML(poweredTitle);
        logo.addStyleName(poweredLabel.getStyleName());
        poweredLabel.setText("");
    }

    protected void setAccessibilityAttributes() {
        close.setTitle(closeTitle());
    }

    protected AsyncCallback<JsArray<T>> delegatingCb(
            final AsyncCallback<JsArray<T>> emptyCb, final boolean isFlowFull,
            final boolean needGrouping) {
        InvalidatableCb<JsArray<T>> invalidateableCb = new InvalidatableCb<JsArray<T>>(
                new AsyncCallback<JsArray<T>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        handleFailure(caught);
                        emptyCb.onFailure(caught);
                    }

                    @Override
                    public void onSuccess(JsArray<T> result) {
                        handleFlows((JsArray<T>) result, isFlowFull,
                                needGrouping);
                        emptyCb.onSuccess((JsArray<T>) result);
                    }
                });
        if (latestWidgetCb != null) {
            latestWidgetCb.invalidate();
        }
        latestWidgetCb = invalidateableCb;
        return invalidateableCb;
    }

    private void handleFailure(Throwable caught) {
        contentsInWidget = 0;
        flows.clear();
        showNothingFound();
    }

    private void handleFlows(JsArray<T> result, boolean isFlowFull,
            boolean needGrouping) {
        clearFlowsPanel();

        if (result == null || result.length() == 0) {
            contentsInWidget = 0;
            showNothingFound();
            return;
        }

        Iterator<T> iterator;
        iterator = new JsIterator<T>(result);

        flows.add(handleFlows(iterator, isFlowFull, needGrouping));
    }

    protected abstract String widgetSrc();

    protected void showNothingFound() {
        flows.add(Common.label(nothingFound(), styleElement(), styleNothing()));
    }

    protected void updateBrand(String poweredTitle, String powered) {
        logo.setTitle(poweredTitle);
        poweredLabel.setText(powered);
    }

    protected void closeWidget(String content) {
        Where where = Where.create("closeBy", content, "segment_name",
                GaUtil.analyticsCache.segment_name(), "segment_id",
                GaUtil.analyticsCache.segment_id(), "src_id", widgetSrc());
        CrossMessager.sendMessageToParent(prefix("_close"),
                StringUtils.stringifyObject(where));
        ShortcutHandler.unregister(escape, this);
    }

    protected void updateUnseenContent(String contentId) {
        // Update Unseen only in case of Self Help
    }

    protected String prefix(String type) {
        return prefix + type;
    }

    protected void addGAContextualInfo(
            Map<ContextualInfo.OuterKey, ? extends JavaScriptObject> outerKeyMap) {
    }
    

    protected void setContextualInfo(
            ContentFlowPanel contentFlowPanel, String source) {
        Map<ContextualInfo.OuterKey, JavaScriptObject> map = new HashMap<ContextualInfo.OuterKey, JavaScriptObject>();
        map.put(OuterKey.POSITION, makePositionPayload(contentFlowPanel));
        map.put(OuterKey.CRAWL, makeSourcePayload(source));
        addGAContextualInfo(map);
        return;
    }

    protected JavaScriptObject makePositionPayload(
            ContentFlowPanel contentFlowPanel) {
                return DataUtil.create();
    }

    protected JavaScriptObject makeSourcePayload(String source) {
        return DataUtil.create();
    }
    protected abstract String brandLogoStyle();

    protected abstract String logoTitle();

    protected abstract String poweredTitle();

    protected abstract Label poweredLabel();

    protected abstract String powered();

    protected abstract String nothingFound();

    protected abstract String closeTitle();

    protected abstract Propertizable constants();

    protected abstract String styleWidget();

    protected abstract String styleScroller();

    protected abstract String styleContainer();

    protected abstract String styleClose();

    protected abstract String styleElement();

    protected abstract String styleNothing();

    protected Widget getProgressor() {
        Progressor progressor = new Progressor();
        progressor.showProgress();
        return progressor;
    }

    public class FlowHandler implements ClickHandler {
        private Flow flow;
        protected FocusWidget anchor;
        private boolean isFlowFull = false;
        private ContentFlowPanel contentFlowPanel;
        private Group group;

        public FlowHandler(ContentFlowPanel contentFlowPanel) {
            this.contentFlowPanel = contentFlowPanel;
            this.flow = (Flow) contentFlowPanel.getContent();
            this.anchor = contentFlowPanel.getAnchor();
            this.group = contentFlowPanel.getGroup();
        }

        @Override
        public void onClick(ClickEvent event) {
            GaUtil.analyticsCache.group_id(contentFlowPanel.getGroupId());
            GaUtil.analyticsCache.group_title(contentFlowPanel.getGroupTitle());
            setContextualInfo(contentFlowPanel, flow.source());
            updateUnseenInGroup();
            updateNotification(contentFlowPanel);
            if ("url".equals(mode)) {
                CrossMessager.sendMessageToParent("wfx_run", JsonUtils.stringify(flow));
                return;
            }

            if (!("live".equals(mode) || "live_here".equals(mode)
                    || "live_here_popup".equals(mode))) {
                showSlideShow(flow);
                return;
            }

            /*
             * Play flow in new tab for other enterprise. This change is for a
             * POC for organization level search.
             * Slide show isn't supported yet.
             */
            if (!flow.ent_id().equals(Enterprise.enterprise().ent_id())) {
                runLive(anchor, prefix, flow);
                return;
            }

            if ("live".equals(mode)) {
                handle(flow);
                return;
            }
            fetchFlow();
        }

        private void updateUnseenInGroup() {
            if (group == null) {
                return;
            }
            group.unseenContentCount(group.unseenContentCount() - 1);
        }

        public Flow getFlow() {
            return flow;
        }

        protected void fetchFlow() {
            manager.flow(flow.flow_id(), new AsyncCallback<Flow>() {
                @Override
                public void onFailure(Throwable caught) {
                }

                @Override
                public void onSuccess(Flow flow) {
                    isFlowFull = true;
                    handle(flow);
                }
            });

        }

        protected void handle(Flow flow) {
            if (!flow.seeLive()) {
                showSlideShow(flow);
            } else if ("live".equals(mode)) {
                runLive(anchor, prefix, flow);
            } else if ("live_here".equals(mode)
                    || "live_here_popup".equals(mode)) {
                if (flow.needExtension()) {
                    runLive(anchor, prefix, flow);
                } else {
                    if ("live_here_popup".equals(mode)
                            || flow.isPlayAsPopup()) {
                        startLiveHerePopup(flow);
                    } else {
                        startLiveHere(flow);
                    }
                }
            } else {
                showSlideShow(flow);
            }
        }

        protected void showSlideShow(Flow flow) {
            performAction(flow, prefix("_run"));
        }

        protected void startLiveHerePopup(Flow flow) {
            performAction(flow, "embed_run_popup_secure");
        }

        protected void startLiveHere(Flow flow) {
            performAction(flow, "embed_run_secure");
        }

        private void performAction(Flow flow, String action) {
            if(AppFactory.isMobileApp()) {
                /*give some time for mobile native to get the flow message 
                 * before closing the widget.*/
                String message = Runner.stateForEmbed(flow);
                CrossMessager.sendMessageToParent(action, message);
                closeWidget("flow/click");
            } else {
                closeWidget("flow/click");
                String message = Runner.stateForEmbed(flow);
                CrossMessager.sendMessageToParent(action, message);
            }
        }

        protected void runLive(FocusWidget widget, final String action,
                final Flow flow) {
            final HasHTML run = (HasHTML) widget;

            Runner.shouldRun(run, action, flow, new AsyncCallback<Void>() {
                @Override
                public void onSuccess(Void result) {
                    justRun(action, flow);
                }

                @Override
                public void onFailure(Throwable caught) {
                }
            });
        }

        private void justRun(String action, Flow flow) {
            if (isFlowFull) {
                Runner.justRun(action, flow);
                return;
            }

            if (ExtensionHelper.isInstalled()) {
                Runner.justRun(action, flow.flow_id(), null, flow.ent_id());
            } else {
                Runner.justRun(action, flow);
            }
        }
    }
    
    private static class JsIterator<T extends JavaScriptObject>
            implements Iterator<T> {
        private JsArray<T> array;
        private int index;

        public JsIterator(JsArray<T> result) {
            this.array = result;
        }

        @Override
        public boolean hasNext() {
            return index < array.length();
        }

        @Override
        public T next() {
            try {
                return array.get(index);
            } finally {
                index++;
            }
        }

        @Override
        public void remove() {
        }
    }

    @Override
    public void onShortcut(Shortcut shortcut) {
        closeWidget(shortKeyName(shortcut.key_code()));
    }

    private String shortKeyName(int keyCode) {
        if (keyCode == escape.key_code()) {
            return "escape";
        } else {
            return "shortcut";
        }
    }

    protected boolean hideOnEscape() {
        return false;
    }
    
    protected void setBorderRadius(com.google.gwt.user.client.Element element,
            String position) {
        Common.setBorderRadius(element, position);
    }

    static native JavaScriptObject mainWindow() /*-{
		return $wnd.parent;
    }-*/;

    protected Widget handleFlows(Iterator<T> contents, boolean isFlowfull,
            boolean needGrouping) {
        allContents.clear();
        allContents.addStyleName(Common.CSS.widgetContentListPanel());
        allContents.getElement().setAttribute(DATA_WFX_ID, this.prefix + "-all-contents");

        Iterator<? extends AbstractContent> contentWithGroup = arrangeContentWithGroup(
                contents, needGrouping);
        int currentPosition = 0;
        while (contentWithGroup.hasNext()) {
            currentPosition += 1;
            AbstractContent content = contentWithGroup.next();
            Boolean hasContent = true;
            if(content.isGroup()) {
            	Group group = (Group) content;
            	hasContent = !group.isEmpty();
            }
            if(hasContent) {
            	FlowPanel contentPanel = getContentPanel(content, isFlowfull, null,
                    new AbstractContentPosition(currentPosition, null), null);
            	allContents.add(sequence(contentPanel, content));
            }
        }

        contentsInWidget = currentPosition;
        postFlowsSetup();
        return allContents;
    }

    protected void postFlowsSetup() {
        return;
    }

    /**
     * Adding Groups to the List of content fetched from the server to display
     * in the widget. No groups are fetched as part of top_help or tasks
     * response. Group information is part of segment settings. Also arranging
     * contents with Group included in.
     * 
     * @param contents
     *            list of content fetched in the top_help or task response
     * @return mixedContentList list of arranged content including groups.
     */
    private Iterator<? extends AbstractContent> arrangeContentWithGroup(
            Iterator<T> contents, boolean needGrouping) {
        JsArrayString order = settings.order();
        if (!needGrouping || settings.order() == null
                || !settings.isGroupPresent()) {
            return contents;
        }

        List<AbstractContent> mixedContentList = new ArrayList<>();

        JavaScriptObjectExt groups = settings.groups();

        List<String> grouped_contents = new ArrayList<>();

        for (String key : groups.keys()) {
            Group group = (Group) groups.value(key);
            grouped_contents
                    .addAll(Arrays.asList(((Group) group).contentAsArray()));
            
        }
        
        Set<String> userPersonalisedContentOrderSet = new HashSet<>();
        if (isPersonalisationEnabled()
        		&& !WfxData.endUser().isEmpty() && WfxData.endUser().custom().hasKey("content_order")){
        
        	String[] contentInOrderIds = WfxData.endUser().custom().valueAsStringArray("content_order");
        	Collections.addAll(userPersonalisedContentOrderSet, contentInOrderIds);
        	Content content = contents.hasNext() ? contents.next() : null;
        	/*
        	 * Order will always contain more elements than what is returned from API, 
        	 * If more content is returned, it is by default added at the end
        	 * 
        	 */
        	for (int index = 0; index < order.length(); index++) {
                String contentId = order.get(index);
                
                if (groups.hasKey(contentId)) {
                    Group group = (Group) groups.value(contentId);
                    mixedContentList.add(group);
                } else if(content != null && content.flow_id().equals(contentId)){

                		/*
                		 * Not part of a group i.e. normal content then add to self help
                		 * or content is part of a group but is part of personalised content then also
                		 * add, basically duplicating content outside the group in case of personalisation
                		 */
                    	if (!grouped_contents.contains(content.flow_id()) 
                    			|| userPersonalisedContentOrderSet.contains(content.flow_id())) {
                            mixedContentList.add(content);
                        } 
                    	/*
                    	 * Move forward to next content only if this content has been processed
                    	 */
                    	content = contents.hasNext() ? contents.next() : null;
                    
                }
            }
        } else {
        	/*
        	 * This is the default logic of ordering, with bug
        	 * bug is when content is deleted from dashboard but segment is not updated,
        	 * the group position is locked. 
        	 * Same with IS, if content is filtered via IS, the relative group order is not respected.
        	 */
        	for (int index = 0; index < order.length(); index++) {
                String contentId = order.get(index);
                if (groups.hasKey(contentId)) {
                    Group group = (Group) groups.value(contentId);
                    // TODO: Add mixedContentList in the if condition to avoid empty
                    // groups
                    // if (!group.isEmpty()) {}
                    mixedContentList.add(group);
                } else if(contents.hasNext()){
                    Content content = contents.next();
                	if (!grouped_contents.contains(content.flow_id())) {
                        mixedContentList.add(content);
                    }
                }
            }
        }

        while (contents.hasNext()) {
            Content content = contents.next();
            if (!grouped_contents.contains(content.flow_id())) {
                mixedContentList.add(content);
            } else if(Enterpriser.hasFeature(AdditionalFeatures.personalisation_content_reorder)
            		&& userPersonalisedContentOrderSet.contains(content.flow_id())) {
            	mixedContentList.add(content);
            }
        }
            
        return mixedContentList.iterator();
    }

    private FlowPanel getContentPanel(AbstractContent abstractContent,
            boolean isContentFull, Group group,
            AbstractContentPosition contentPosition,
            WidgetGroupPanel parentGroupPanel) {
        if (abstractContent.isGroup()) {
            contentPosition.setAsGroup();
            FlowPanel groupPanel = getGroupPanel((Group) abstractContent,
                    isContentFull, contentPosition);
            Roles.getPresentationRole().set(groupPanel.getElement());
            return groupPanel;
        } else {
            Content content = (Content) abstractContent;
            FlowPanel dropDownPanel = getExpandableContentPanel(content, group,
                    contentPosition, parentGroupPanel);
            if (dropDownPanel != null) {
                Roles.getPresentationRole().set(dropDownPanel.getElement());
            }
            return dropDownPanel != null ? dropDownPanel
                    : getNonTextContentPanel(content, isContentFull, group,
                            contentPosition, parentGroupPanel);
        }
    }

    protected FlowPanel sequence(FlowPanel panel, AbstractContent content) {
        return panel;
    }
    
    protected FlowPanel sequenceInGroup(FlowPanel panel, Content content) {
        return panel;
    }

    /*
     * Expandable Panel is for Content type as text and for flows that can be
     * executed in a headless browser
     */
    protected FlowPanel getExpandableContentPanel(Content content, Group group,
            AbstractContentPosition contentPosition,
            WidgetGroupPanel parentGroupPanel) {
        final FlowPanel contentPanel = new FlowPanel();
        ExpandablePanel contentDropDownpanel;
        ContentFlowPanel contentFlowPanel = new ContentFlowPanel(content, group,
                contentPosition, parentGroupPanel);
        boolean showNewLabel = isNewLabelApplicable(content.flow_id(),
                settings);
        if (content.getType() == ContentType.text) {
			contentDropDownpanel = getTextPanel(contentFlowPanel, scroller, showNewLabel);
            contentDropDownpanel.getElement().setAttribute(AriaProperty.LIVE,
                    AriaPropertyValues.OFF);
            Roles.getPresentationRole().set(contentDropDownpanel.getElement());
        } else if (canBeExecutedInline(content)) {
            contentDropDownpanel = new InlineFlowExecutionPanel(
                    contentFlowPanel, scroller);
        } else {
            return null;
        }
        contentDropDownpanel
                .addFocusHandler(getAnchorFocushandler(contentPanel));
        contentDropDownpanel.addBlurHandler(getAnchorBlurHandler(contentPanel));
        contentPanel.add(contentDropDownpanel);
        setId(content, contentDropDownpanel);
        return contentPanel;
    }

    protected TextPanel getTextPanel(ContentFlowPanel contentFlowPanel,
            ScrollPanel scroller, boolean  showNewLabel) {    	
        TextPanel textPanel = new TextPanel(contentFlowPanel, scroller, showNewLabel);
        textPanel.getElement().setAttribute(DATA_WFX_ID, this.prefix + "-content-text");
        Roles.getPresentationRole().set(textPanel.getElement());

        return textPanel;
    }

    /*
     * This is for PoC and might be removed later
     * 
     * Checks whether a flow can be executed in a headless browser and the
     * answers shown inline
     */
    private boolean canBeExecutedInline(Content content) {
        ContentType contentType = content.getType();
        if (contentType == ContentType.flow) {
            Flow flow = (Flow) content;
            String flowDescription = JsUtils
                    .getTextFromHtml(flow.description_md());
            if (StringUtils.isNotBlank(flowDescription)) {
                return flowDescription.startsWith(INLINE_EXECUTE_FLOW);
            }
        }
        return false;
    }

    protected FlowPanel getGroupPanel(final Group group,
            final boolean isContentFull,
            AbstractContentPosition contentPosition) {
        final ContentFlowPanel contentPanel = new ContentFlowPanel(group,
                isContentFull);
        FlowPanel iconPanel = new FlowPanel();
        iconPanel.add(getWidgetIcon(contentPanel));
        WidgetGroupPanel groupPanel = new WidgetGroupPanel(group, iconPanel,
                contentPosition, scroller) {
            @Override
            protected void handleOpen() {
                super.handleOpen();
                /*
                 * Group panel has to be scrolled when it is opened so that the
                 * content inside it come in the view frame of self help
                 */
                this.handlePostOpen();
            }
        };
        addGroupContent(group, groupPanel);
        groupPanel.addFocusHandler(getAnchorFocushandler(contentPanel));
        groupPanel.addBlurHandler(getAnchorBlurHandler(contentPanel));
        groupPanel.getElement().setAttribute(AriaProperty.LIVE,
                AriaPropertyValues.OFF);
        Roles.getPresentationRole().set(getElement());
        contentPanel.add(groupPanel);
        contentPanel.getElement().setAttribute(DATA_WFX_ID, this.prefix + "-content-group");
        // TODO need to add id for the group and group contents
        // setId(content, groupPanel);
        return contentPanel;
    }

    protected void addGroupContent(final Group group,
            final WidgetGroupPanel groupPanel) {
    	
        if (group.isEmpty()) {
            return;
        }
        
        final FlowPanel groupContentPanel = groupPanel.getContentPanel();
        List<String> type = new ArrayList<>();
        List<String> value = new ArrayList<>();
        type.add("flow_ids");
        value.add(group.content_ids().join());
        if (this.type != null) {
            for (int i = 0; i < this.type.length; i++) {
                if ("tag_ids".equals(this.type[i])) {
                    type.add("tag_ids");
                    value.add(this.value[i]);
                    break;
                }
            }
        }
        manager.groupContents(type.toArray(new String[type.size()]),
                value.toArray(new String[value.size()]), group.content_ids(),
                getGroupContentHandler(group, groupPanel, groupContentPanel));

    }

    protected AsyncCallback<JsArray<Content>> getGroupContentHandler(Group group,
            WidgetGroupPanel groupPanel, FlowPanel groupContentPanel) {
        return new AsyncCallback<JsArray<Content>>() {
            @Override
            public void onFailure(Throwable caught) {
                Console.error(caught.getMessage());
            }

            @Override
            public void onSuccess(JsArray<Content> result) {
                int unseenContentCount = 0;
                groupContentPanel.clear();
                Iterator<Content> iterator = new JsIterator<>(result);
                int currentPosition = 1;

                if(!iterator.hasNext()) {
                    groupPanel.setVisible(false);
                    return;
                }

                while (iterator.hasNext()) {
                    Content content = iterator.next();
                    if (isNewLabelApplicable(content.flow_id(), settings)) {
                        groupPanel.showNewLabel = true;
                        Header groupHeader = groupPanel.header;
                        groupHeader.setWidget(0, 2, groupHeader.addIcon());
                        groupHeader.remove(groupHeader.getWidget(0, 3));
                        Label newLabel = Common.label(
                                Overlay.CONSTANTS.newLabel(),
                                Common.CSS.newLabel());
                        groupHeader.setWidget(0, 3, newLabel);
                        unseenContentCount++;
                    }
                    group.unseenContentCount(unseenContentCount);
                    FlowPanel contentPanel = getContentPanel(content, true,
                            group, new AbstractContentPosition(currentPosition,
                                    groupPanel.getPosition()),
                            groupPanel);
                    groupContentPanel
                            .add(sequenceInGroup(contentPanel, content));
                    currentPosition += 1;
                }
            }
        };
    }

    /**
     * Method to get the panel with icon and anchor for non text content like
     * Flow, Video, Link
     * 
     * @param content
     * @param isContentFull
     */
    protected FlowPanel getNonTextContentPanel(Content content,
            boolean isContentFull, Group group,
            AbstractContentPosition contentPosition,
            WidgetGroupPanel parentGroupPanel) {
        final ContentFlowPanel contentPanel = new ContentFlowPanel(content,
                group, isContentFull, contentPosition, parentGroupPanel);
        boolean showNewLabel = isNewLabelApplicable(content.flow_id(),
                settings);
        if (showNewLabel) {
            Label newLabel = Common.label(Overlay.CONSTANTS.newLabel(),
                    Common.CSS.newLabel());
            newLabel.addStyleName(Common.CSS.gnewLabel());
            contentPanel.setNewLabel(newLabel);
        }

        contentPanel.getElement().setAttribute(DATA_WFX_ID, this.prefix + "-content");
        FlowPanel iconPanel = new FlowPanel();
        iconPanel.addStyleName(Common.CSS.groupIcon());
        iconPanel.add(getWidgetIcon(contentPanel));
        iconPanel.getElement().setAttribute(DATA_WFX_ID, "content-icon");
        contentPanel.add(iconPanel);
        FlowPanel flowPanelWrapper = new FlowPanel();
        Anchor run = Common.anchor(content.title(), false, "#", true,
                Common.CSS.widgetAnchorElement());
        run.getElement().setAttribute(DATA_WFX_ID, "content-title");
        run.addFocusHandler(getAnchorFocushandler(contentPanel));
        run.addBlurHandler(getAnchorBlurHandler(contentPanel));
        flowPanelWrapper.add(run);
        setId(content, run);
        contentPanel.setAnchor(run);
        flowPanelWrapper.addStyleName(Common.CSS.groupContent());
        run.addClickHandler(getHandler(contentPanel));
        if (StringUtils.isNotBlank(content.name())) {
            Label enterpriseName = new Label(content.name());
            enterpriseName.addStyleName(Common.CSS.enterpriseLabel());
            flowPanelWrapper.add(enterpriseName);
        }
        contentPanel.add(flowPanelWrapper);
        if (showNewLabel) {
            contentPanel.add(contentPanel.getNewLabel());
        }

        Roles.getPresentationRole().set(contentPanel.getElement());
        run.getElement().setAttribute(AriaProperty.LABEL,
                content.type() + "," + content.title());
        contentPanel.addStyleName(Common.CSS.grow());
        return contentPanel;
    }

    protected Anchor getWidgetIcon(ContentFlowPanel contentFlowPanel) {
        Anchor identifierIcon = Common.anchor(null);
        Element iconElement = identifierIcon.getElement();
        String innerHtml = "";
        if (contentFlowPanel.getGroup() == null
                || contentFlowPanel.getContent() != null) {
            Content content = (Content) contentFlowPanel.getContent();
            ContentFlowPanel cfp = new ContentFlowPanel(content, contentFlowPanel.getGroup(),
                    contentFlowPanel.isContentFull(), identifierIcon, contentFlowPanel.getPositionInfo());
            cfp.setNewLabel(contentFlowPanel.getNewLabel());
            identifierIcon.addClickHandler(getHandler(cfp));
            innerHtml = Common.getIconForContentType((content.getType()));
        } else {
            innerHtml = SVGElements.GROUP;
        }
        iconElement.setInnerHTML(innerHtml);
        iconElement.setAttribute(AriaProperty.HIDDEN,
                String.valueOf(true));
        iconElement.setTabIndex(-2);
        identifierIcon.addStyleName(Common.CSS.circle());
        return identifierIcon;
    }

    protected ClickHandler getHandler(ContentFlowPanel contentFlowPanel) {
        ContentType type = contentFlowPanel.getContent().getType();
        if (AppFactory.isNonBrowserApp()
                && ContentType.link == type) {
            return new DesktopAnchorHandler(contentFlowPanel, type);
        }
        else if (ContentType.video == type) {
            return new VideoHandler(contentFlowPanel);
        } else if (ContentType.link == type) {
            return getLinkHandler(contentFlowPanel);
        } else {
            return getFlowHandler(contentFlowPanel);
        }
    }

    protected ClickHandler getFlowHandler(ContentFlowPanel contentFlowPanel) {
        return new FlowHandler(contentFlowPanel);
    }
    
    protected ClickHandler getLinkHandler(ContentFlowPanel contentFlowPanel) {
        return new LinkHandler(contentFlowPanel);
    }

    protected FocusHandler getAnchorFocushandler(
            final com.google.gwt.user.client.ui.Widget wrapperPanel) {
        return event -> wrapperPanel
                .addStyleName(Common.CSS.widgetFlowRowBackground());
    }

    protected BlurHandler getAnchorBlurHandler(
            final com.google.gwt.user.client.ui.Widget wrapperPanel) {
        return event -> wrapperPanel
                .removeStyleName(Common.CSS.widgetFlowRowBackground());
    }

    protected abstract void setId(Content content,
            com.google.gwt.user.client.ui.Widget widget);

    public class VideoHandler implements ClickHandler {
        private Video video;
        private ContentFlowPanel contentFlowPanel;
        private Group group;

        public VideoHandler(ContentFlowPanel contentFlowPanel) {
            this.contentFlowPanel = contentFlowPanel;
            this.video = (Video) contentFlowPanel.getContent();
            this.group = contentFlowPanel.getGroup();
        }

        @Override
        public void onClick(ClickEvent event) {
            GaUtil.analyticsCache.group_id(contentFlowPanel.getGroupId());
            GaUtil.analyticsCache.group_title(contentFlowPanel.getGroupTitle());
            setContextualInfo(contentFlowPanel, video.source());
            updateUnseenInGroup();
            updateNotification(contentFlowPanel);
            if (contentFlowPanel.isContentFull()) {
                handle(video);
                return;
            }
            manager.video((Video) video, new AsyncCallback<Video>() {

                @Override
                public void onFailure(Throwable caught) {
                }

                @Override
                public void onSuccess(Video video) {
                    handle(video);
                }
            });
        }

        private void updateUnseenInGroup() {
            if (group == null) {
                return;
            }
            group.unseenContentCount(group.unseenContentCount() - 1);
        }

        private void handle(Video content) {
            Video videoDetails = DataUtil.create();
            videoDetails.title(video.title());
            videoDetails.flow_id(video.flow_id());
            videoDetails.url(video.url());
            CrossMessager.sendMessageToParent(prefix("_video"),
                    StringUtils.stringifyObject(videoDetails));
            CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                    StringUtils.stringifyObject(Where.create("flow_id",
                            content.flow_id(), "flow_title", content.title(),
                            "event_type", "video_click", "segment_name",
                            GaUtil.analyticsCache.segment_name(), "segment_id",
                            GaUtil.analyticsCache.segment_id(), "src_id",
                            widgetSrc(), "group_id",
                            GaUtil.analyticsCache.group_id(), "group_title",
                            GaUtil.analyticsCache.group_title(),
                            "contextual_info",
                            GaUtil.analyticsCache.contextual_info())));

            Scheduler.get().scheduleDeferred(new ScheduledCommand() {
                @Override
                public void execute() {
                    closeWidget("video/click");
                }
            });
        }
    }

    public class LinkHandler implements ClickHandler {
        private Content content;
        private ContentFlowPanel contentFlowPanel;
        private Group group;
        
        private boolean closeOnClick = true;

        public LinkHandler(ContentFlowPanel contentFlowPanel) {
            this(contentFlowPanel, true);
        }
        
        public LinkHandler(ContentFlowPanel contentFlowPanel, boolean closeOnClick) {
            this.contentFlowPanel = contentFlowPanel;
            this.content = contentFlowPanel.getContent();
            this.closeOnClick = closeOnClick;
            this.group = contentFlowPanel.getGroup(); 
        }

        @Override
        public void onClick(ClickEvent event) {
            if (closeOnClick) {
                closeWidget("link/click");
            }
            updateUnseenInGroup();
            updateNotification(contentFlowPanel);
            String url = content.url();
            if (StringUtils.isNotBlank(url)) {
                handle(url);
            }
            GaUtil.analyticsCache.group_id(contentFlowPanel.getGroupId());
            GaUtil.analyticsCache.group_title(contentFlowPanel.getGroupTitle());

            setContextualInfo(contentFlowPanel, content.source());
            CrossMessager.sendMessageToParent(prefix("_content"),
                    StringUtils.stringifyObject(content));
            CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                    StringUtils.stringifyObject(Where.create("flow_id",
                            content.flow_id(), "flow_title", content.title(),
                            "event_type", "link_click", "segment_name",
                            GaUtil.analyticsCache.segment_name(), "segment_id",
                            GaUtil.analyticsCache.segment_id(), "src_id",
                            widgetSrc(), "group_id",
                            GaUtil.analyticsCache.group_id(), "group_title",
                            GaUtil.analyticsCache.group_title(),
                            "contextual_info",
                            GaUtil.analyticsCache.contextual_info())));
        }

        private void updateUnseenInGroup() {
            if (group == null) {
                return;
            }
            group.unseenContentCount(group.unseenContentCount() - 1);
        }

        private void handle(String url) {
            PlayInvoker.play(url);
        }
    }

    private void updateNotification(ContentFlowPanel contentFlowPanel) {
        if (!Enterpriser.hasFeature(AdditionalFeatures.NEW_NOTIFICATION)
                || !isNewLabelApplicable(contentFlowPanel.getFlowId(),
                        settings))
            return;
        updateUnseenContent(contentFlowPanel.getFlowId());

        if (contentFlowPanel.getNewLabel() != null) {
            contentFlowPanel.getNewLabel().removeFromParent();
        }
    }

    // playing url on desktop will be handle by native code
    public class DesktopAnchorHandler implements ClickHandler {

        private ContentType type;
        private ContentFlowPanel contentFlowPanel;

        public DesktopAnchorHandler(ContentFlowPanel contentFlowPanel,
                ContentType type) {
            this.type = type;
            this.contentFlowPanel = contentFlowPanel;
        }

        @Override
        public void onClick(ClickEvent event) {
            Content content = contentFlowPanel.getContent();
            Content desktopMessageContent = DataUtil.create();
            desktopMessageContent.title(content.title());
            desktopMessageContent.flow_id(content.flow_id());
            desktopMessageContent.url(content.url());
            desktopMessageContent.type(type.toString());
            GaUtil.analyticsCache.group_id(contentFlowPanel.getGroupId());
            GaUtil.analyticsCache.group_title(contentFlowPanel.getGroupTitle());
            CrossMessager.sendMessageToParent(prefix("_desktop_content"),
                    StringUtils.stringifyObject(desktopMessageContent));
            CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                    StringUtils.stringifyObject(Where.create("flow_id",
                            content.flow_id(), "flow_title", content.title(),
                            "event_type", type.toString() + "_click",
                            "segment_name",
                            GaUtil.analyticsCache.segment_name(), "segment_id",
                            GaUtil.analyticsCache.segment_id(), "src_id",
                            widgetSrc(), "group_id",
                            GaUtil.analyticsCache.group_id(), "group_title",
                            GaUtil.analyticsCache.group_title(),
                            "contextual_info",
                            GaUtil.analyticsCache.contextual_info())));
            closeWidget(type.toString() + "/click");
        }
    }

    public static class AbstractContentPosition {
        private Integer contentPosition;
        private Integer groupPosition;

        public AbstractContentPosition(Integer contentPosition,
                Integer groupPosition) {
            this.contentPosition = contentPosition;
            this.groupPosition = groupPosition;
        }

        public AbstractContentPosition(Integer position) {
            this.contentPosition = position;
            this.groupPosition = position;
        }

        public Integer getContentPosition() {
            return contentPosition;
        }

        public Integer getGroupPosition() {
            return groupPosition;
        }

        public void setAsGroup() {
            groupPosition = contentPosition;
        }
    }

    protected boolean isNewLabelApplicable(String contentId, Settings settings) {
        boolean isNewNotificationEnabled = Enterpriser
                .hasFeature(AdditionalFeatures.NEW_NOTIFICATION);
        if (isNewNotificationEnabled && settings != null
                && settings.unseenContent() != null) {
            for (int i = 0; i < settings.unseenContent().length(); i++) {
                if (contentId.equals(settings.unseenContent().get(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isPersonalisationEnabled() {
    	if (Enterpriser.hasFeature(AdditionalFeatures.personalisation_content_reorder)) {
    		return Enterpriser.enterprise().personalisationConfig()
				.valueAsObjExt("config")
				.valueAsBoolean(PersonalisationAttributes.ENABLED.getKey());
    	}
    	return false;
    }
    
}
