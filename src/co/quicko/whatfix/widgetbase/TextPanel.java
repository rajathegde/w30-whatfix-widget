package co.quicko.whatfix.widgetbase;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.aria.client.Roles;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.HyperlinkAnalytics;
import co.quicko.whatfix.common.LinkedFlowHandler;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.AbstractContent;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.Content.Text;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.Group;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.extension.util.Runner;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.widgetbase.WidgetBase.AbstractContentPosition;

public class TextPanel extends ExpandablePanel {
    protected AbstractContent content;
    private String description;
    protected Header header;
    protected ContentFlowPanel contentFlowPanel;
    protected AbstractContentPosition contentPosition;
    boolean showNewLabel;
    private Group group;

    private ScrollPanel scroller;

    private final String PREFIX = "widget";

    public TextPanel(AbstractContent content, ScrollPanel scroller) {
        this.content = content;
        this.description = setDescription();
        this.scroller = scroller;
        setupPanel();
    }

    public TextPanel(ContentFlowPanel contentFlowPanel) {
        this(contentFlowPanel, null, false);
        this.group = contentFlowPanel.getGroup();
    }
    
    public TextPanel(ContentFlowPanel contentFlowPanel, ScrollPanel scroller, boolean  showNewLabel) {
        this.content = contentFlowPanel.getContent();
        this.description = setDescription();
        this.contentFlowPanel = contentFlowPanel;
        this.contentPosition = contentFlowPanel.getPositionInfo();
        this.scroller = scroller;
        this.showNewLabel = showNewLabel;
        this.group = contentFlowPanel.getGroup();
        setupPanel();
        Roles.getPresentationRole().set(getElement());
    }

    public String setDescription() {
        return injectBlankTarget(((Text) this.content).description_md());
    }

    @Override
    protected Widget getHeader() {
        if (header == null) {
            header = new Header(getContentTitle(), 4);
            addExpandableIconVisibilityRule();
        }
        header.getElement().setAttribute("data-wfx-id", "text-header");
        setHeaderIcon();

        if (showNewLabel) {
            Label newLabel = Common.label(Overlay.CONSTANTS.newLabel(),
                    Common.CSS.newLabel());
            header.setWidget(0, 3, newLabel);
        }

        Roles.getPresentationRole().set(header.getRowFormatter().getElement(0));
        return header;
    }

    protected void addExpandableIconVisibilityRule() {
       header.addStyleName(Common.CSS.dropDownIconVisibility());
    }

    public String getContentTitle() {
        return content.title();
    }

    protected void setHeaderIcon() {
        header.setIcon(getHeaderIcon());
    }
    
    @Override
    protected Widget getContent() {
        Widget contentPanel = new HTML(this.description);
        Roles.getPresentationRole().set(contentPanel.getElement());
        contentPanel.getElement().setAttribute("data-wfx-id", "text-content");
        contentPanel.setStyleName(Common.CSS.contentTextDescription());

        LinkedFlowHandler.handleSpecials(contentPanel, widgetSrc(),
                new AsyncCallback<Flow>() {

                    @Override
                    public void onSuccess(Flow flow) {
                        Where where = Where.create("closeBy", "link_click",
                                Common.SEGMENT_NAME,
                                GaUtil.analyticsCache.segment_name(),
                                Common.SEGMENT_ID,
                                GaUtil.analyticsCache.segment_id(),
                                Common.SRC_ID, widgetSrc());
                        CrossMessager.sendMessage(mainWindow(),
                                prefix("_close"),
                                StringUtils.stringifyObject(where));
                        String message = Runner.stateForEmbed(flow);
                        CrossMessager.sendMessage(mainWindow(), "embed_run_secure",
                                message);
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                    }
                });

        HyperlinkAnalytics.addNativeLinkListener(contentPanel.getElement(),
                "text_panel", getTrackingInfo());

        return contentPanel;
    }

    private static native JavaScriptObject mainWindow() /*-{
		return $wnd.parent;
    }-*/;
    
    private Map<String, Object> getTrackingInfo() {
        Map<String, Object> info = new HashMap<String, Object>();
        info.put("flow_id", ((Content) content).flow_id());
        info.put("flow_title", ((Content) content).title());
        info.put("segment_name", GaUtil.analyticsCache.segment_name());
        info.put("segment_id", GaUtil.analyticsCache.segment_id());
        info.put("group_id", contentFlowPanel.getGroupId());
        info.put("group_title", contentFlowPanel.getGroupTitle());
        info.put("src_id", widgetSrc());
        return info;
    }

    /**
     * Add blank target to html so that all 
     * links will be opened in new tab
     * @param html
     * @return
     */
    private String injectBlankTarget(String html) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<base target=\"_blank\">");
        buffer.append(html);
        return buffer.toString();
    }

    @Override
    protected void handleOpen() {
        if (showNewLabel) {
            showNewLabel = !showNewLabel;
            header.remove(header.getWidget(0, 3));
            header.remove(header.getWidget(0, 2));
            updateUnseenInGroup();
            CrossMessager.sendMessageToParent("updateUnseenContent",
                    ((Content) content).flow_id());
        }
        header.toggleIcon();
        header.getParent().getElement().setAttribute(AriaProperty.LABEL,
                Common.CONSTANTS.expanded() + "," + content.title());
        trackOpen();
    }

    private void updateUnseenInGroup() {
        if (group == null) {
            return;
        }
        int noOfUnseenInGroup = group.unseenContentCount();
        if (--noOfUnseenInGroup == 0
                && contentFlowPanel.getParentGroupPanel() != null) {
            contentFlowPanel.getParentGroupPanel().removeNewLabel();
        }
        group.unseenContentCount(group.unseenContentCount() - 1);
    }

    @Override
    public void handlePostOpen() {
        if (scroller != null) {
            Common.alignIntoView(scroller.getElement(), getElement());
        } else {
            Common.scrollIntoView(getElement(), true);
        }
    }

    @Override
    protected void handleClose() {
        header.toggleIcon();
        header.getParent().getElement().setAttribute(AriaProperty.LABEL,
                Common.CONSTANTS.collapsed() + "," + content.title());
        trackClose();
        getParent().removeStyleName(
                Common.CSS.widgetFlowRowBackground());
    }

    protected void trackOpen() {
        CrossMessager.sendMessageToParent(prefix("_content"),
                StringUtils.stringifyObject(content));
        CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                StringUtils.stringifyObject(Where.create("flow_id",
                        ((Content) content).flow_id(), "flow_title",
                        ((Content) content).title(), "event_type",
                        GaUtil.PayloadTypes.text_open.toString(),
                        "segment_name", GaUtil.analyticsCache.segment_name(),
                        "segment_id", GaUtil.analyticsCache.segment_id(),
                        "src_id", widgetSrc(), "group_id",
                        contentFlowPanel.getGroupId(), "group_title",
                        contentFlowPanel.getGroupTitle(), "contextual_info",
                        GaUtil.analyticsCache.contextual_info())));
    }

    protected void trackClose() {
        CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                StringUtils.stringifyObject(Where.create("flow_id",
                        ((Content) content).flow_id(), "flow_title",
                        ((Content) content).title(), "event_type",
                        GaUtil.PayloadTypes.text_close.toString(),
                        "segment_name", GaUtil.analyticsCache.segment_name(),
                        "segment_id", GaUtil.analyticsCache.segment_id(),
                        "src_id", widgetSrc(), "group_id",
                        contentFlowPanel.getGroupId(), "group_title",
                        contentFlowPanel.getGroupTitle())));
    }

    protected Integer getPosition() {
        return contentPosition.getContentPosition();
    }

    protected String prefix(String type) {
        return PREFIX + type;
    }
    
    protected String widgetSrc() {
        return TrackerEventOrigin.SELF_HELP.getSrcName();
    }
    
    protected Widget getHeaderIcon() {
        return textIcon();
    }
    
    protected Widget textIcon() {
        FlowPanel iconPanel = new FlowPanel();
        Anchor icon = Common.anchor(null);
        icon.getElement().setInnerHTML(getIconStyle());
        icon.addStyleName(Common.CSS.circle());
        iconPanel.add(icon);
        icon.setTabIndex(-2);
        icon.getElement().setAttribute(AriaProperty.HIDDEN,
                String.valueOf(true));
        return iconPanel;
    }
    
    protected String getIconStyle() {
        return SVGElements.TEXT_ICON;
    }
    
    protected class Header extends Grid {

        private static final int ROW_SIZE = 1;
        private static final int COL_SIZE = 4;
        
        private boolean isOpen;

        public Header(String header, int columnSize) {
            super(ROW_SIZE, columnSize);
            Widget headerText = getHeaderTextWidget(header);
            setWidget(0, 1, headerText);
            getWidget(0, 1).getElement().setAttribute("data-wfx-id", "expandable-content");
            if(showNewLabel) {
            	setWidget(0, 2, addIcon());
            }else {
            	setWidget(0, 3, addIcon());
            }
            getRowFormatter().getElement(0).setAttribute(AriaProperty.HIDDEN,
                    String.valueOf(true));
            Roles.getPresentationRole().set(getRowFormatter().getElement(0));
            setCellPadding(0);
            setCellSpacing(0);
            getWidget(0, 1).getElement().setTabIndex(-2);
            getWidget(0, 1).getElement().setAttribute(AriaProperty.HIDDEN,
                    String.valueOf(true));
            getElement().setAttribute(AriaProperty.HIDDEN,
                    String.valueOf(true));
            Roles.getPresentationRole().set(getElement());
            addStyleName(Common.CSS.expandablePanelHeader());
        }

		public Header(String header) {
			this(header, COL_SIZE);
		}
        
        protected Widget getHeaderTextWidget(String header) {
            String enterpriseName = (((Content) content).name());
            if (StringUtils.isNotBlank(enterpriseName)) {
                FlowPanel headerWrapper = new FlowPanel();
                headerWrapper.add(
                        Common.label(header, Common.CSS.widgetAnchorElement()));
                Label enterpriseLabel = new Label(enterpriseName);
                enterpriseLabel.addStyleName(Common.CSS.enterpriseLabel());
                headerWrapper.add(enterpriseLabel);
                headerWrapper.addStyleName(Common.CSS.removeOutlineOnFocus());
                return headerWrapper;
            } else {
                return Common.label(header, Common.CSS.widgetAnchorElement());
            }
        }
        
        public void setIcon(Widget icon) {
            icon.getElement().setAttribute("data-wfx-id", "content-icon");
            setWidget(0, 0, icon);
        }
        
        public void toggleIcon() {
            this.isOpen = !this.isOpen;
            if (showNewLabel) {
                setWidget(0, 2, addIcon());
            } else {
                setWidget(0, 3, addIcon());
            }
            if (isOpen) {
                addStyleName(headerOpenStyle());
            } else {
                removeStyleName(headerOpenStyle());
            }
        }
        
        private String headerOpenStyle() {
            return Common.CSS.expandablePanelHeaderOpen();
        }

        protected final Anchor addIcon() {
            Anchor identifierIcon = Common.anchor(null);
            String iconStyle = this.isOpen ? SVGElements.ANGLE_UP
                    : SVGElements.ANGLE_DOWN;
            identifierIcon.getElement().setInnerHTML(iconStyle);
            identifierIcon.addStyleName(Common.CSS.expandablePanelIcon());
            identifierIcon.setTabIndex(-2);
            identifierIcon.getElement().setAttribute(AriaProperty.HIDDEN,
                    String.valueOf(true));
            return identifierIcon;
        }
    }
}
