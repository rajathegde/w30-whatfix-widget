
/**
    A separate js file was added to attach listener before the install event was getting triggered.
    In the previous version, the listener was getting attached and callback received only after onModuleLoad()
    was getting invoked, by which time the install event had already been triggered.
    Adding the listener through another file ensures we have the listener added before this event is triggered.
**/


function extension_analytics() {
	browser.runtime.onInstalled.addListener(function(value) {
        installEvent(value);
    });
}

function installEvent(details) {
  window.installEventCall(details);
}

extension_analytics();
