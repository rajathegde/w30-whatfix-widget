package co.quicko.whatfix.blog;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLTable.CellFormatter;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.blogbase.BlogBasePopup;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Decider;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.snap.SmallStepSnap;
import co.quicko.whatfix.common.snap.StepSnap;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.extension.util.Extension;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.security.Enterpriser;

/**
 * @Description: This class renders the flow in a portrait format which would be
 *               converted to the PDF using the puppeteer. The layout of the
 *               webpage is the first page contains flow title and flow
 *               description and then the flow steps, followed by the foot note.
 *               The height and width of the render page is taken as 1120 and
 *               794 (A4 portrait size)
 * @author deepak
 *
 */
public class PortraitPDFBlog extends PdfBlog {
    // Class constants
    private static final String CONST_PX = "px";
    // Number of steps in the first page
    private static final int FIRST_PAGE_STEP_COUNT = 3;
    // Height of the separator between the steps including margin
    private static final int HEIGHT_OF_HZ_SEPARATOR = 23;
    // Overall height of the step including the margin
    private static final int HEIGHT_OF_STEP = 268;
    // Starting step number for second page
    private static final int FLOW_STEP_SECOND_PAGE = 4;
    // Step grid constants
    private static final int STEP_GRID_COLUMN_COUNT = 2;
    private static final int STEP_GRID_ROW_COUNT = 1;
    // Step inner grid constants
    private static final int STEP_INNER_GRID_COLUMN_COUNT = 2;
    private static final int STEP_INNER_GRID_ROW_COUNT = 2;

    // Step image dimensions
    private static final int STEP_SNAP_HEIGHT = 240;
    private static final int STEP_SNAP_WIDTH = 360;
    // Number of steps in a page
    private static final int STEPS_IN_A_PAGE = 3;
    // Margin for header and footer section
    private static final int HEADER_OFFSET_MARGIN = 48;
    private static final int FOOTER_OFFSET_MARGIN = 33;
    // Non-static member variables
    private int portraitFirstPageHeight = 1125;
    private int portraitPageHeight = 1120;
    // Header and footer enable status
    private boolean bHeader = false;
    private boolean bFooter = false;

    // Puppeteer has a bug with chromium browser that when we have
    // header/footer, it appends a blank page at end. hence for last page brand
    // will be added 5px above
    private int lastPageOffset = 5;

    /**
     * @Description: Constructor
     * @param flow
     * @param stepWidth
     * @param stepHeight
     * @param versionNum
     * @param bHeader
     * @param bFooter
     * @param cb
     */
    public PortraitPDFBlog(String flow, int stepWidth, int stepHeight,
            long versionNum, AsyncCallback<PdfBlog> cb) {
        super(flow, stepWidth, stepHeight, versionNum, cb);
    }

    /**
     * Initialize the member variables depending on the Enterprise theme
     */
    private void initVariables() {
        String header = Themer.value(Themer.Pdf.SHOW_PDF_HEADER);
        String footer = Themer.value(Themer.Pdf.SHOW_PDF_FOOTER);

        this.bHeader = StringUtils.isNotBlank(header)
                && header.equalsIgnoreCase("show");
        this.bFooter = StringUtils.isNotBlank(footer)
                && footer.equalsIgnoreCase("show");

        // Adjust the page height based on the status of header and footer
        portraitPageHeight = getPageHeight(portraitPageHeight);
        portraitFirstPageHeight = getPageHeight(portraitFirstPageHeight);

        // when there is a header or footer, the last page branding div is
        // overflowing to an empty page, addition 5 px is adjusted
        if (this.bHeader || this.bFooter) {
            lastPageOffset = 2 * lastPageOffset;
        }
    }

    /**
     * Description: override method to render the webpage, the layout is 1.Flow
     * panel : Contains Flow title and Flow description 2. Steps of the flow.
     * First page contains 2 steps and remaining pages contains 3 pages
     */
    @Override
    protected void initialize(Flow flow, int stepWidth, int stepHeight) {
        this.flow = flow;
        if (null == flow) {
            // if flow is null, return from here
            return;
        }

        // Init properties
        JavaScriptObject properties = Enterpriser
                .properties(flow.locale());
        Extension.CONSTANTS.use(properties);
        Common.CONSTANTS.use(properties);

        // Initialize the variables
        initVariables();

        // Steps in the flow
        int steps = flow.steps();
        int pageBreakHeight = portraitPageHeight;
        
        // Construct the first page, as first page contains flow description and
        // flow title, handling of first page is separate from other pages
        boolean footNoteAdded = addFirstPage(flow);
        
        // Commented code for FlowPanel footNotePanel = getFootNotePanel(flow)

        // Special handling for first page, as first page will have the
        // flowInformationpanel as well
        int stepCountInPage = 0;
        // Flow steps pages, Step 3 onwards
        for (int step = FLOW_STEP_SECOND_PAGE; step <= steps; step++) {
            ++stepCountInPage;
            // Grid for each step
            Grid grid = getStepGrid(flow, step);
            // Add the grid to the page
            add(grid);

            // Recomputing the page break height,
            pageBreakHeight = pageBreakHeight - HEIGHT_OF_STEP;
            // Checking whether the number of steps in a page reached max limit
            if (stepCountInPage >= STEPS_IN_A_PAGE) {
                // add the page break, and footnote if its last step
                footNoteAdded = addPageBreakDiv(steps == step,
                        pageBreakHeight, getFootNotePanel(flow));
                // reseting counters
                stepCountInPage = 0;
                pageBreakHeight = portraitPageHeight;
            }
            else {
                // if still steps needs to be added, then add horizontal row
                if (step != steps) {
                    InlineHTML hrTag = InlineHTML
                            .wrap(Document.get().createHRElement());
                    hrTag.setStyleName(BlogBasePopup.CSS.horizontalSeparator());

                    pageBreakHeight = pageBreakHeight - HEIGHT_OF_HZ_SEPARATOR;
                    add(hrTag);
                }
            }
        }

        // add foot note if not added earlier
        if (!footNoteAdded) {
            addPageBreakDiv(true, pageBreakHeight, getFootNotePanel(flow));
        }
    }

    /**
     * @Description: Returns the footnote panel
     * @param flow
     * @return
     */
    private FlowPanel getFootNotePanel(Flow flow) {
        // Add the foot note panel at the end, this would be at the last
        // page of the PDF
        FlowPanel footNotePanel = new FlowPanel();
        if (StringUtils.isNotBlank(flow.footnote_md())) {
            footNotePanel.add(Common.html(flow.footnote_md(),
                    BlogBasePopup.CSS.portraitFootnote()));
        } else {
            footNotePanel.add(Common.label(Common.CONSTANTS.endDefaultMessage(),
                    BlogBasePopup.CSS.portraitFootnote()));
        }
        return footNotePanel;
    }

    /**
     * @Description This method is to add the div at the end of each page. Log
     *              is page height(852)-(no of steps*step height)-(header
     *              height)-(footer height)
     * @param steps
     * @param pageBreakHeight
     * @param step
     * @return
     */
    private boolean addPageBreakDiv(boolean isLaststep, int pageBreakHeight,
            FlowPanel footNotePanel) {
        boolean footNodeAdded = false;
        // add a dummy div for pagebreak
        FlowPanel pageBreak = new FlowPanel();
        pageBreak.setStyleName(BlogBasePopup.CSS.portraitPageBreak());

        pageBreak.setHeight(pageBreakHeight + CONST_PX);

        if (isLaststep && pageBreakHeight >= footNotePanel.getOffsetHeight()) {
            footNodeAdded = true;
            pageBreak.setHeight((pageBreakHeight - lastPageOffset) + CONST_PX);
            pageBreak.add(footNotePanel);
        }
        if (Decider.IMPL.showLogo()) {
            FlowPanel branding = new FlowPanel();
            branding.getElement().setInnerHTML(SVGElements.WHATFIX_BRANDING);
            branding.setStyleName(BlogBasePopup.CSS.pdfWhatfixBrand());
            pageBreak.add(branding);
        }
        add(pageBreak);
        if (footNotePanel.getOffsetHeight() > pageBreakHeight
                && footNodeAdded) {
            pageBreak.remove(footNotePanel);
            footNodeAdded = false;
            pageBreak.setHeight(pageBreakHeight + CONST_PX);
        }
        return footNodeAdded;
    }

    /**
     * @Description: Add the first page break of the PDF, return boolean to
     *               mention whether footnote is added or not
     * @param pdfFirstPage
     * @param isLaststep
     * @return
     */
    private boolean addFirstPageBreakDiv(FlowPanel pdfFirstPage,
            boolean isLaststep, int totalSteps) {
        boolean footNodeAdded = false;

        // Add footnote if its last step
        FlowPanel footPanel = getFootNotePanel(flow);
        if (isLaststep) {
            footNodeAdded = true;
            pdfFirstPage.add(footPanel);
            pdfFirstPage.setHeight(
                    (portraitFirstPageHeight - lastPageOffset)
                            + CONST_PX);
        }

        // Add branding logo is its not opted out
        if (Decider.IMPL.showLogo()) {
            FlowPanel branding = new FlowPanel();
            branding.getElement().setInnerHTML(SVGElements.WHATFIX_BRANDING);
            branding.setStyleName(BlogBasePopup.CSS.pdfWhatfixBrand());
            pdfFirstPage.add(branding);
        }
        
        // Add the first page to dom
        add(pdfFirstPage);

        // if the size with footnote exceed the portrait height, then remove the
        // footnote and add in next page. If the scroll height is more than
        // first page height and total steps more than 2 then remove the footer
        // and add on next page
        if (pdfFirstPage.getElement()
                .getScrollHeight() > portraitFirstPageHeight && footNodeAdded
                && totalSteps > 2) {
            pdfFirstPage.remove(footPanel);
            footNodeAdded = false;
            pdfFirstPage.setHeight(portraitFirstPageHeight + CONST_PX);
        }

        return footNodeAdded;
    }


    /**
     * @Description: Add the first page to the PDF first
     * @param flow
     * @return
     */
    private boolean addFirstPage(Flow flow) {
        // Creating a 842x595 page for first page
        FlowPanel pdfFirstPage = new FlowPanel();
        pdfFirstPage.getElement().setId("pdffirstpage");
        pdfFirstPage.addStyleName(BlogBasePopup.CSS.portraitFirstPage());
        pdfFirstPage.setHeight(portraitFirstPageHeight + CONST_PX);

        // Add flow Information panel
        pdfFirstPage.add(getFlowPanel(flow));

        // Add a dummy div with height of 10 px and width 100%
        FlowPanel dummyDiv = new FlowPanel();
        dummyDiv.addStyleName(BlogBasePopup.CSS.dummyMargin());
        pdfFirstPage.add(dummyDiv);

        int steps = flow.steps();

        boolean footNodeAdded = false;

        // Flow steps pages
        for (int step = 1; step <= FIRST_PAGE_STEP_COUNT; step++) {
            Grid grid = getStepGrid(flow, step);

            pdfFirstPage.add(grid);

            // Checking whether the number of steps in a page reached max limit
            if (step >= FIRST_PAGE_STEP_COUNT || step >= steps) {
                // its last step
                footNodeAdded = addFirstPageBreakDiv(pdfFirstPage,
                        steps == step, steps);
                break;
            }
            else {
                InlineHTML hrTag = InlineHTML
                        .wrap(Document.get().createHRElement());
                hrTag.setStyleName(BlogBasePopup.CSS.horizontalSeparator());

                pdfFirstPage.add(hrTag);
            }
        }
        return footNodeAdded;
    }

    /**
     * @Description: Gets the grid for each step
     * @param flow
     * @param step
     * @return
     */
    private Grid getStepGrid(Flow flow, int step) {
        Grid grid = new Grid(STEP_GRID_ROW_COUNT, STEP_GRID_COLUMN_COUNT);
        grid.setStyleName(BlogBasePopup.CSS.gridStyle());
        CellFormatter formatter = grid.getCellFormatter();

        StepSnap snap = new SmallStepSnap(false, false, true);
        boolean isHistory = flow.step_image_creation_time_isHistory(step);
        if (isHistory) {
            snap.setValueWithoutPopover(flow.step(step), isHistory);
        } else {
            snap.setValueWithoutPopover(flow.step(step));
        }

        snap.refreshGoodCrop(STEP_SNAP_WIDTH, STEP_SNAP_HEIGHT);
        snap.addStyleName(BlogBasePopup.CSS.pdfStepSnapPortrait());

        // Add step snap to first row and first column
        grid.setWidget(0, 0, snap);
        // Add Step info to first row and second column
        grid.setWidget(0, 1, innerGrid(flow, step));
        grid.getColumnFormatter().setWidth(1, "100%");

        // Setting the vertical alignment to top for both
        // the columns
        formatter.setVerticalAlignment(0, 0, HasVerticalAlignment.ALIGN_TOP);
        formatter.setVerticalAlignment(0, 1, HasVerticalAlignment.ALIGN_TOP);
        return grid;
    }

    /**
     * @Description: Get's the innerGrid for flow title and description
     * @param flow
     * @param step
     * @return
     */
    private Grid innerGrid(Flow flow, int step) {

        boolean isRTL = !Common.isLTR(flow);

        Grid grid = new Grid(STEP_INNER_GRID_ROW_COUNT,
                STEP_INNER_GRID_COLUMN_COUNT);

        CellFormatter formatter = grid.getCellFormatter();
        Step stepInfo = flow.step(step);

        // Prepare the Step information
        // Step number
        FlowPanel setpNumberFlow = new FlowPanel();
        Widget stepNumber = Common.html(step + ". ");

        // Step title
        Widget stepTitle = Common.html(stepInfo.description_md());

        // flow for step title and description
        FlowPanel setpTitleFlow = new FlowPanel();
        FlowPanel setpDescFlow = new FlowPanel();
        Widget stepDesc = Common.html(stepInfo.note_md(),
                BlogBasePopup.CSS.pdfStepDesc());

        setpNumberFlow.add(stepNumber);
        setpTitleFlow.add(stepTitle);
        setpDescFlow.add(stepDesc);

        // special case for RTL languages because of table has to align
        // differently
        // for RTL stepnumber and title comes from right side and for non rtl it
        // will come from left
        if (isRTL) {

            grid.setStyleName(BlogBasePopup.CSS.innerGridStyleRTL());
            stepTitle.addStyleName(BlogBasePopup.CSS.pdfStepTitleRTL());
            stepTitle.addStyleName(Overlay.CSS.rtl());
            stepDesc.addStyleName(Overlay.CSS.rtl());
            stepNumber.addStyleName(Overlay.CSS.rtl());

            grid.setWidget(0, 1, setpNumberFlow);
            grid.setWidget(0, 0, setpTitleFlow);
            grid.setWidget(1, 0, stepDesc);

            grid.getColumnFormatter().setWidth(0, "100%");
            formatter.setHorizontalAlignment(0, 1,
                    HasHorizontalAlignment.ALIGN_RIGHT);
            formatter.setVerticalAlignment(0, 1, HasVerticalAlignment.ALIGN_MIDDLE);
            formatter.setVerticalAlignment(0, 0, HasVerticalAlignment.ALIGN_TOP);

        } else {
            grid.setStyleName(BlogBasePopup.CSS.innerGridStyle());
            stepTitle.addStyleName(BlogBasePopup.CSS.pdfStepTitle());
            setpNumberFlow.addStyleName(BlogBasePopup.CSS.pdfStepInfo());
            grid.setWidget(0, 0, setpNumberFlow);
            grid.setWidget(0, 1, setpTitleFlow);
            grid.setWidget(1, 1, stepDesc);
            grid.getColumnFormatter().setWidth(1, "100%");
            formatter.setVerticalAlignment(0, 0, HasVerticalAlignment.ALIGN_MIDDLE);
            formatter.setVerticalAlignment(0, 1, HasVerticalAlignment.ALIGN_TOP);
        }

        
        formatter.setVerticalAlignment(1, 1, HasVerticalAlignment.ALIGN_TOP);
        return grid;

    }
    /**
     * @Description Gets the flow panel for the PDF, contains flow title and
     *              flow description
     * @param flow
     * @return
     */
    private FlowPanel getFlowPanel(Flow flow) {
        // Flow panel
        FlowPanel flowInfoPanel = new FlowPanel();
        flowInfoPanel.getElement().setId("flowpanel");
        flowInfoPanel.addStyleName(BlogBasePopup.CSS.flowContainer());

        // Add flow title to the flowpanel
        Widget title = Common.html(flow.title(),
                BlogBasePopup.CSS.pdfFlowTitle());
        flowInfoPanel.add(title);

        // add the horizontal separator
        InlineHTML flowHrTag = InlineHTML
                .wrap(Document.get().createHRElement());
        flowHrTag.setStyleName(BlogBasePopup.CSS.flowHzSeparator());
        flowInfoPanel.add(flowHrTag);

        // add flow description
        Widget description = Common.html(flow.description_md(),
                BlogBasePopup.CSS.pdfFlowDesc());
        description.setStyleName(BlogBasePopup.CSS.pdfFlowDesc());
        flowInfoPanel.add(description);

        return flowInfoPanel;
    }

    /**
     * @Description: Based on the header or footer enable flag, adjust the page
     *               height
     * @param defaultHeight
     * @return
     */
    private int getPageHeight(final int defaultHeight) {
        int pageHeight = defaultHeight;
        if (bHeader) {
            // If header is enabled, reduce the page height to accommodate
            // header
            pageHeight = pageHeight - HEADER_OFFSET_MARGIN;
            // Adding the check when header is there and footer not there, the
            // branding logo is overflowing
            if (!bFooter) {
                pageHeight = pageHeight - 1;
            }
        }
        if (bFooter) {
            // If footer is enabled, reduce the page height to accommodate
            // footer
            pageHeight = pageHeight - FOOTER_OFFSET_MARGIN;
        }
        return pageHeight;
    }
}