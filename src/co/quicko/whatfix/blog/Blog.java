package co.quicko.whatfix.blog;

import co.quicko.whatfix.common.Common;

import com.google.gwt.core.client.GWT;

public class Blog {
    public static final BlogBundle BUNDLE = GWT.create(BlogBundle.class);
    public static final BlogCss CSS = BUNDLE.css();

    static {
        Common.CSS.ensureInjected();
        CSS.ensureInjected();
    }

}
