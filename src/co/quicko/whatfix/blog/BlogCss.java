package co.quicko.whatfix.blog;

import com.google.gwt.resources.client.CssResource;

public interface BlogCss extends CssResource {

    public int stepWidth();

    public int stepHeight();

    public int microStepWidth();

    public int microStepHeight();

    public int blogPlusWidth();

    public String blog();

    public String title();

    public String snap();

    public String snaps();

    public String authorWrap();

    public String poweredBy();

    public String brandLogo();

    public String footerStyle();

    public String footerPanel();
    
    public String blogFootnote();
}
