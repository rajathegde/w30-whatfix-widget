package co.quicko.whatfix.blog;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.blogbase.BlogBasePopup;
import co.quicko.whatfix.blogbase.BlogBasePopupPanel;
import co.quicko.whatfix.blogbase.BlogBasePopupPanel.EndFlowPopup;
import co.quicko.whatfix.blogbase.BlogBasePopupPanel.StartFlowPopup;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.snap.ScaledStepSnap;
import co.quicko.whatfix.common.snap.StepSnap;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.FlowAndEnt;
import co.quicko.whatfix.extension.util.Extension;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.FlowService;

public class PdfBlog extends FlowPanel {
    protected Flow flow;

    public PdfBlog(final String flow, final int stepWidth, final int stepHeight, long versionNum,
            final AsyncCallback<PdfBlog> cb) {
        final Label loading = new Label("loading");
        add(loading);
        FlowService.IMPL.flowAndEnt(flow, new AsyncCallback<FlowAndEnt>() {
            @Override
            public void onSuccess(FlowAndEnt result) {
                clear();
                Common.updateVimeoURLs(result, new AsyncCallback<Flow>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        loading.setText("Content unavailable");
                        cb.onFailure(caught);                        
                    }

                    @Override
                    public void onSuccess(Flow updatedFlow) {
                        Enterpriser.initialize(result.enterprise());
                        initialize(updatedFlow, stepWidth, stepHeight);
                        cb.onSuccess(PdfBlog.this);
                    }
                });
            }

            @Override
            public void onFailure(Throwable caught) {
                loading.setText("Content unavailable");
                cb.onFailure(caught);
            }
        }, versionNum);
    }

    protected void initialize(Flow flow, int stepWidth, int stepHeight) {
        this.flow = flow;
        if (flow == null) {
            return;
        }
        Common.initFlowLevelTheme(flow.flow_theme());
        JavaScriptObject properties = Enterpriser
                .properties(flow.locale());
        Extension.CONSTANTS.use(properties);
        Common.CONSTANTS.use(properties);

        int steps = flow.steps();
        boolean isRTL = !Common.isLTR(flow);

        FlowPanel grid = new FlowPanel();
        grid.addStyleName(BlogBasePopup.CSS.page());
        
        // Flow title page
        FlowPanel firstPageContainer = new FlowPanel();
        firstPageContainer.addStyleName(BlogBasePopup.CSS.pageContainer());
        firstPageContainer.add(getPage(flow, stepWidth, stepHeight, 1));

        StartFlowPopup startFlowPopup = new StartFlowPopup(flow, isRTL);
        startFlowPopup.addStyleName(BlogBasePopup.CSS.popup());
        firstPageContainer.add(startFlowPopup);

        BlogBasePopupPanel.addBrandingPanel(firstPageContainer);

        grid.add(firstPageContainer);
        
        // Flow steps pages
        for (int step = 1; step <= steps; step++) {
            StepSnap snap = new ScaledStepSnap(false, true, false);
            boolean isHistory = flow.step_image_creation_time_isHistory(step);
            if (isHistory) {
                snap.setValue(flow.step(step),
                        isHistory);
            } else {
                snap.setValue(flow.step(step));
            }
            snap.setPixelSize(stepWidth, stepHeight);
            if (isRTL) {
                snap.alignPopoverRTL();
            }
            snap.refresh(flow.title(), step, steps);
            snap.addStyleName(BlogBasePopup.CSS.pdfSnaps());
            grid.add(snap);
        }

        // Footnote page
        FlowPanel lastPageContainer = new FlowPanel();
        lastPageContainer.addStyleName(BlogBasePopup.CSS.pageContainer());
        lastPageContainer.add(getPage(flow, stepWidth, stepHeight, steps));

        EndFlowPopup endFlowPopup = new EndFlowPopup(flow);
        endFlowPopup.addStyleName(BlogBasePopup.CSS.popup());
        lastPageContainer.add(endFlowPopup);

        BlogBasePopupPanel.addBrandingPanel(lastPageContainer);

        grid.add(lastPageContainer);

        setWidth(Integer.toString(stepWidth) + "px");
        add(grid);
    }

    public Flow getFlow() {
        return flow;
    }

    public static Widget getPage(Flow flow, int stepWidth, int stepHeight,
            int step) {
        StepSnap snap = new ScaledStepSnap(true, false, false);
        snap.setValueWithoutPopover(flow.step(step),
                flow.step_image_creation_time_isHistory(step));
        snap.setPixelSize(stepWidth, stepHeight);
        return snap;
    }
}
