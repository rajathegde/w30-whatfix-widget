package co.quicko.whatfix.blog;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.Resizer;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.snap.StepSnap;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.Tokens;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.extension.util.Action;
import co.quicko.whatfix.extension.util.ExtensionHelper;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;

public class BlogEntry implements EntryPoint {
    private static final String VERSION = "version";
    private static final String ENT_ID = "entId";

    @Override
    public void onModuleLoad() {
        Action.setPrefix(Location.getParameter("message_prefix"));
        Blog.BUNDLE.icons().ensureInjected();
        Common.tracker().initialize(Security.unq_id());
        Themer.initialize();

        String flow = Window.Location.getParameter("flow");
        Long versionNum = 0l;
        if (Window.Location.getParameter(VERSION) != null)
            versionNum = Long.parseLong(Window.Location.getParameter(VERSION));

        if (flow == null || flow.length() == 0) {
            return;
        }
        String entId = getEntId();
        if (StringUtils.isNotBlank(entId)) {
            Enterpriser.enterprise().ent_id(entId);
        }        
        ExtensionHelper.initialize(Enterpriser.community());

        String size = Window.Location.getParameter(Framers.BLOG_SIZE_PARAM);
        boolean first = !Framers.BLOG_INTRO_NO.equals(Window.Location
                .getParameter(Framers.BLOG_START_PARAM));
        boolean nolive = Framers.LIVE_OFF.equals(Window.Location
                .getParameter(Framers.NO_LIVE_PARAM));

        int stepWidth = -1;
        int stepHeight = -1;
        if (Framers.BLOG_SIZE_FULL.equals(size)) {
            stepWidth = Common.getStepDimension(Framers.BLOG_SIZE_WIDTH);
            stepHeight = Common.getStepDimension(Framers.BLOG_SIZE_HEIGHT);
            if (stepWidth == -1 || stepHeight == -1) {
                // Reset to micro format if width & height are not given.
                size = Framers.BLOG_SIZE_MICRO;
            }
        }
        
        boolean pdf = Framers.BLOG_PDF
                .equals(Window.Location.getParameter(Framers.BLOG_PDF_PARAM));

        // Get PDFStyle
        String pdfStyle = "";
        if (null != Window.Location.getParameter(Framers.PDF_STYLE)) {
            pdfStyle = Window.Location.getParameter(Framers.PDF_STYLE);
        }

        RootPanel rp = RootPanel.get();
        rp.clear();
        
        if (pdf) {
            switch (pdfStyle) {
                case "portrait":
                    rp.add(new PortraitPDFBlog(flow, stepWidth, stepHeight,
                            versionNum, new AsyncCallback<PdfBlog>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    // Overridden method, nothing to do in case
                                    // of failure
                                }

                                @Override
                                public void onSuccess(PdfBlog result) {
                                    initialize(result);
                                }
                            }));
                    break;
                case "classic":
                default:
                    rp.add(new PdfBlog(flow, stepWidth, stepHeight, versionNum,
                        new AsyncCallback<PdfBlog>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // Overridden method, nothing to do in case
                                // of failure
                            }

                            @Override
                            public void onSuccess(PdfBlog result) {
                                initialize(result);
                            }
                        }));
                    break;
            }
        } else {
            rp.add(new TheBlog(flow, size, first, nolive, stepWidth, stepHeight,
                    new AsyncCallback<TheBlog>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            // Overridden method, nothing to do in case of
                            // failure
                        }
    
                        @Override
                        public void onSuccess(final TheBlog blog) {
                            initialize(blog);
                        }
                    }) {
                @Override
                protected void initialize(Flow flow, String size, boolean start,
                        boolean nolive, int stepWidth, int stepHeight) {
                    Common.tracker().use(flow.ent_id(), Security.user_id(),
                            Security.displayName(), Security.user_name(),
                            Enterpriser.enterprise().ga_id());
                    super.initialize(flow, size, start, nolive, stepWidth,
                            stepHeight);
                }
            });
        }
    }
        
    private void initialize(TheBlog blog) {
        Resizer.resizeWrt(blog, Framers.MIN_BLOG_WIDTH, Framers.MIN_BLOG_HEIGHT);
        GaUtil.src_id = src_id();
        // seoize & ga
        Flow flow = blog.flow();
        Common.seoize(Enterpriser.ent(), flow.title(), Tokens.flowToken(flow),
                JsUtils.getTextFromHtml(flow.description_md()), true,
                StepSnap.fullImageUrl(flow));
        
        GaUtil.trackFlowViewStart(flow.flow_id(), flow.title(), src_id(), ContentType.flow);
        GaUtil.trackFlowViewEnd(flow.flow_id(), flow.title(), src_id(), ContentType.flow);
    }
    
    private void initialize(PdfBlog blog) {
        Resizer.resizeWrt(blog, Framers.PDF_BLOG_WIDTH,
                Framers.PDF_BLOG_HEIGHT);
    }
    
    private String src_id() {
        return Common.getSrcIdFromURL(TrackerEventOrigin.BLOG);
    }

    private String getEntId() {
        if (StringUtils.isNotBlank(Window.Location.getParameter(ENT_ID))) {
            return Window.Location.getParameter(ENT_ID);
        } else {
            String path = Window.Location.getPath();
            int last = path.lastIndexOf('/');
            int first = path.lastIndexOf('/', last - 1);
            return path.substring(first + 1, last);
        }
    }

}
