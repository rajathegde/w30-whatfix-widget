package co.quicko.whatfix.blog;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.Import;

public interface BlogBundle extends ClientBundle {
    @Source("blog.css")
    @Import(value = { BlogCss.class })
    BlogCss css();
    
    @Source("style.css")
    CssResource icons();
}