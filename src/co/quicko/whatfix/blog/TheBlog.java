package co.quicko.whatfix.blog;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLTable.CellFormatter;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.blogbase.BlogBasePopupPanel;
import co.quicko.whatfix.common.Awesome;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Decider;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.LinkedFlowHandler;
import co.quicko.whatfix.common.PlayInvoker;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.FlowRunInterceptor;
import co.quicko.whatfix.common.snap.FlowAuthorSnap;
import co.quicko.whatfix.common.snap.MiniStepSnap;
import co.quicko.whatfix.common.snap.ScaledStepSnap;
import co.quicko.whatfix.common.snap.StepSnap;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.FlowAndEnt;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.extension.util.Extension;
import co.quicko.whatfix.extension.util.Runner;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.service.Callbacks;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.service.LocaleUtil;

public class TheBlog extends VerticalPanel {
    private Flow flow;
    private static final long ZERO = 0l;
    public TheBlog(final String flow_id, final String size, final boolean start,
            final boolean nolive, final int stepWidth, final int stepHeight,
            final AsyncCallback<TheBlog> cb) {
        final Label loading = new Label("loading");
        add(loading);
        FlowService.IMPL.flowAndEnt(flow_id, LocaleUtil.locale(),
                new AsyncCallback<FlowAndEnt>() {
            @Override
            public void onSuccess(FlowAndEnt result) {
                clear();
                Enterpriser.initialize(result.enterprise());
                initialize(result.flow(), size, start, nolive, stepWidth,
                        stepHeight);
                cb.onSuccess(TheBlog.this);
                // For stats update.
                FlowService.IMPL.viewed(Security.user_id(), flow_id,
                        Security.unq_id(), Callbacks.emptyVoidCb());
            }

            @Override
            public void onFailure(Throwable caught) {
                loading.setText("Content unavailable");
                cb.onFailure(caught);
            }
        },ZERO);
    }

    public TheBlog(Flow flow, String mini, boolean start, boolean nolive,
            int stepWidth, int stepHeight, FlowRunInterceptor interceptor) {
        initialize(flow, mini, start, nolive, stepWidth, stepHeight, interceptor);
    }
    
    protected void initialize(Flow flow, String size, boolean start,
            boolean nolive, int stepWidth, int stepHeight) {
        initialize(flow, size, start, nolive, stepWidth, stepHeight, null);
    } 

    protected void initialize(Flow flow, String size, boolean start,
            boolean nolive, int stepWidth, int stepHeight,
            FlowRunInterceptor interceptor) {
        this.flow = flow;
        nolive = nolive || !flow.seeLive();
        Common.initFlowLevelTheme(flow.flow_theme());

        JavaScriptObject properties = Enterpriser
                .properties(flow != null ? flow.locale() : null);
        Extension.CONSTANTS.use(properties);
        Common.CONSTANTS.use(properties);

        if (!Framers.BLOG_SIZE_FULL.equals(size)) {
            if (Framers.BLOG_SIZE_MICRO.equals(size)) {
                stepWidth = Blog.CSS.microStepWidth();
                stepHeight = Blog.CSS.microStepHeight();
            } else {
                stepWidth = Blog.CSS.stepWidth();
                stepHeight = Blog.CSS.stepHeight();
            }
        }

        int steps = flow.steps();
        boolean isRTL = !Common.isLTR(flow);

        Grid grid = new Grid(steps, 2);
        grid.setStyleName(Blog.CSS.snaps());
        CellFormatter formatter = grid.getCellFormatter();

        for (int step = 1; step <= steps; step++) {
            StepSnap snap;
            if (Framers.BLOG_SIZE_MICRO.equals(size)) {
                snap = new StepSnap();
            } else if (Framers.BLOG_SIZE_FULL.equals(size)) {
                snap = new ScaledStepSnap();
            } else {
                snap = new MiniStepSnap();
            }
            snap.addStyleName(Common.CSS.round());
            snap.addStyleName(Blog.CSS.snap());
            snap.setValue(flow.step(step));
            if (isRTL) {
                snap.alignPopoverRTL();
            }
            snap.refresh(flow.title(), step, steps);
            snap.setPixelSize(stepWidth, stepHeight);
            LinkedFlowHandler.handleSpecials(snap,
                    TrackerEventOrigin.BLOG.getSrcName(),
                    new AsyncCallback<Flow>() {

                @Override
                public void onSuccess(Flow flow) {
                    if (Common.isEmbed()) {
                        Framers.blogRefresh(flow.flow_id());
                    } else {
                        Common.setFlowUrl(flow.flow_id());
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                }
            });

            grid.setWidget(step - 1, 0,
                    Common.label(Integer.toString(step) + "."));
            grid.setWidget(step - 1, 1, snap);
            formatter.setVerticalAlignment(step - 1, 0, ALIGN_TOP);
        }

        setStyleName(Blog.CSS.blog());
        setWidth(Integer.toString(stepWidth + Blog.CSS.blogPlusWidth()) + "px");

        if (start) {
            Label title = Common.label(flow.title(), Blog.CSS.title());
            if (isRTL) {
                title.addStyleName(Overlay.CSS.rtl());
            }
            add(title);
        }
        Label description = Common.html(flow.description_md());
        if (isRTL) {
            description.addStyleName(Overlay.CSS.rtl());
        }

        add(Common.html(flow.description_md()));
        if (!nolive) {
            add(header(flow, interceptor));
        }
        if (PlayInvoker.isNormal(flow.url())) {
            Anchor url = Common.anchor(Common.cutShort(flow.url(), 50));
            Common.handleAnchor(url, flow.url(), false);
            add(Common.actions(Common.label("Open "), url));
        }
        add(grid);

        endMessage(flow);
        
        // Whatfix branding and See-Live button
        add(footer(flow, nolive, interceptor));
    }

    private Widget header(Flow flow,
            FlowRunInterceptor interceptor) {
        Widget run = Runner.makeRunner(flow, Callbacks.emptyVoidCb(),
                interceptor);
        return Common.makeThreePart(null, null, run);
    }

    private Widget footer(Flow flow, boolean nolive,
            FlowRunInterceptor interceptor) {
        Grid footer = null;
        Widget author = null;
        if (Enterpriser.community() && Decider.IMPL.showLinks()) {
            author = FlowAuthorSnap.author(flow, true);
            author.addStyleName(Blog.CSS.authorWrap());
        }

        Widget run = null;
        if (!nolive) {
            run = Runner.makeRunner(flow, Callbacks.emptyVoidCb(),
                    interceptor);
        }

        VerticalPanel logoPanel = null;
        if (Decider.IMPL.showLogo()) {
            Anchor logo = Common.anchor("", Common.referralUrl(), false,
                    Awesome.LOGO, Blog.CSS.brandLogo());
            FlowPanel footerPanel = new FlowPanel();
            footerPanel.setStyleName(Blog.CSS.footerPanel());
            footerPanel.add(Common.label("Powered by ", Blog.CSS.poweredBy()));
            footerPanel.add(logo);
            logoPanel = new VerticalPanel();
            logoPanel.setHorizontalAlignment(ALIGN_CENTER);
            logoPanel.add(footerPanel);
        }

        footer = author == null ? Common.makeThreePart(logoPanel, null, run)
                : Common.makeThreePart(author, logoPanel, run);
        footer.setStyleName(Blog.CSS.footerStyle());
        return footer;
    }

    public Flow flow() {
        return flow;
    }
    
    // This method adds end-message to an article
    public void endMessage(Flow flow) {
        FlowPanel footNotePanel = new FlowPanel();
        String endCustomMessage = flow.footnote_md();
        
        if (StringUtils.isNotBlank(endCustomMessage)) {
            footNotePanel.add(Common.html(endCustomMessage, Blog.CSS.blogFootnote()));
        } else {
            footNotePanel.add(Common.label(Common.CONSTANTS.endDefaultMessage(),
                    Blog.CSS.blogFootnote()));
        }

        BlogBasePopupPanel.applyTheme(footNotePanel, Themer.STYLE.COLOR,
                Themer.ENDPOP.TEXT_COLOR, Themer.STYLE.FONT_SIZE,
                Themer.ENDPOP.TEXT_SIZE + ";px", Themer.STYLE.TEXT_ALIGN,
                Themer.ENDPOP.TEXT_ALIGN, Themer.STYLE.FONT_STYLE,
                Themer.ENDPOP.TEXT_STYLE, Themer.STYLE.FONT_WEIGHT,
                Themer.ENDPOP.TEXT_WEIGHT);
        add(footNotePanel);
    }
}
