package co.quicko.whatfix.deck;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.CssResource.Import;

public interface DeckBundle extends ClientBundle {
    @Source("deck.css")
    @Import(value = { DeckCss.class })
    DeckCss css();

    @Source("style.css")
    CssResource icons();
}