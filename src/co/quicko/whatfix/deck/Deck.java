package co.quicko.whatfix.deck;

import co.quicko.whatfix.common.Common;

import com.google.gwt.core.client.GWT;

public class Deck {
    public static final DeckBundle BUNDLE = GWT.create(DeckBundle.class);
    public static final DeckCss CSS = BUNDLE.css();
    public static final DeckConstants CONSTANTS = GWT.create(DeckConstants.class);

    static {
        Common.CSS.ensureInjected();
        CSS.ensureInjected();
    }

}
