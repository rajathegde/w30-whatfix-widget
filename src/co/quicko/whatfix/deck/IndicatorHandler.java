package co.quicko.whatfix.deck;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.event.logical.shared.AttachEvent.Handler;
import com.google.gwt.user.client.ui.Anchor;

import co.quicko.whatfix.common.Awesome;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.TransImage;
import co.quicko.whatfix.deck.TheDeck.Displayer;
import co.quicko.whatfix.deck.TheDeck.PageProvider;
import co.quicko.whatfix.deck.TheDeck.SelectDirection;

public class IndicatorHandler
        implements MouseMoveHandler, MouseOutHandler, Handler {
    private TransImage trans;
    private PageProvider provider;
    private Displayer indicatorDisplayer;
    private SelectDirection direction;
    private TheDeck theDeck;
    private Anchor angleRight = Common.anchor("", Awesome.ANGLE_CIRCLE_RIGHT,
            Deck.CSS.flowNavigateRight());
    private Anchor angleLeft = Common.anchor("", Awesome.ANGLE_CIRCLE_LEFT,
            Deck.CSS.flowNavigateLeft());

    IndicatorHandler(Displayer displayer, TheDeck theDeck, TransImage trans,
            PageProvider provider, SelectDirection direct) {
        this.trans = trans;
        this.provider = provider;
        this.direction = direct;
        this.indicatorDisplayer = displayer;
        this.theDeck = theDeck;
        addIndicatorsClickHandler();
    }

    @Override
    public void onAttachOrDetach(AttachEvent event) {
        onMouseOut(null);
    }

    @Override
    public void onMouseOut(MouseOutEvent event) {
        // If the mouse is not over the deck, the left or right icons should be
        // removed based on their presence. If right icon was present before
        // then that
        // should be removed and if left was their then left should be removed
        if (trans.getWidgetIndex(angleLeft) != -1) {
            if (event != null && angleLeft.isAttached()) {
                // As the icon is added above the overlay, it gets removed when
                // we hover over the icon. To get rid of this, we check that if
                // the mouse is in the boundary of the icon, the icon should not
                // be removed
                if (!(event.getClientX() >= angleLeft.getAbsoluteLeft()
                        && event.getClientX() <= (angleLeft.getOffsetWidth()
                                + angleLeft.getAbsoluteLeft())
                        && event.getClientY() >= angleLeft.getAbsoluteTop()
                        && event.getClientY() <= (angleLeft.getAbsoluteTop()
                                + angleLeft.getOffsetHeight()))) {
                    trans.remove(angleLeft);
                }
            } else {
                trans.remove(angleLeft);
            }

        } else if (trans.getWidgetIndex(angleRight) != -1) {
            if (event != null && angleRight.isAttached()) {
                if (!(event.getClientX() >= angleRight.getAbsoluteLeft()
                        && event.getClientX() <= (angleRight.getAbsoluteLeft()
                                + angleRight.getOffsetWidth())
                        && event.getClientY() >= angleRight.getAbsoluteTop()
                        && event.getClientY() <= (angleRight.getAbsoluteTop()
                                + angleRight.getOffsetHeight())))
                    trans.remove(angleRight);
            } else {
                trans.remove(angleRight);
            }

        }
    }

    @Override
    public void onMouseMove(MouseMoveEvent event) {
        if (event.getX() < provider.width() / 2
                && !(direction == SelectDirection.FRONT)) {
            indicate(angleLeft, angleRight);
        } else if (direction == SelectDirection.FRONT) {
            indicate(angleRight, angleLeft);
        } else if (event.getX() > provider.width() / 2
                && (direction == SelectDirection.BOTH)) {
            indicate(angleRight, angleLeft);
        }
    }

    private void indicate(Anchor add, Anchor rem) {
        if (trans.getWidgetIndex(add) != -1) {
            return;
        }
        trans.add(add);
        trans.remove(rem);
    }

    private void addIndicatorsClickHandler() {
        angleRight.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                indicatorDisplayer.forward();
                indicatorDisplayer.trackButton();
                theDeck.updatePageNumber(
                        indicatorDisplayer.getCurrentPageNumber() + 1,
                        indicatorDisplayer.getPages().length);
            }
        });
        angleLeft.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                indicatorDisplayer.backward();
                indicatorDisplayer.trackButton();
                theDeck.updatePageNumber(
                        indicatorDisplayer.getCurrentPageNumber() + 1,
                        indicatorDisplayer.getPages().length);
            }
        });
    }
}
