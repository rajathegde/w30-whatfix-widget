package co.quicko.whatfix.deck;

import java.util.ArrayList;
import java.util.List;

import co.quicko.whatfix.common.*;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.ShortcutHandler.Shortcut;
import co.quicko.whatfix.common.ShortcutHandler.ShortcutListener;
import co.quicko.whatfix.common.snap.StepSnap;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.FlowAndEnt;
import co.quicko.whatfix.data.FlowGrabber;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.deck.handlers.BothSidesHandler;
import co.quicko.whatfix.deck.handlers.UniSideHandler;
import co.quicko.whatfix.extension.util.Extension;
import co.quicko.whatfix.extension.util.ExtensionHelper;
import co.quicko.whatfix.extension.util.Runner;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.service.Callbacks;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.service.LocaleUtil;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.slide.EndPage;
import co.quicko.whatfix.slide.FullEndPage;
import co.quicko.whatfix.slide.FullStartPage;
import co.quicko.whatfix.slide.FullStepPage;
import co.quicko.whatfix.slide.FullStepPage.FullBeaconPage;
import co.quicko.whatfix.slide.MicroEndPage;
import co.quicko.whatfix.slide.MicroStartPage;
import co.quicko.whatfix.slide.MicroStepPage;
import co.quicko.whatfix.slide.Slide;
import co.quicko.whatfix.slide.StartPage;
import co.quicko.whatfix.slide.StepPage;
import co.quicko.whatfix.workflowengine.app.AppFactory;

public class TheDeck extends VerticalPanel {
    private Displayer displayer;
    private Label pageNumber;
    private static final long ZERO = 0l;
    public static enum SelectDirection {
        BOTH, FRONT, BACK
    }

    public TheDeck(final String flow_id, final PageProvider provider,
            final boolean skipIntro, final int canFull, final int firstPage,
            final boolean showLive, final AsyncCallback<Where> onClose) {
        Label loading = new Label("loading...");
        add(loading);
        FlowService.IMPL.flowAndEnt(flow_id, LocaleUtil.locale(),
                new AsyncCallback<FlowAndEnt>() {
            @Override
            public void onSuccess(FlowAndEnt result) {
                Enterpriser.initialize(result.enterprise());
                        initialize(result.flow(), provider, skipIntro, canFull,
                        firstPage, showLive, onClose);

                // For stats update.
                FlowService.IMPL.viewed(Security.user_id(), flow_id,
                        Security.unq_id(), Callbacks.emptyVoidCb());
            }

            @Override
            public void onFailure(Throwable caught) {
                setStyleName(Deck.CSS.deck());
                FlowPanel closePanel = new FlowPanel();
                closePanel.addStyleName(Deck.CSS.bottomHeight());
                Anchor close = Common.anchor(Common.i18nConstants.closeButton(),
                        Common.CSS.seeLiveButton(), Deck.CSS.closeButton());
                close.addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        onClose.onSuccess(null);
                    }
                });
                closePanel.add(close);

                clear();
                FlowPanel contentPanel = new FlowPanel();
                contentPanel.addStyleName(Deck.CSS.contentUnavailable());
                contentPanel.setPixelSize(provider.width(), provider.height());
                Label contentLabel = Common.label("Content Unavailable");
                contentPanel.add(
                        Common.image(Common.BUNDLE.selfHelpNothingFound()));
                contentPanel.add(contentLabel);
                add(contentPanel);
                add(closePanel);
            }
        },ZERO);
    }

    public TheDeck(Flow flow, PageProvider provider, boolean skipIntro,
            final int canFull, int firstPage, boolean showLive) {
        initialize(flow, provider, skipIntro, canFull, firstPage, showLive,
                null);
    }
    
    public TheDeck(Flow flow, PageProvider provider, boolean skipIntro,
            final int canFull, int firstPage, boolean showLive, FlowRunInterceptor interceptor) {
        initialize(flow, provider, skipIntro, canFull, firstPage, showLive,
                null, interceptor);
    }

    protected void initialize(final Flow flow, PageProvider provider,
            boolean skipIntro, final int canFull, final int firstPage,
            boolean showLive, final AsyncCallback<Where> onClose) {
        initialize(flow, provider, skipIntro, canFull, firstPage, showLive, onClose, null);
    }
    
    protected void initialize(final Flow flow, PageProvider provider,
            boolean skipIntro, final int canFull, final int firstPage,
            boolean showLive, final AsyncCallback<Where> onClose, FlowRunInterceptor interceptor) {
        clear();
        setStyleName(Deck.CSS.deck());
        Common.initFlowLevelTheme(flow.flow_theme());

        // deck iframe width and height is reduced in the script but the
        // customers which had embed the deck with iframe tag in their website
        // would
        // have a height and width difference between the deck and the iframe.
        // There is a gap that appears because of that. To get rid of that we
        // increased border-spacing of the deck.
        if ((provider.width() + Deck.CSS.deck_h_gap()) < Window
                .getClientWidth()) {
            addStyleName(Deck.CSS.deckBorderSpacing());
        }
        Common.tracker().initialize(Security.unq_id());

        Extension.CONSTANTS.use(
                Enterpriser.properties(flow != null ? flow.locale() : null));
        List<Widget> widgets = new ArrayList<Widget>();
        Anchor run = null;
        if (showLive && flow.seeLive() && !AppFactory.isNonBrowserApp()) {
            run = Runner.makeRunner(flow, Callbacks.emptyVoidCb(), interceptor);
            widgets.add(0, run);
        }

        displayer = new Displayer(this, flow, provider, run, skipIntro,
                firstPage, isDeckPreview());

        pageNumber = Common.label(
                (displayer.now + 1) + "/" + (displayer.pages.length),
                Deck.CSS.slideNumberLabel());

        Anchor logoAnchor = null;
        HorizontalPanel brand = null;
        if (Decider.IMPL.showLogo()) {
            logoAnchor = Common.anchor("", Common.referralUrl(), false,
                    Awesome.LOGO, Deck.CSS.brandLogo(), Common.CSS.cursor());
            logoAnchor.setTitle(Deck.CONSTANTS.poweredTitle());
            Label poweredLabel = Common.label(Deck.CONSTANTS.powered(),
                    Deck.CSS.powered());
            brand = Common.actions(poweredLabel, logoAnchor);
            brand.setVisible(true);
        }

        if (canFull == 0) {
            widgets.add(makeFullAnchor(flow, showLive, skipIntro));
        } else if (canFull == 1) {
            Anchor cancel = Common.anchor("", Awesome.COMPRESS,
                    Deck.CSS.resizeSlide());
            cancel.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    FullScreen.API.cancelFullScreen();
                }
            });
            widgets.add(cancel);
        }

        add(displayer);
        
        if (onClose != null && canFull != 1) {
            Anchor close = Common.anchor("", Awesome.CANCEL_CIRCLE,
                    Deck.CSS.cancelCircle());
            close.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    int stepNumber = displayer.now;
                    String stepId = (stepNumber == 0
                            || stepNumber > flow.steps()) ? null
                                    : flow.step(stepNumber).stepId();
                    onClose.onSuccess(Where.create("flow_title", flow.title(),
                            "step_id", stepId, "step_number", displayer.now,
                            "steps", flow.steps()));

                }
            });
            widgets.add(close);
        }
        
        add(Common.makeThreePart(brand, pageNumber, widgets.size() == 1
                ? widgets.get(0)
                : Common.actions(widgets.toArray(new Widget[widgets.size()]))));
        Initiator.initialize(flow);
        setAnalytics();
    }

    private void setAnalytics() {
        if (StringUtils.isNotBlank(Common.getSegmentIdFromURL())) {
            GaUtil.analyticsCache.segment_id(Common.getSegmentIdFromURL());
        }
        if (StringUtils.isNotBlank(Common.getSegmentNameFromURL())) {
            GaUtil.analyticsCache.segment_name(Common.getSegmentNameFromURL());
        }
    }

    private Anchor makeFullAnchor(final Flow flow, final boolean showLive,
            final boolean skipIntro) {
        final Anchor full = Common.anchor("", Awesome.EXPAND,
                Deck.CSS.resizeSlide());
        full.setTitle("see full images");
        full.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (FullScreen.API.supportsFullScreen()) {
                    FullScreen.API.requestFullScreen(new FullSizeDeck(flow,
                            skipIntro, displayer.now, showLive));
                } else {
                    // open url which shows currently viewing step.
                    UrlBuilder builder = Window.Location.createUrlBuilder();
                    builder.setParameter("fullat",
                            Integer.toString(displayer.now));
                    Common.windowOpen(builder.buildString(), "_blank", "");
                    displayer.trackButton();
                }
            }
        });
        return full;
    }

    public void resize() {
        if (displayer != null) {
            displayer.resize();
        }
    }

    public int footerHeight() {
        if (displayer != null) {
            return getWidget(1).getOffsetHeight();
        } else {
            return 0;
        }
    }
    
    public void updatePageNumber(int nextSlide, int pagesLength) {
        pageNumber.setText((nextSlide) + "/" + (pagesLength));
    }
    
    protected boolean isDeckPreview() {
        return false;
    }

    public static class Displayer extends AbsolutePanel
            implements ShortcutListener {
        private static final Shortcut NEXT_CUT = new Shortcut(
                KeyCodes.KEY_RIGHT, false, false, false);
        private static final Shortcut PREV_CUT = new Shortcut(KeyCodes.KEY_LEFT,
                false, false, false);

        private TheDeck theDisplayerDeck;
        private TransImage[] pages;
        private int now;

        // for size changes aka full screen.
        private PageProvider provider;

        // for grabbing attention
        private boolean showedGrabber = false;
        private Anchor run;

        // for ga.
        private boolean skipIntro;
        private Flow flow;
        private boolean isDeckPreview;

        Displayer(TheDeck theDeck, Flow flow, PageProvider provider, Anchor run,
                boolean skipIntro, int firstPage, boolean isDeckPreview) {
            this.provider = provider;
            this.run = run;
            this.flow = flow;
            this.skipIntro = skipIntro;
            this.theDisplayerDeck = theDeck;
            this.isDeckPreview = isDeckPreview;

            BothSidesHandler bothHandler = new BothSidesHandler(this, theDisplayerDeck, provider);
            UniSideHandler fwdhandler = new UniSideHandler(this, theDisplayerDeck, true);
            UniSideHandler backHandler = new UniSideHandler(this, theDisplayerDeck, false);

            ClickHandler restartHandler = new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    show(0);
                    trackButton();
                    theDisplayerDeck.updatePageNumber(1, pages.length);
                }
            };

            int steps = flow.steps();
            
            boolean isRTL = !Common.isLTR(flow);
            int stepOff = 0;
            if (skipIntro) {
                stepOff = -1;
                pages = new TransImage[steps + 1];
            } else {
                pages = new TransImage[steps + 2];
                ClickHandler startHandler = new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        show(1);
                        trackButton();
                        theDisplayerDeck.updatePageNumber(now + 1, pages.length);
                    }
                };

                pages[0] = provider.start(flow, startHandler, isRTL);
                pages[0].transNone();
                addOverlay(pages[0], fwdhandler, SelectDirection.FRONT);  
            }

            setPixelSize(provider.width(), provider.height());
            pages[pages.length - 1] = provider.end(flow, restartHandler,
                    run != null);
            pages[pages.length - 1].transNone();
            addOverlay(pages[pages.length - 1], backHandler, SelectDirection.BACK);

            for (int step = 1; step <= steps; step++) {
                StepSnap snap = provider.step(flow.step(step), step,
                        steps, flow.title());
                if (isRTL) {
                    snap.alignPopoverRTL();
                }
                snap.refresh(flow.title(), step, steps);
                pages[stepOff + step] = snap;
                addOverlay(pages[stepOff + step], bothHandler, SelectDirection.BOTH);

                Label hightlight = ((StepSnap) pages[stepOff + step])
                        .getHightlight();
                hightlight.addStyleName(Common.CSS.cursor());
                hightlight.addMouseDownHandler(fwdhandler);
                LinkedFlowHandler.handleSpecials(pages[stepOff + step],
                        TrackerEventOrigin.DECK.getSrcName(),
                        new AsyncCallback<Flow>() {

                            @Override
                            public void onSuccess(Flow flow) {
                                if (Common.isEmbed()) {
                                    Framers.deckRefresh(flow.flow_id());
                                } else {
                                    Common.setFlowUrl(flow.flow_id());
                                }
                            }

                            @Override
                            public void onFailure(Throwable caught) {
                            }
                        });
            }

            show(firstPage % pages.length);
            
            String srcId = getSrcId();
            GaUtil.trackFlowViewStart(flow.flow_id(), flow.title(), srcId,
                    ContentType.flow);
            if (skipIntro) {
                GaUtil.trackFlowViewStep(flow, srcId, now + 1, ContentType.flow);
            }
        }
        
        private void addOverlay(TransImage page, MouseDownHandler handler,
                SelectDirection direction) {
            Label overlay = page.overlay();
            page.addOverlayStyles(0, Common.CSS.cursor());
            overlay.addMouseDownHandler(handler);
            page.setPixelSize(provider.width(), provider.height());
            if (direction == SelectDirection.BACK
                    || direction == SelectDirection.FRONT) {
                overlay.setPixelSize(provider.width() / 2, provider.height());
                if (direction == SelectDirection.FRONT) {
                    Common.setLeftProperty(overlay.getElement(),
                            provider.width() / 2);
                }
            }
            IndicatorHandler indicator;
            indicator = new IndicatorHandler(this, theDisplayerDeck, page, provider, direction);
            overlay.addMouseOutHandler(indicator);
            overlay.addMouseMoveHandler(indicator);
            overlay.addAttachHandler(indicator);
        }

        @Override
        protected void onLoad() {
            super.onLoad();
            ShortcutHandler.register(NEXT_CUT, this);
            ShortcutHandler.register(PREV_CUT, this);
        }

        @Override
        protected void onUnload() {
            super.onUnload();
            ShortcutHandler.unregister(NEXT_CUT, this);
            ShortcutHandler.unregister(PREV_CUT, this);
        }

        private void show(int index) {
            clear();
            now = index % pages.length;
            add(pages[now], 0, 0);

            showGrabber();
        }

        public void forward() {
            show(now + 1);
        }

        public void backward() {
            if (now == 0) {
                show(pages.length - 1);
            } else {
                show(now - 1);
            }
        }

        private void showGrabber() {
            if (showedGrabber) {
                return;
            }
            if (now == 0) {
                return;
            }

            // there is a possibility that this message is displayed before
            // extension helper has figured out extension availablity. so,
            // delaying the call.
            Scheduler.get().scheduleFixedPeriod(new RepeatingCommand() {
                @Override
                public boolean execute() {
                    tryGrabber();
                    return false;
                }
            }, 2000);
        }

        private void tryGrabber() {
            if (ExtensionHelper.isInstalled() || run == null
                    || flow.run_direct()) {
                showedGrabber = true;
                return;
            }

            Grabber pop = new Grabber(FlowGrabber.message(flow), false, true);
            pop.showSlowly(run);
            showedGrabber = true;
        }

        @Override
        public void onShortcut(Shortcut shortcut) {
            if (shortcut.key_code() == NEXT_CUT.key_code()) {
                forward();
            } else {
                backward();
            }
            trackButton();
            theDisplayerDeck.updatePageNumber(now + 1, pages.length);
        }
        
        public void trackButton() {
            String src_id = getSrcId();
            if (!skipIntro && now == 0) {
                GaUtil.trackFlowViewRestart(flow.flow_id(), flow.title(), src_id,
                        ContentType.flow);
                GaUtil.trackFlowViewStart(flow.flow_id(), flow.title(), src_id,
                        ContentType.flow);
            } else if (now == pages.length - 1) {
                GaUtil.trackFlowViewEnd(flow.flow_id(), flow.title(), src_id,
                        ContentType.flow);
            } else if (skipIntro) {
                GaUtil.trackFlowViewStep(flow, src_id,
                        now + 1, ContentType.flow);
            } else {
                GaUtil.trackFlowViewStep(flow, src_id, now,
                        ContentType.flow);
            }
        }

        void resize() {
            setPixelSize(provider.width(), provider.height());
            provider.resize(pages);
        }
        
        // now is the current page number on which the user currently is
        public int getCurrentPageNumber(){
            return now;
        }
        
        // pages contain front, end and step images of the flow
        public TransImage[] getPages() {
            return pages;
        }
        
        private String getSrcId() {
            TrackerEventOrigin eventOrigin = isDeckPreview
                    ? TrackerEventOrigin.DASHBOARD
                    : TrackerEventOrigin.DECK;
            return Common.getSrcIdFromURL(eventOrigin);
        }
    }

    public static interface PageProvider {
        public StartPage start(Flow flow, ClickHandler startHandler,
                boolean isRTL);

        public EndPage end(Flow flow, ClickHandler restartHandler,
                boolean showLive);

        public StepSnap step(Step step, int index, int length, String flowTitle);

        public int width();

        public int height();

        public void resize(TransImage[] pages);
    }

    public static class MicroPageProvider implements PageProvider {
        @Override
        public StartPage start(Flow flow, ClickHandler startHandler,
                boolean isRTL) {
            return new MicroStartPage(flow, startHandler, isRTL);
        }

        @Override
        public EndPage end(Flow flow, ClickHandler restartHandler,
                boolean showLive) {
            return new MicroEndPage(flow, restartHandler, showLive);
        }

        @Override
        public StepSnap step(Step step, int index, int length, String flowTitle) {
            return new MicroStepPage(step, index, length, flowTitle);
        }

        @Override
        public int width() {
            return Slide.CSS.microStepWidth();
        }

        @Override
        public int height() {
            return Slide.CSS.microStepHeight();
        }

        @Override
        public void resize(TransImage[] pages) {
        }
    }

    public static class MiniPageProvider implements PageProvider {
        @Override
        public StartPage start(Flow flow, ClickHandler startHandler,
                boolean isRTL) {
            return new StartPage(flow, startHandler, isRTL);
        }

        @Override
        public EndPage end(Flow flow, ClickHandler restartHandler,
                boolean showLive) {
            return new EndPage(flow, restartHandler, showLive);
        }

        @Override
        public StepSnap step(Step step, int index, int length, String flowTitle) {
            return new StepPage(step, index, length, flowTitle);
        }

        @Override
        public int width() {
            return Slide.CSS.stepWidth();
        }

        @Override
        public int height() {
            return Slide.CSS.stepHeight();
        }

        @Override
        public void resize(TransImage[] pages) {
        }
    }

    public static class FullPageProvider implements PageProvider {
        private int width;
        private int height;

        @Override
        public StartPage start(Flow flow, ClickHandler startHandler,
                boolean isRTL) {
            StartPage start = new FullStartPage(flow, startHandler, isRTL);
            start.setPixelSize(width, height);
            return start;
        }

        @Override
        public EndPage end(Flow flow, ClickHandler restartHandler,
                boolean showLive) {
            EndPage end = new FullEndPage(flow, restartHandler, showLive);
            end.setPixelSize(width, height);
            return end;
        }

        @Override
        public StepSnap step(Step step, int index, int length, String flowTitle) {
            if (step.getType() == ContentType.beacon) {
                return new FullBeaconPage(step, index, length, flowTitle);
            }
            return new FullStepPage(step, index, length, flowTitle);
        }

        @Override
        public int width() {
            return width;
        }

        @Override
        public int height() {
            return height;
        }

        @Override
        public void resize(TransImage[] pages) {
            for (TransImage page : pages) {
                page.setPixelSize(width, height);
            }
        }

        public void setSize(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }
    
}
