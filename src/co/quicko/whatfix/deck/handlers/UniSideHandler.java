package co.quicko.whatfix.deck.handlers;

import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;

import co.quicko.whatfix.deck.TheDeck;
import co.quicko.whatfix.deck.TheDeck.Displayer;

public class UniSideHandler implements MouseDownHandler {
    private Displayer displayer;
    private Boolean isFront;
    private TheDeck theDeck;

    public UniSideHandler(Displayer displayer, TheDeck theDeck, Boolean isFront) {
        this.displayer = displayer;
        this.isFront = isFront;
        this.theDeck = theDeck;
    }

    @Override
    public void onMouseDown(MouseDownEvent event) {
        if (isFront){
            displayer.forward();
        } else {
            displayer.backward();
        }
        displayer.trackButton();
        theDeck.updatePageNumber(displayer.getCurrentPageNumber() + 1,
                displayer.getPages().length);
    }
}
