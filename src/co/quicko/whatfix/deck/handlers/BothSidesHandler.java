package co.quicko.whatfix.deck.handlers;

import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;

import co.quicko.whatfix.deck.TheDeck;
import co.quicko.whatfix.deck.TheDeck.Displayer;
import co.quicko.whatfix.deck.TheDeck.PageProvider;

public class BothSidesHandler implements MouseDownHandler {
    private Displayer displayer;
    private PageProvider provider;
    private TheDeck theDeck;

    public BothSidesHandler(Displayer displayer,
            TheDeck theDeck, PageProvider provider) {
        this.displayer = displayer;
        this.provider = provider;
        this.theDeck = theDeck;
    }

    @Override
    public void onMouseDown(MouseDownEvent event) {
        if (event.getX() < provider.width() / 2) {
            displayer.backward();
            displayer.trackButton();
        } else {
            displayer.forward();
            displayer.trackButton();
        }
        theDeck.updatePageNumber(displayer.getCurrentPageNumber() + 1,
                displayer.getPages().length);
    }
}
