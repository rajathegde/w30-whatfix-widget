package co.quicko.whatfix.deck;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.FlowRunInterceptor;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.service.Where;

public class FullSizeDeck extends TheDeck {
    // no size alteration supported.
    public FullSizeDeck(String flow_id, final boolean skipIntro, int canFull,
            final int firstPage, boolean showLive) {
        super(flow_id, new FullPageProvider(), skipIntro, canFull, firstPage,
                showLive, null);
    }

    // supports docking back to normal size.
    public FullSizeDeck(Flow flow, final boolean skipIntro, final int firstPage,
            boolean showLive) {
        this(flow, skipIntro, firstPage, showLive, null);
    }

    // supports docking back to normal size.
    public FullSizeDeck(Flow flow, final boolean skipIntro, final int firstPage,
            boolean showLive, FlowRunInterceptor interceptor) {
        super(flow, new FullPageProvider(), skipIntro, 1, firstPage, showLive, interceptor);
    }
    
    @Override
    protected void initialize(Flow flow, final PageProvider provider,
            boolean skipIntro, int canFull, int firstPage, boolean showLive,
            AsyncCallback<Where> onClose) {
        initialize(flow, provider, skipIntro, canFull, firstPage, showLive, onClose, null);
    }
    
    @Override
    protected void initialize(Flow flow, final PageProvider provider,
            boolean skipIntro, int canFull, int firstPage, boolean showLive,
            AsyncCallback<Where> onClose, FlowRunInterceptor interceptor) {
        resizeProvider((FullPageProvider) provider, Deck.CSS.footer_height());
        super.initialize(flow, provider, skipIntro, canFull, firstPage,
                showLive, onClose, interceptor);

        // listen for window resize & adjust
        Window.addResizeHandler(new ResizeHandler() {
            @Override
            public void onResize(ResizeEvent event) {
                resizeDeck((FullPageProvider) provider);
            }
        });
        // now that we have initialized deck, we can make it occupy full
        // screen.
        Scheduler.get().scheduleDeferred(new ScheduledCommand() {
            @Override
            public void execute() {
                resizeDeck((FullPageProvider) provider);
            }
        });
    }

    private void resizeDeck(FullPageProvider full) {
        resizeProvider(full, footerHeight());
        setPixelSize(Window.getClientWidth(), Window.getClientHeight());
        resize();
    }

    private void resizeProvider(FullPageProvider full, int footer_height) {
        full.setSize(Window.getClientWidth() - Deck.CSS.deck_h_gap(),
                Window.getClientHeight() - footer_height
                        - Deck.CSS.deck_v_gap());
    }    
}
