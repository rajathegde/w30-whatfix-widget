package co.quicko.whatfix.deck;

import co.quicko.whatfix.i18n.client.Propertizable;

public interface DeckConstants extends Propertizable{
    
    @DefaultStringValue("Powered by whatfix.com")
    public String poweredTitle();

    @DefaultStringValue("Powered by")
    public String powered();
}
