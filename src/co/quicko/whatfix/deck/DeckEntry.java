package co.quicko.whatfix.deck;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.Resizer;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.snap.StepSnap;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.Tokens;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.deck.TheDeck.MicroPageProvider;
import co.quicko.whatfix.deck.TheDeck.MiniPageProvider;
import co.quicko.whatfix.deck.TheDeck.FullPageProvider;
import co.quicko.whatfix.deck.TheDeck.PageProvider;
import co.quicko.whatfix.extension.util.Action;
import co.quicko.whatfix.extension.util.ExtensionHelper;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.service.Where;

public class DeckEntry implements EntryPoint {
    private static final String MICRO_TOKEN = Framers.DECK_SIZE_MICRO + "/";
    private static final String FULL_TOKEN = Framers.DECK_SIZE_FULL + "/";
    private static final String CUSTOM_TOKEN = Framers.DECK_SIZE_CUSTOM + "/";
    private static final String ENT_ID = "entId";

    @Override
    public void onModuleLoad() {
        Action.setPrefix(Location.getParameter("message_prefix"));
        Deck.BUNDLE.icons().ensureInjected();
        Common.tracker().initialize(Security.unq_id());
        Themer.initialize();

        String token = Common.historyItem();
        if (token == null || token.length() == 0) {
            return;
        }
        ExtensionHelper.initialize(Enterpriser.community());
        String entId = Window.Location.getParameter(ENT_ID);
        if (StringUtils.isNotBlank(entId)) {
            Enterpriser.enterprise().ent_id(entId);
        }

        boolean micro = false;
        boolean full = false;
        boolean custom = false;
        if (token.startsWith(MICRO_TOKEN)) {
            micro = true;
            token = token.substring(MICRO_TOKEN.length());
        } else if (token.startsWith(FULL_TOKEN)) {
            full = true;
            token = token.substring(FULL_TOKEN.length());
        } else if (token.startsWith(CUSTOM_TOKEN)) {
            custom = true;
            token = token.substring(CUSTOM_TOKEN.length());
        }

        if ('/' == token.charAt(token.length() - 1)) {
            token = token.substring(0, token.length() - 1);
        }

        int slash = token.lastIndexOf('/');
        if (slash != -1) {
            token = token.substring(slash + 1);
        }

        // Some LMS append few parameters at the end of url. Ignoring them.
        int ampersand = token.indexOf("&");
        if (ampersand != -1) {
            token = token.substring(0, ampersand);
        }

        final String flow_id = token;

        final boolean skipIntro = Framers.DECK_INTRO_NO
                .equals(Location.getParameter(Framers.DECK_START_PARAM));
        final boolean showLive = !Framers.LIVE_OFF
                .equals(Location.getParameter(Framers.NO_LIVE_PARAM));

        int firstPage = 0;
        int canFull = 0;
        PageProvider provider = null;
        if (full || OverlayUtil.isTopWindow()) {
            String page = Window.Location.getParameter("fullat");
            if (page != null) {
                try {
                    firstPage = Integer.parseInt(page);
                } catch (Exception error) {
                    // ignore. some one played with the argument.
                }
            }
            canFull = full ? 0 : 2;
        } else if (micro) {
            provider = new MicroPageProvider();
        } else if (custom) {
            provider = new FullPageProvider();
        } else {
            provider = new MiniPageProvider();
        }

        TheDeck deck;
        if (provider != null) {
            AsyncCallback<Where> onClose = null;
            if ("true".equals(Window.Location.getParameter("closeable"))) {
                onClose = new AsyncCallback<Where>() {
                    @Override
                    public void onFailure(Throwable caught) {
                    }

                    @Override
                    public void onSuccess(Where result) {
                        CrossMessager.sendMessageToParent("popup_close", "");
                        GaUtil.trackFlowViewClose(result, flow_id);
                    };
                };
            }

            if (custom) {
                deck = new FullSizeDeck(token, skipIntro, canFull, firstPage,
                        showLive) {
                    @Override
                    protected void initialize(Flow flow,
                            final PageProvider provider, boolean skipIntro,
                            int canFull, int firstPage, boolean showLive,
                            AsyncCallback<Where> onClose) {

                        initializeSeo(flow);
                        initializeGa();

                        super.initialize(flow, provider, skipIntro, canFull,
                                firstPage, showLive, onClose);
                    }
                };

            } else {
                deck = new TheDeck(flow_id, provider, skipIntro, canFull,
                        firstPage, showLive, onClose) {
                    @Override
                    protected void initialize(Flow flow,
                            final PageProvider provider, boolean skipIntro,
                            int canFull, int firstPage, boolean showLive,
                            AsyncCallback<Where> onClose) {

                        initializeSeo(flow);
                        initializeGa();

                        super.initialize(flow, provider, skipIntro, canFull,
                                firstPage, showLive, onClose);
                    }
                };
            }

        } else {
            deck = new FullSizeDeck(token, skipIntro, canFull, firstPage,
                    showLive) {
                @Override
                protected void initialize(Flow flow,
                        final PageProvider provider, boolean skipIntro,
                        int canFull, int firstPage, boolean showLive,
                        AsyncCallback<Where> onClose) {

                    initializeSeo(flow);
                    initializeGa();

                    super.initialize(flow, provider, skipIntro, canFull,
                            firstPage, showLive, onClose);
                }
            };
        }

        RootPanel.get().add(deck);

        if (micro) {
            Resizer.resizeWrt(deck, Framers.MIN_DECK_MICRO_WIDTH,
                    Framers.MIN_DECK_MICRO_HEIGHT);
        } else if (custom) {
            int width = Integer
                    .parseInt(Location.getParameter(Framers.DECK_SIZE_WIDTH));
            int height = Integer
                    .parseInt(Location.getParameter(Framers.DECK_SIZE_HEIGHT));
            Resizer.resizeWrt(deck, width, height);
        } else {
            Resizer.resizeWrt(deck, Framers.MIN_DECK_WIDTH,
                    Framers.MIN_DECK_HEIGHT);
        }
    }
        
    private void initializeGa() {
        GaUtil.src_id = src_id();
        Common.tracker().use(Enterpriser.ent_id(), Security.user_id(),
                Security.displayName(), Security.user_name(),
                src_id(), Enterpriser.enterprise().ga_id());
        if (!Common.isViaEmbed()) {
            GaUtil.setWindowType(GaUtil.WindowType.iframe);
        }
    }

    private void initializeSeo(Flow flow) {
        Common.seoize(Enterpriser.ent(), flow.title(), Tokens.flowToken(flow),
                JsUtils.getTextFromHtml(flow.description_md()),
                StepSnap.fullImageUrl(flow));
    }

    private String src_id() {
        return Common.getSrcIdFromURL(TrackerEventOrigin.DECK);
    }
}
