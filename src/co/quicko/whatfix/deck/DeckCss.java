package co.quicko.whatfix.deck;

import com.google.gwt.resources.client.CssResource;

public interface DeckCss extends CssResource {
    public int deck_h_gap();

    public int deck_v_gap();
    
    public int footer_height();

    public String deck();

    public String deckMove();

    public String deckActionGap();

    public String flowNavigateLeft();

    public String flowNavigateRight();
    
    public String error();
    
    public String brandLogo();

    public String powered();

    public String slideNumberLabel();

    public String resizeSlide();
    
    public String cancelCircle();

    public String contentUnavailable();

    public String closeButton();

    public String bottomHeight();

    public String deckBorderSpacing();

}
