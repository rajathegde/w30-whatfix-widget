package co.quicko.whatfix.blogbase;

import com.google.gwt.core.client.GWT;

import co.quicko.whatfix.common.Common;

public class BlogBasePopup {
    public static final BlogBaseBundle BUNDLE = GWT.create(BlogBaseBundle.class);
    public static final BlogBasePopupCss CSS = BUNDLE.css();

    static {
        Common.CSS.ensureInjected();
        CSS.ensureInjected();
    }
}
