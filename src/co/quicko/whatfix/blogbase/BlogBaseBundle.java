package co.quicko.whatfix.blogbase;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource.Import;

public interface BlogBaseBundle extends ClientBundle {
    @Source("blogbasepopup.css")
    @Import(value = { BlogBasePopupCss.class })
    BlogBasePopupCss css();
}
