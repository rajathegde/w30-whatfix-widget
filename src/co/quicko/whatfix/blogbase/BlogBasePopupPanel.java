package co.quicko.whatfix.blogbase;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.ContentUtil;
import co.quicko.whatfix.common.Decider;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.snap.ScaledStepSnap;
import co.quicko.whatfix.common.snap.StepSnap;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.Themer.JsTheme;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.security.Enterpriser;

public class BlogBasePopupPanel {
    public static class EndFlowPopup extends FlowPanel {
        protected FlowPanel endFlowPanel;

        public EndFlowPopup(Flow flow) {
            endFlowPanel = new FlowPanel();

            FlowPanel barPanel = new FlowPanel();
            barPanel.addStyleName(BlogBasePopup.CSS.footnoteBar());
            applyTheme(barPanel, Themer.STYLE.BACKGROUD_COLOR,
                    getBgColorKey(flow));
            endFlowPanel.add(barPanel);

            FlowPanel footNotePanel = new FlowPanel();
            if (StringUtils.isNotBlank(flow.footnote_md())) {
                footNotePanel.add(Common.html(flow.footnote_md(),
                        BlogBasePopup.CSS.pdfFootnote()));
            } else {
                footNotePanel
                        .add(Common.label(Common.CONSTANTS.endDefaultMessage(),
                                BlogBasePopup.CSS.pdfFootnote()));
            }

            applyTheme(footNotePanel, Themer.STYLE.COLOR,
                    Themer.ENDPOP.TEXT_COLOR, Themer.STYLE.FONT_SIZE,
                    Themer.ENDPOP.TEXT_SIZE + ";px", Themer.STYLE.TEXT_ALIGN,
                    Themer.ENDPOP.TEXT_ALIGN, Themer.STYLE.FONT_STYLE,
                    Themer.ENDPOP.TEXT_STYLE, Themer.STYLE.FONT_WEIGHT,
                    Themer.ENDPOP.TEXT_WEIGHT);

            endFlowPanel.add(footNotePanel);

            add(endFlowPanel);
        }
    }

    public static class StartFlowPopup extends FlowPanel {
        protected FlowPanel startFlowPanel;

        public StartFlowPopup(Flow flow, boolean isRTL) {
            startFlowPanel = new FlowPanel();
            FlowPanel titleFlowPanel = new FlowPanel();
            FlowPanel descFlowPanel = new FlowPanel();

            HTML title = Common.html(flow.title(),
                    BlogBasePopup.CSS.pdfTitleDesc());
            if(isRTL) {
                title.addStyleName(Overlay.CSS.rtl());
            }
            if (StringUtils.isNotBlank(Common.fallback(flow.description_md(),
                    flow.description_md()))) {                
                HTML description = Common.html(
                        Common.fallback(flow.description_md(),
                                flow.description_md()),
                        BlogBasePopup.CSS.pdfDescription());
                if (isRTL) {
                    description.addStyleName(Overlay.CSS.rtl());
                }
                titleFlowPanel.add(title);
                descFlowPanel.add(description);
                startFlowPanel.add(titleFlowPanel);
                startFlowPanel.add(descFlowPanel);
            } else {
                titleFlowPanel.add(title);
                startFlowPanel.add(titleFlowPanel);
            }
            
            applyTheme(titleFlowPanel, Themer.STYLE.BACKGROUD_COLOR,
                    getBgColorKey(flow), Themer.STYLE.COLOR,
                    getTextColorKey(flow));
            add(startFlowPanel);
        }
    }

    public static class BrandingPanel extends FlowPanel {
        private FlowPanel brandingPanel;

        public BrandingPanel() {
            brandingPanel = new FlowPanel();
            brandingPanel.add(Common.label("powered by ",
                    BlogBasePopup.CSS.brandingText()));
            brandingPanel.add(
                    new Image(Common.BUNDLE.newLogo().getSafeUri().asString()));
            add(brandingPanel);
        }
    }

    private static String getBgColorKey(Flow flow) {
        if (ContentUtil.isSmartTips(flow)) {
            return Themer.SMART_TIP.BODY_BG_COLOR;
        }
        return Themer.getThemeKey(Themer.TIP.BODY_BG_COLOR);
    }

    public static void applyTheme(Object... themeConf) {
        JsTheme theme = Enterpriser.enterprise().theme();
        Themer.applyTheme(theme, themeConf);
    }

    private static String getTextColorKey(Flow flow) {
        if (ContentUtil.isSmartTips(flow)) {
            return Themer.SMART_TIP.TITLE_COLOR;
        }
        return Themer.getThemeKey(Themer.TIP.TITLE_COLOR);
    }

    public static Widget getPage(Flow flow, int stepWidth, int stepHeight,
            int step) {
        StepSnap snap = new ScaledStepSnap(true, false, false);
        snap.setValueWithoutPopover(flow.step(step));
        snap.setPixelSize(stepWidth, stepHeight);
        return snap;
    }

    public static void addBrandingPanel(FlowPanel container) {
        if (Decider.IMPL.showLogo()) {
            BrandingPanel brandingPanel = new BrandingPanel();
            brandingPanel.addStyleName(BlogBasePopup.CSS.branding());
            container.add(brandingPanel);
        }
    }

}
