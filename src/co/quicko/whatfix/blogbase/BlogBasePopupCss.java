package co.quicko.whatfix.blogbase;

import com.google.gwt.resources.client.CssResource;

public interface BlogBasePopupCss extends CssResource {
    
    public String footnoteBar();
    
    public String pdfFootnote();
        
    public String pdfTitleDesc();
    
    public String pdfDescription();
    
    public String branding();
    
    public String brandingText();

    public String popup();

    public String pageContainer();

    public String pdfSnaps();

    public String page();

    public String portraitFirstPage();

    public String flowContainer();

    public String pdfFlowTitle();

    public String pdfFlowDesc();

    public String pdfStepTitle();

    public String pdfStepDesc();

    public String pdfStepSnapPortrait();

    public String pdfStepInfo();

    public String dummyMargin();

    public String horizontalSeparator();

    public String flowHzSeparator();

    public String gridStyle();

    public String innerGridStyle();

    public String innerGridStyleRTL();

    public String pdfStepTitleRTL();

    public String portraitPageBreak();

    public String portraitFirstPageBreak();

    public String portraitFootnote();

    public String pdfWhatfixBrand();
}
